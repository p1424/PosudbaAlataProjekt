import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojZahtjevComponent } from './moj-zahtjev.component';

describe('MojZahtjevComponent', () => {
  let component: MojZahtjevComponent;
  let fixture: ComponentFixture<MojZahtjevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojZahtjevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojZahtjevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
