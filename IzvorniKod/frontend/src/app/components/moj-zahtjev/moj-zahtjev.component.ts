import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { najMajstor } from 'src/app/models/najMajstor';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { MojZahtjevDetailsComponent } from '../moj-zahtjev-details/moj-zahtjev-details.component';

@Component({
  selector: 'app-moj-zahtjev',
  templateUrl: './moj-zahtjev.component.html',
  styleUrls: ['./moj-zahtjev.component.css']
})
export class MojZahtjevComponent implements OnInit {
  @Input() zahtjev: VlastitiZahtjev

  constructor(private zahtjevService: ZahtjevService, private modal: NgbModal, private router: Router, public userService: UserService) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showOptions(idZahtjeva: any){
    document.getElementById(`${idZahtjeva}-moj-zahtjev-dropdown-content`).classList.toggle('d-block')
  }

  public showZahtjev(){
    const modalResult = this.modal.open(MojZahtjevDetailsComponent, {scrollable: true, size: 'xl', centered: true})
    modalResult.componentInstance.mojZahtjevDetails = this.zahtjev
  }

  public deleteVlastitiZahtjev(id: any){
    this.zahtjevService.deleteZahtjev(id).subscribe(
      (res) =>{
        this.zahtjevService.mojiZahtjevi.splice(this.zahtjevService.mojiZahtjevi.findIndex(o => o.zahtjev.idZahtjeva === id), 1)
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

  public urediZahtjev(){
    this.router.navigate([`/profil/mojiZahtjevi/urediZahtjev/${this.zahtjev.zahtjev.idZahtjeva}`])
  }
}
