import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojaPosudbaZahtjevComponent } from './moja-posudba-zahtjev.component';

describe('MojaPosudbaZahtjevComponent', () => {
  let component: MojaPosudbaZahtjevComponent;
  let fixture: ComponentFixture<MojaPosudbaZahtjevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojaPosudbaZahtjevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojaPosudbaZahtjevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
