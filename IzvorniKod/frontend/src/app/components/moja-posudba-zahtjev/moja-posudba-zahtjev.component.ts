import { UserService } from 'src/app/services/userService/user.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PosudbaZahtjev } from 'src/app/models/posudbaZahtjev';
import { MojaPosudbaZahtjevDetailsComponent } from '../moja-posudba-zahtjev-details/moja-posudba-zahtjev-details.component';
import { najMajstor } from 'src/app/models/najMajstor';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-moja-posudba-zahtjev',
  templateUrl: './moja-posudba-zahtjev.component.html',
  styleUrls: ['./moja-posudba-zahtjev.component.css']
})
export class MojaPosudbaZahtjevComponent implements OnInit {
  @Input() mojaPosudba: PosudbaZahtjev

  constructor(private modal: NgbModal, public userService: UserService) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showPosudba(){
    const modalResult = this.modal.open(MojaPosudbaZahtjevDetailsComponent, {scrollable: true, size: 'xl', centered: true})
    modalResult.componentInstance.mojaPosudbaDetails = this.mojaPosudba
  }
}
