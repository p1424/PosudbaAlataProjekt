import { najMajstor } from './../../models/najMajstor';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Korisnik } from 'src/app/models/korisnik';
import { AdminService } from 'src/app/services/adminService/admin.service';
import { UserService } from 'src/app/services/userService/user.service';
import { KorisnikDetailsComponent } from '../korisnik-details/korisnik-details.component';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-korisnik',
  templateUrl: './korisnik.component.html',
  styleUrls: ['./korisnik.component.css']
})
export class KorisnikComponent implements OnInit {
  @Input() korisnik: Korisnik

  constructor(public userService: UserService, private modal: NgbModal, private adminService: AdminService) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showkorisnik(){
    const modalResult = this.modal.open(KorisnikDetailsComponent, {size: 'lg', centered: true})
    modalResult.componentInstance.korisnikDetails = this.korisnik
  }

  public showOptions(){
    document.getElementById(`${this.korisnik.userId}-korisnik-dropdown-content`).classList.toggle('d-block')
  }

}
