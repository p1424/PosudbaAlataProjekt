import { UserService } from 'src/app/services/userService/user.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PosudbaOglas } from 'src/app/models/posudbaOglas';
import { MojaPosudbaOglasDetailsComponent } from '../moja-posudba-oglas-details/moja-posudba-oglas-details.component';
import { najMajstor } from 'src/app/models/najMajstor';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-moja-posudba-oglas',
  templateUrl: './moja-posudba-oglas.component.html',
  styleUrls: ['./moja-posudba-oglas.component.css']
})
export class MojaPosudbaOglasComponent implements OnInit {
  @Input() mojaPosudba: PosudbaOglas

  constructor(private modal: NgbModal, public userService: UserService) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showPosudba(){
    const modalResult = this.modal.open(MojaPosudbaOglasDetailsComponent, {scrollable: true, size: 'xl', centered: true})
    modalResult.componentInstance.mojaPosudbaDetails = this.mojaPosudba
  }

}
