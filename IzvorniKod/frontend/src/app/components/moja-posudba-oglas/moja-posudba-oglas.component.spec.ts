import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojaPosudbaOglasComponent } from './moja-posudba-oglas.component';

describe('MojaPosudbaOglasComponent', () => {
  let component: MojaPosudbaOglasComponent;
  let fixture: ComponentFixture<MojaPosudbaOglasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojaPosudbaOglasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojaPosudbaOglasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
