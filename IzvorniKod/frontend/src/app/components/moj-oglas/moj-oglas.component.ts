import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { najMajstor } from 'src/app/models/najMajstor';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { MojOglasDetailsComponent } from '../moj-oglas-details/moj-oglas-details.component';

@Component({
  selector: 'app-moj-oglas',
  templateUrl: './moj-oglas.component.html',
  styleUrls: ['./moj-oglas.component.css']
})
export class MojOglasComponent implements OnInit {
  @Input() mojOglas: VlastitiOglas

  constructor(private oglasService: OglasService, private modal: NgbModal, private router: Router, public userService: UserService) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showOglas(){
    const modalResult = this.modal.open(MojOglasDetailsComponent, {scrollable: true, size: 'xl', centered: true})
    modalResult.componentInstance.mojOglasDetails = this.mojOglas
  }

  public showOptions(id: any){
    document.getElementById(`${this.mojOglas.oglas.idOglasa}-moj-oglas-dropdown-content`).classList.toggle('d-block')
  }

  public deleteVlastitiOglas(id: any){
    this.oglasService.deleteOglas(id).subscribe(
      (res) =>{
        this.oglasService.mojiOglasi.splice(this.oglasService.mojiOglasi.findIndex(o => o.oglas.idOglasa === id), 1)
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

  public urediOglas(){
    this.router.navigate([`/profil/mojiOglasi/urediOglas/${this.mojOglas.oglas.idOglasa}`])
  }

}
