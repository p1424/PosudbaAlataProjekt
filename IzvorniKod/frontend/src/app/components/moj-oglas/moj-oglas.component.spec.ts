import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojOglasComponent } from './moj-oglas.component';

describe('MojOglasComponent', () => {
  let component: MojOglasComponent;
  let fixture: ComponentFixture<MojOglasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojOglasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojOglasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
