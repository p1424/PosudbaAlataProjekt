import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Oglas } from 'src/app/models/oglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-oglas-admin-details',
  templateUrl: './oglas-admin-details.component.html',
  styleUrls: ['./oglas-admin-details.component.css']
})
export class OglasAdminDetailsComponent implements OnInit {
  @Input() oglasDetails: Oglas

  constructor(public userService: UserService, private oglasService: OglasService, private router: Router, private posudbaService: PosudbaService, public modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll();
      }
    });
  }

  public deleteOglas(id: any){
    this.oglasService.deleteOglas(id).subscribe(
      (res) =>{
        this.oglasService.sviOglasi.splice(this.oglasService.sviOglasi.findIndex(o => o.idOglasa === id), 1)
        this.router.navigate(['/'], { skipLocationChange: true }).then(() => {
          this.router.navigate(['/profil/oglasi'])
        })
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

}
