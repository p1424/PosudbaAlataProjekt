import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OglasAdminDetailsComponent } from './oglas-admin-details.component';

describe('OglasAdminDetailsComponent', () => {
  let component: OglasAdminDetailsComponent;
  let fixture: ComponentFixture<OglasAdminDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OglasAdminDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OglasAdminDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
