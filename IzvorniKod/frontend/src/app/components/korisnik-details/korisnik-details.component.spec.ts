import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KorisnikDetailsComponent } from './korisnik-details.component';

describe('KorisnikDetailsComponent', () => {
  let component: KorisnikDetailsComponent;
  let fixture: ComponentFixture<KorisnikDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KorisnikDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KorisnikDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
