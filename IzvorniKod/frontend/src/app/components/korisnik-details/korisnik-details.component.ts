import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Korisnik } from 'src/app/models/korisnik';
import { Prijava } from 'src/app/models/prijava';
import { AdminService } from 'src/app/services/adminService/admin.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-korisnik-details',
  templateUrl: './korisnik-details.component.html',
  styleUrls: ['./korisnik-details.component.css']
})
export class KorisnikDetailsComponent implements OnInit {
  @Input() korisnikDetails: Korisnik
  prijave: Prijava[]

  constructor(private adminService: AdminService, public modal: NgbModal, private router: Router, public userService: UserService) {
    this.userService.getUser()

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll()
      }
    })

  }

  ngOnInit(): void {
    this.adminService.getReport(this.korisnikDetails.userId).subscribe(
      (res) => {
        this.prijave = res as Prijava[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  public blockUser(id: any){
    this.adminService.blockUser(id).subscribe(
      (res) =>{
        this.router.navigate(['/'], { skipLocationChange: true }).then(() => {
          this.router.navigate(['/profil/korisnici'])
        })
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

  public unblockUser(id: any){
    this.adminService.unblockUser(id).subscribe(
      (res) =>{
        this.router.navigate(['/'], { skipLocationChange: true }).then(() => {
          this.router.navigate(['/profil/korisnici']);
      });
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

}
