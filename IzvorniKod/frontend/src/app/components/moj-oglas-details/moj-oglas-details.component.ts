import { NavigationEnd, Router } from '@angular/router';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { OglasService } from 'src/app/services/oglasService/oglas.service';

@Component({
  selector: 'app-moj-oglas-details',
  templateUrl: './moj-oglas-details.component.html',
  styleUrls: ['./moj-oglas-details.component.css']
})
export class MojOglasDetailsComponent implements OnInit {
  @Input() mojOglasDetails: VlastitiOglas

  constructor(public modal: NgbModal, private posudbaService: PosudbaService, private router: Router, private oglasService: OglasService) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll()
      }
    })
  }

  ngOnInit(): void {
  }

  ponistiInteres(){
    this.posudbaService.ponistiOglas(this.mojOglasDetails.oglas.idOglasa).subscribe(
      (res) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Uspjeh";
        modalInstance.componentInstance.afterClose = () => {
          this.modal.dismissAll()
          this.oglasService.interesiOglasi.splice(this.oglasService.interesiOglasi.findIndex(o => o.oglas.idOglasa === this.mojOglasDetails.oglas.idOglasa), 1)
        }

      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }
}
