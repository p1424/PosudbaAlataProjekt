import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojOglasDetailsComponent } from './moj-oglas-details.component';

describe('MojOglasDetailsComponent', () => {
  let component: MojOglasDetailsComponent;
  let fixture: ComponentFixture<MojOglasDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojOglasDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojOglasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
