import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtjevComponent } from './zahtjev.component';

describe('ZahtjevComponent', () => {
  let component: ZahtjevComponent;
  let fixture: ComponentFixture<ZahtjevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtjevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtjevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
