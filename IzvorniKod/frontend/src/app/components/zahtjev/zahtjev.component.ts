import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { najMajstor } from 'src/app/models/najMajstor';
import { Zahtjev } from 'src/app/models/zahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { ZahtjevDetailsComponent } from '../zahtjev-details/zahtjev-details.component';

@Component({
  selector: 'app-zahtjev',
  templateUrl: './zahtjev.component.html',
  styleUrls: ['./zahtjev.component.css']
})
export class ZahtjevComponent implements OnInit {
  @Input() zahtjev: Zahtjev

  constructor(public userService: UserService, private zahtjevService: ZahtjevService, private modal: NgbModal) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showOptions(){
    document.getElementById(`${this.zahtjev.idZahtjeva}-zahtjev-dropdown-content`).classList.toggle('d-block')
  }

  public showZahtjev(){
    const modalResult = this.modal.open(ZahtjevDetailsComponent, {size: 'xl', centered: true})
    modalResult.componentInstance.zahtjevDetails = this.zahtjev
  }

  public deleteZahtjev(id: any){
    this.zahtjevService.deleteZahtjev(id).subscribe(
      (res) =>{
        this.zahtjevService.zahtjevi.splice(this.zahtjevService.zahtjevi.findIndex(o => o.idZahtjeva === id), 1)
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }

}
