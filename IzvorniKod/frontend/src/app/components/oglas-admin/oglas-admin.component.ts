import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { najMajstor } from 'src/app/models/najMajstor';
import { Oglas } from 'src/app/models/oglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { OglasAdminDetailsComponent } from '../oglas-admin-details/oglas-admin-details.component';

@Component({
  selector: 'app-oglas-admin',
  templateUrl: './oglas-admin.component.html',
  styleUrls: ['./oglas-admin.component.css']
})
export class OglasAdminComponent implements OnInit {
  @Input() oglas: Oglas

  constructor(public userService: UserService, private oglasService: OglasService, private modal: NgbModal) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showOglas(){
    const modalResult = this.modal.open(OglasAdminDetailsComponent, {size: 'xl', centered: true})
    modalResult.componentInstance.oglasDetails = this.oglas
  }

  public showOptions(){
    document.getElementById(`${this.oglas.idOglasa}-oglas-admin-dropdown-content`).classList.toggle('d-block')
  }

  public deleteOglas(id: any){
    this.oglasService.deleteOglas(id).subscribe(
      (res) =>{
        this.oglasService.sviOglasi.splice(this.oglasService.sviOglasi.findIndex(o => o.idOglasa === id), 1)
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }
}
