import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OglasAdminComponent } from './oglas-admin.component';

describe('OglasAdminComponent', () => {
  let component: OglasAdminComponent;
  let fixture: ComponentFixture<OglasAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OglasAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OglasAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
