import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { najMajstor } from 'src/app/models/najMajstor';
import { Oglas } from 'src/app/models/oglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';
import { OglasDetailsComponent } from '../oglas-details/oglas-details.component';

@Component({
  selector: 'app-oglas',
  templateUrl: './oglas.component.html',
  styleUrls: ['./oglas.component.css']
})
export class OglasComponent implements OnInit {
  @Input() oglas: Oglas

  constructor(public userService: UserService, private oglasService: OglasService, private modal: NgbModal) {
    this.userService.getUser()

    this.userService.getNajMajstor().subscribe(
      (res) => {
        this.userService.najMajstor = res as najMajstor
      }
    )
  }

  ngOnInit(): void {
  }

  public showOglas(){
    const modalResult = this.modal.open(OglasDetailsComponent, {size: 'xl', centered: true})
    modalResult.componentInstance.oglasDetails = this.oglas
  }

  public showOptions(){
    document.getElementById(`${this.oglas.idOglasa}-oglas-dropdown-content`).classList.toggle('d-block')
  }

  public deleteOglas(id: any){
    this.oglasService.deleteOglas(id).subscribe(
      (res) =>{
        this.oglasService.oglasi.splice(this.oglasService.oglasi.findIndex(o => o.idOglasa === id), 1)
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      })
  }
}
