import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojZahtjevDetailsComponent } from './moj-zahtjev-details.component';

describe('MojZahtjevDetailsComponent', () => {
  let component: MojZahtjevDetailsComponent;
  let fixture: ComponentFixture<MojZahtjevDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojZahtjevDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojZahtjevDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
