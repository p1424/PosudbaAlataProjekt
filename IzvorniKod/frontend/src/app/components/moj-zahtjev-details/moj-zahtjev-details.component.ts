import { NavigationEnd, Router } from '@angular/router';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-moj-zahtjev-details',
  templateUrl: './moj-zahtjev-details.component.html',
  styleUrls: ['./moj-zahtjev-details.component.css']
})
export class MojZahtjevDetailsComponent implements OnInit {
  @Input() mojZahtjevDetails: VlastitiZahtjev

  constructor(private zahtjevService: ZahtjevService, public modal: NgbModal, private posudbaService: PosudbaService, private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll()
      }
    })

  }

  ngOnInit(): void {
  }

  ponistiInteres(){
    this.posudbaService.ponistiZahtjev(this.mojZahtjevDetails.zahtjev.idZahtjeva).subscribe(
      (res) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Uspjeh";
        modalInstance.componentInstance.afterClose = () => {
          this.modal.dismissAll()
          this.zahtjevService.interesiZahtjevi.splice(this.zahtjevService.interesiZahtjevi.findIndex(o => o.zahtjev.idZahtjeva === this.mojZahtjevDetails.zahtjev.idZahtjeva), 1)
        }

      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }
}
