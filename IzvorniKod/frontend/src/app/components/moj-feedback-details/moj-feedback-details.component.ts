import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Feedback } from 'src/app/models/feedback';

@Component({
  selector: 'app-moj-feedback-details',
  templateUrl: './moj-feedback-details.component.html',
  styleUrls: ['./moj-feedback-details.component.css']
})
export class MojFeedbackDetailsComponent implements OnInit {
  @Input() feedbackDetails: Feedback

  constructor(public modal: NgbModal) { }

  ngOnInit(): void {
  }

}
