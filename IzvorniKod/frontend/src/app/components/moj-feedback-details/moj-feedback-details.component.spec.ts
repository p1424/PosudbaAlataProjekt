import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojFeedbackDetailsComponent } from './moj-feedback-details.component';

describe('MojFeedbackDetailsComponent', () => {
  let component: MojFeedbackDetailsComponent;
  let fixture: ComponentFixture<MojFeedbackDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojFeedbackDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojFeedbackDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
