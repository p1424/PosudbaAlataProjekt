import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Zahtjev } from 'src/app/models/zahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-zahtjev-admin-details',
  templateUrl: './zahtjev-admin-details.component.html',
  styleUrls: ['./zahtjev-admin-details.component.css']
})
export class ZahtjevAdminDetailsComponent implements OnInit {
  @Input() zahtjevDetails: Zahtjev

  constructor(public modal: NgbModal, public userService: UserService, private zahtjevService: ZahtjevService, private router: Router) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll();
      }
    });
  }

  public deleteZahtjev(id: any){
    this.zahtjevService.deleteZahtjev(id).subscribe(
      (res) =>{
        this.zahtjevService.sviZahtjevi.splice(this.zahtjevService.sviZahtjevi.findIndex(o => o.idZahtjeva === id), 1)
        this.router.navigate(['/'], { skipLocationChange: true }).then(() => {
          this.router.navigate(['/profil/zahtjevi'])
        })
      },
      (error) =>{
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message
      })
  }
}
