import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtjevAdminDetailsComponent } from './zahtjev-admin-details.component';

describe('ZahtjevAdminDetailsComponent', () => {
  let component: ZahtjevAdminDetailsComponent;
  let fixture: ComponentFixture<ZahtjevAdminDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtjevAdminDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtjevAdminDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
