import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Komentar } from 'src/app/models/komentar';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { environment } from 'src/environments/environment';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-komentar',
  templateUrl: './komentar.component.html',
  styleUrls: ['./komentar.component.css']
})
export class KomentarComponent implements OnInit {
  @Input() komentar: Komentar
  @Input() idZahtjeva: any
  helper: string = this.router.url

  constructor(public userService: UserService, private zahtjevService: ZahtjevService, private router: Router, private modal: NgbModal) {
    this.userService.getUser()

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll()
      }
    })
  }

  ngOnInit(): void {
  }

  addLike(idKomentara: any){
    if(this.userService.currentUser){
      this.zahtjevService.addLike(idKomentara).subscribe(
        (res) => {
          this.komentar.currentUserLiked = !this.komentar.currentUserLiked
          this.komentar.currentUserLiked ? this.komentar.lajkovi.length++ : this.komentar.lajkovi.length--
        },
        (error) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = error.error.message;
        }
      )
    }
  }

  deleteComment(idKomentara: any){
    this.zahtjevService.deleteComment(this.idZahtjeva, idKomentara).subscribe(
      (res) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Komentar uspješno obrisan";
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/profil'], { skipLocationChange: true }).then(() => {
            this.router.navigate([`${this.helper}`])
          })
        }
      },
      (error) => {
       const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
       modalInstance.componentInstance.message = error.error.message;
      }
    )
  }
}
