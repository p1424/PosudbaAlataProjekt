import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Zahtjev } from 'src/app/models/zahtjev';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-zahtjev-details',
  templateUrl: './zahtjev-details.component.html',
  styleUrls: ['./zahtjev-details.component.css']
})
export class ZahtjevDetailsComponent implements OnInit {
  @Input() zahtjevDetails: Zahtjev
  addCommentForm: FormGroup

  constructor(public modal: NgbModal, private formBuilder:FormBuilder, public userService: UserService, private zahtjevService: ZahtjevService, private router: Router, private posudbaService: PosudbaService) {
    this.userService.getUser()

    this.addCommentForm = this.formBuilder.group({
      tekstKomentara: formBuilder.control('', [Validators.required])
    })
  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll();
      }
    });

  }

  addComment(idZahtjeva: any){
    this.zahtjevService.addComment(this.addCommentForm.value, idZahtjeva).subscribe(
      (res)=> {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Uspjeh";
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/'], { skipLocationChange: true }).then(() => {
            this.router.navigate(['/zahtjevi'])
          })
        }

      },
      (error)=> {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  pokaziInteresZaZahtjev(idZahtjeva: any){
    return this.posudbaService.interesZahtjev(idZahtjeva).subscribe(
      (res) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Zahtjev za posudbom uspješno poslan";
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/profil/zahtjevi/interesi'])
        }

      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  control(value: string){
    return this.addCommentForm.get(value)
  }

}
