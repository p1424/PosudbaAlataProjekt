import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtjevDetailsComponent } from './zahtjev-details.component';

describe('ZahtjevDetailsComponent', () => {
  let component: ZahtjevDetailsComponent;
  let fixture: ComponentFixture<ZahtjevDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtjevDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtjevDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
