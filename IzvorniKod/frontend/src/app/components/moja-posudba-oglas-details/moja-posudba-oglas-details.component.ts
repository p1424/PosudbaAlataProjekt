import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PosudbaOglas } from 'src/app/models/posudbaOglas';
import { UserService } from 'src/app/services/userService/user.service';
import { FeedbackComponent } from '../feedback/feedback.component';

@Component({
  selector: 'app-moja-posudba-oglas-details',
  templateUrl: './moja-posudba-oglas-details.component.html',
  styleUrls: ['./moja-posudba-oglas-details.component.css']
})
export class MojaPosudbaOglasDetailsComponent implements OnInit {
  @Input() mojaPosudbaDetails: PosudbaOglas

  constructor(public modal: NgbModal, public userService: UserService) {
    this.userService.getUser()
  }

  ngOnInit(): void {
  }

  zavrsiPosudbu(){
    const modalResult = this.modal.open(FeedbackComponent, {scrollable: true, size: 'lg', centered: true})
    modalResult.componentInstance.details = this.mojaPosudbaDetails
  }
}
