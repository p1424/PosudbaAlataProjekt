import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojaPosudbaOglasDetailsComponent } from './moja-posudba-oglas-details.component';

describe('MojaPosudbaOglasDetailsComponent', () => {
  let component: MojaPosudbaOglasDetailsComponent;
  let fixture: ComponentFixture<MojaPosudbaOglasDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojaPosudbaOglasDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojaPosudbaOglasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
