import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  @Input() details: any

  addFeedbackForm: FormGroup
  addReportForm: FormGroup
  showFeedback: boolean = false
  showReport: boolean = true

  constructor(private posudbaService: PosudbaService, private formBuilder:FormBuilder, private router: Router, private userService: UserService, public modal: NgbModal) {
    this.userService.getUser()

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll();
      }
    });

    this.addFeedbackForm = this.formBuilder.group({
      ocjena: formBuilder.control('', [Validators.required]),
      tekstFeedbacka: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
    })

    this.addReportForm = this.formBuilder.group({
      tekstReporta: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
    })
  }

  ngOnInit(): void {
  }

  addFeedback(){
    if(this.details.idOglasa){
      this.posudbaService.addFeedbackOglas(this.userService.currentUser.userId === this.details.nuditelj.userId ? this.details.posuditelj.userId : this.details.nuditelj.userId, this.addFeedbackForm.value)
      .subscribe(
        (res) => {
          this.posudbaService.povratOglas(this.details.idOglasa).subscribe((response) => this.router.navigate(['/profil']))
        },
        (error) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = error.error.message;
        }
      )

    } else if(this.details.idZahtjeva){
      this.posudbaService.addFeedbackZahtjev(this.userService.currentUser.userId === this.details.trazitelj.userId ? this.details.ponudac.userId : this.details.trazitelj.userId, this.addFeedbackForm.value)
      .subscribe(
        (res) => {
          this.posudbaService.povratZahtjev(this.details.idZahtjeva).subscribe((response) => this.router.navigate(['/profil']))
        },
        (error) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = error.error.message;
        }
      )
    }
  }

  addReport(){
    if(this.details.idOglasa){
      this.posudbaService.addReportOglas(this.userService.currentUser.userId === this.details.nuditelj.userId ? this.details.posuditelj.userId : this.details.nuditelj.userId, this.addReportForm.value)
      .subscribe(
        (res) => {
          this.posudbaService.povratOglas(this.details.idOglasa).subscribe((response) => this.router.navigate(['/profil']))
        },
        (error) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = error.error.message;
        }
      )

    } else if(this.details.idZahtjeva){
      this.posudbaService.addReportZahtjev(this.userService.currentUser.userId === this.details.trazitelj.userId ? this.details.ponudac.userId : this.details.trazitelj.userId, this.addReportForm.value)
      .subscribe(
        (res) => {
          this.posudbaService.povratZahtjev(this.details.idZahtjeva).subscribe((response) => this.router.navigate(['/profil']))
        },
        (error) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = error.error.message;
        }
      )
    }
  }

  controlFeedback(value: string){
    return this.addFeedbackForm.get(value)
  }

  controlReport(value: string){
    return this.addReportForm.get(value)
  }
}
