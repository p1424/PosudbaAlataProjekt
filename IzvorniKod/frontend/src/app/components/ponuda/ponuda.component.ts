import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Korisnik } from 'src/app/models/korisnik';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';
import { ErrorHandlerComponent } from '../error-handler/error-handler.component';

@Component({
  selector: 'app-ponuda',
  templateUrl: './ponuda.component.html',
  styleUrls: ['./ponuda.component.css']
})
export class PonudaComponent implements OnInit {
  @Input() ponuda: Korisnik
  @Input() id: any
  @Input() vrsta: any
  @Input() vlasnikId: any

  constructor(private posudbaService: PosudbaService, public userService: UserService, private router: Router, private modal: NgbModal) {
    this.userService.getUser()

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modal.dismissAll()
      }
    })
  }

  ngOnInit(): void {
  }

  posudiAlat(idDobitnika: any){
    if(this.vrsta === 'zahtjev'){
      this.posudbaService.posudbaZahtjev(this.id, idDobitnika).subscribe(
        (res) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = "Uspjeh";
          modalInstance.componentInstance.afterClose = () => {
            this.router.navigate(['/profil/zahtjevi/posudba'])
          }

        }
      )
    } else if(this.vrsta === 'oglas'){
      this.posudbaService.posudbaOglas(this.id, idDobitnika).subscribe(
        (res) => {
          const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
          modalInstance.componentInstance.message = "Uspjeh";
          modalInstance.componentInstance.afterClose = () => {
            this.router.navigate(['/profil/oglasi/posudba'])
          }
        }
      )
    } else{
      console.log("error")
    }
  }

}
