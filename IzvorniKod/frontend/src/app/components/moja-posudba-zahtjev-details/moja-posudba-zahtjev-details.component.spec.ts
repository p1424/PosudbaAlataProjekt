import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojaPosudbaZahtjevDetailsComponent } from './moja-posudba-zahtjev-details.component';

describe('MojaPosudbaZahtjevDetailsComponent', () => {
  let component: MojaPosudbaZahtjevDetailsComponent;
  let fixture: ComponentFixture<MojaPosudbaZahtjevDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojaPosudbaZahtjevDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojaPosudbaZahtjevDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
