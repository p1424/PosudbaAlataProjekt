import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PosudbaZahtjev } from 'src/app/models/posudbaZahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { FeedbackComponent } from '../feedback/feedback.component';

@Component({
  selector: 'app-moja-posudba-zahtjev-details',
  templateUrl: './moja-posudba-zahtjev-details.component.html',
  styleUrls: ['./moja-posudba-zahtjev-details.component.css']
})
export class MojaPosudbaZahtjevDetailsComponent implements OnInit {
  @Input() mojaPosudbaDetails: PosudbaZahtjev

  constructor(public modal: NgbModal, public userService: UserService) {
    this.userService.getUser()
  }

  ngOnInit(): void {
  }

  zavrsiPosudbu(){
    const modalResult = this.modal.open(FeedbackComponent, {scrollable: true, size: 'lg', centered: true})
    modalResult.componentInstance.details = this.mojaPosudbaDetails
  }
}
