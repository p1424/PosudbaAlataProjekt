import { Component, Input, OnInit } from '@angular/core';
import { Prijava } from 'src/app/models/prijava';

@Component({
  selector: 'app-prijava',
  templateUrl: './prijava.component.html',
  styleUrls: ['./prijava.component.css']
})
export class PrijavaComponent implements OnInit {
  @Input() prijava: Prijava

  constructor() { }

  ngOnInit(): void {
  }

}
