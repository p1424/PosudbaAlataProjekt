import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.css']
})
export class ErrorHandlerComponent implements OnInit {
  @Input() message: string
  @Input() afterClose: Function

  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.afterClose){
      this.afterClose()
    }
  }

}
