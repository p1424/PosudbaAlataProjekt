import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OglasDetailsComponent } from './oglas-details.component';

describe('OglasDetailsComponent', () => {
  let component: OglasDetailsComponent;
  let fixture: ComponentFixture<OglasDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OglasDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OglasDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
