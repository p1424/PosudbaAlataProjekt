import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojFeedbackComponent } from './moj-feedback.component';

describe('MojFeedbackComponent', () => {
  let component: MojFeedbackComponent;
  let fixture: ComponentFixture<MojFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojFeedbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
