import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Feedback } from 'src/app/models/feedback';
import { MojFeedbackDetailsComponent } from '../moj-feedback-details/moj-feedback-details.component';

@Component({
  selector: 'app-moj-feedback',
  templateUrl: './moj-feedback.component.html',
  styleUrls: ['./moj-feedback.component.css']
})
export class MojFeedbackComponent implements OnInit {
  @Input() feedback: Feedback

  constructor(private modal: NgbModal) { }

  ngOnInit(): void {
  }

  public showFeedback(){
    const modalResult = this.modal.open(MojFeedbackDetailsComponent, {size: 'lg', centered: true})
    modalResult.componentInstance.feedbackDetails = this.feedback
  }

}
