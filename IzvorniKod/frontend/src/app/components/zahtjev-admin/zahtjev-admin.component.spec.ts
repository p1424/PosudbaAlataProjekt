import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtjevAdminComponent } from './zahtjev-admin.component';

describe('ZahtjevAdminComponent', () => {
  let component: ZahtjevAdminComponent;
  let fixture: ComponentFixture<ZahtjevAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZahtjevAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtjevAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
