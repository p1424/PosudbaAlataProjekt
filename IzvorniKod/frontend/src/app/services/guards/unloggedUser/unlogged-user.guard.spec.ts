import { TestBed } from '@angular/core/testing';

import { UnloggedUserGuard } from './unlogged-user.guard';

describe('UnloggedUserGuard', () => {
  let guard: UnloggedUserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UnloggedUserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
