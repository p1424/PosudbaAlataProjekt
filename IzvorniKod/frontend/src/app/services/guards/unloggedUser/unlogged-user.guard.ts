import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { UserService } from '../../userService/user.service';

@Injectable({
  providedIn: 'root'
})
export class UnloggedUserGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router, private modal: NgbModal){}

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.userService.isLoggedIn()){
      return true
    }

    const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
    modalInstance.componentInstance.message = "Prijavite se kako biste pristupili ovoj stranici!";

    return this.router.createUrlTree(['/login'])

  }


}
