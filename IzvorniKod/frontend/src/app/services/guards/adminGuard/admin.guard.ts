import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { UserService } from '../../userService/user.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private userService: UserService, private router: Router, private modal:NgbModal){
    this.userService.getUser()
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.userService.isLoggedIn() && this.userService.currentUser.role === 'ADMIN' ){
      return true
    }

    const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
    modalInstance.componentInstance.message = "Nemate dopuštenje za pregled ove stranice!";

    return this.router.createUrlTree([''])
  }

}
