import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { UserData } from 'src/app/models/userData';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  loginUser(data: any){
    return this.http.post<any>(`${environment.baseUrl}/login`, data, httpOptions).pipe(
      map(
        res => {
          delete res.korisnik['password']
          sessionStorage.setItem('user', JSON.stringify(res.korisnik as UserData))
          let token = `Bearer ${res.token}`
          sessionStorage.setItem('token', token)
          return res
        }
      )
    )
  }


}
