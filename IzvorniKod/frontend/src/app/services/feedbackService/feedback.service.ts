import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Feedback } from 'src/app/models/feedback';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
}

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  mojiFeedbackovi: Feedback[]

  constructor(private http: HttpClient) { }

  getUserFeedback(){
    return this.http.get<any>(`${environment.baseUrl}/feedback`, httpOptions)
  }
}
