import { najMajstor } from './../../models/najMajstor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserData } from 'src/app/models/userData';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: UserData
  najMajstor: najMajstor

  constructor(private router: Router, private http: HttpClient) { }

  logout(){
    sessionStorage.removeItem('user')
    sessionStorage.removeItem('token')
    this.router.navigate(['/'])
  }

  isLoggedIn(){
    return sessionStorage.getItem('user') !== null
  }

  getUser(){
    this.currentUser = JSON.parse(sessionStorage.getItem('user')) as UserData
  }

  updateProfile(data: any){
    return this.http.put<any>(`${environment.baseUrl}/users/modify`, data, httpOptions).pipe(
      map(
        res => {
          delete res['password']
          sessionStorage.setItem('user', JSON.stringify(res as UserData))
          return res
        }
      )
    )
  }

  deleteProfile(idKorisnika: any){
    return this.http.delete<any>(`${environment.baseUrl}/users/delete/${idKorisnika}`, httpOptions)
  }

  getNajMajstor(){
    return this.http.get<any>(`${environment.baseUrl}/najMajstor`, httpOptions)
  }
}
