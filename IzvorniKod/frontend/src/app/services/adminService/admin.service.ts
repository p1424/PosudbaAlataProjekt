import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Korisnik } from 'src/app/models/korisnik';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
}

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  korisnici: Korisnik[]

  constructor(private http: HttpClient) { }

  getUsers(){
    return this.http.get<any>(`${environment.baseUrl}/users`, httpOptions)
  }

  getReport(idKorisnika: any){
    return this.http.get<any>(`${environment.baseUrl}/reports/${idKorisnika}`, httpOptions)
  }

  blockUser(idKorisnika: any){
    return this.http.get<any>(`${environment.baseUrl}/user${idKorisnika}/block`, httpOptions)
  }

  unblockUser(idKorisnika: any){
    return this.http.get<any>(`${environment.baseUrl}/user${idKorisnika}/unblock`, httpOptions)
  }

}
