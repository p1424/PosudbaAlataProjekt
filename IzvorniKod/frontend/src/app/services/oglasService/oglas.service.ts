import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Oglas } from 'src/app/models/oglas';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
}

@Injectable({
  providedIn: 'root'
})
export class OglasService {
  oglasi: Oglas[]
  mojiOglasi: VlastitiOglas[]
  interesiOglasi: VlastitiOglas[]
  sviOglasi: Oglas[]

  constructor(private http:HttpClient) { }

  addOglas(data: any){
    return this.http.post<any>(`${environment.baseUrl}/oglas`, data, httpOptions)
  }

  deleteOglas(idOglasa: any){
    return this.http.delete(`${environment.baseUrl}/oglas/delete/${idOglasa}`, httpOptions)
  }

  getUserOglasi(){
    return this.http.get<any>(`${environment.baseUrl}/oglas/korisnik`, httpOptions)
  }

  getInteresiOglasi(){
    return this.http.get<any>(`${environment.baseUrl}/oglas/korisnik/interes`, httpOptions)
  }

  getOglasi(){
    return this.http.get<any>(`${environment.baseUrl}/oglas/active`, httpOptions)
  }

  filterOglasi(filterValue: any){
    return this.http.post<any>(`${environment.baseUrl}/oglas/active`, filterValue, httpOptions)
  }

  getOglas(idOglasa){
    return this.http.get<any>(`${environment.baseUrl}/oglas/${idOglasa}`, httpOptions)
  }

  urediOglas(data: any, idOglasa: any){
    return this.http.put<any>(`${environment.baseUrl}/oglas/modify/${idOglasa}`, data, httpOptions)
  }

  getSviOglasi(){
    return this.http.get<any>(`${environment.baseUrl}/oglas/all`, httpOptions)
  }
}
