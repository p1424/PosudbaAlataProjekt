import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PosudbaOglas } from 'src/app/models/posudbaOglas';
import { PosudbaZahtjev } from 'src/app/models/posudbaZahtjev';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
}

@Injectable({
  providedIn: 'root'
})
export class PosudbaService {
  mojePosudbeZahtjevi: PosudbaZahtjev[]
  mojePosudbeOglasi: PosudbaOglas[]

  constructor(private http: HttpClient) { }

  interesOglas(idOglasa: any){
    return this.http.post<any>(`${environment.baseUrl}/posudba/oglas/${idOglasa}`, httpOptions)
  }

  posudbaOglas(idOglasa: any, idDobitnika: any){
    return this.http.post<any>(`${environment.baseUrl}/posudba/oglas/${idOglasa}/${idDobitnika}`, httpOptions)
  }

  ponistiOglas(idOglasa: any){
    return this.http.get<any>(`${environment.baseUrl}/oglas/interes/cancel/${idOglasa}`, httpOptions)
  }

  addFeedbackOglas(idVlasnik: any, data: any){
    return this.http.post<any>(`${environment.baseUrl}/feedback/${idVlasnik}`, data, httpOptions)
  }

  addReportOglas(idVlasnik: any, data: any){
    return this.http.post<any>(`${environment.baseUrl}/report/${idVlasnik}`, data, httpOptions)
  }

  povratOglas(idOglasa: any){
    return this.http.post<any>(`${environment.baseUrl}/oglas/povrat/${idOglasa}`, httpOptions)
  }

  interesZahtjev(idZahtjeva: any){
    return this.http.post<any>(`${environment.baseUrl}/posudba/zahtjev/${idZahtjeva}`, httpOptions)
  }

  posudbaZahtjev(idZahtjeva: any, idDobitnika: any){
    return this.http.post<any>(`${environment.baseUrl}/posudba/zahtjev/${idZahtjeva}/${idDobitnika}`, httpOptions)
  }

  ponistiZahtjev(idZahtjeva: any){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/interes/cancel/${idZahtjeva}`, httpOptions)
  }

  addFeedbackZahtjev(idVlasnik: any, data: any){
    return this.http.post<any>(`${environment.baseUrl}/feedback/${idVlasnik}`, data, httpOptions)
  }

  addReportZahtjev(idVlasnik: any, data: any){
    return this.http.post<any>(`${environment.baseUrl}/report/${idVlasnik}`, data, httpOptions)
  }

  povratZahtjev(idZahtjeva: any){
    return this.http.post<any>(`${environment.baseUrl}/zahtjev/povrat/${idZahtjeva}`, httpOptions)
  }

  getUserPosudbaZahtjev(){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/korisnik/inactive`, httpOptions)
  }

  getUserPosudbaOglas(){
    return this.http.get<any>(`${environment.baseUrl}/oglas/korisnik/inactive`, httpOptions)
  }
}
