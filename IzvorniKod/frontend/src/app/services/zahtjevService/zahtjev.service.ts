import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Zahtjev } from 'src/app/models/zahtjev';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
}

@Injectable({
  providedIn: 'root'
})
export class ZahtjevService {
  zahtjevi: Zahtjev[]
  mojiZahtjevi: VlastitiZahtjev[]
  interesiZahtjevi: VlastitiZahtjev[]
  sviZahtjevi: Zahtjev[]

  constructor(private http:HttpClient) { }

  addZahtjev(data: any){
    return this.http.post<any>(`${environment.baseUrl}/zahtjev`, data, httpOptions)
  }

  deleteZahtjev(idZahtjeva: any){
    return this.http.delete<any>(`${environment.baseUrl}/zahtjev/delete/${idZahtjeva}`, httpOptions)
  }

  getZahtjev(idZahtjeva: any){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/${idZahtjeva}`, httpOptions)
  }

  getUserZahtjevi(){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/korisnik`, httpOptions)
  }
  
  getInteresiZahtjevi(){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/korisnik/interes`, httpOptions)
  }

  getZahtjevi(filterValue?: any){
    if(filterValue){
      return this.http.post<any>(`${environment.baseUrl}/zahtjev/active`, filterValue, httpOptions)
    }else{
      return this.http.get<any>(`${environment.baseUrl}/zahtjev/active`, httpOptions)
    }
  }

  urediZahtjev(data: any, idZahtjeva: any){
    return this.http.put<any>(`${environment.baseUrl}/zahtjev/modify/${idZahtjeva}`, data, httpOptions)
  }

  addComment(data: any, idZahtjeva: any){
    return this.http.post<any>(`${environment.baseUrl}/zahtjev${idZahtjeva}/komentiraj`, data, httpOptions)
  }

  deleteComment(idZahtjeva: any, idKomentara: any){
    return this.http.delete<any>(`${environment.baseUrl}/zahtjev${idZahtjeva}/komentar${idKomentara}/delete`, httpOptions)
  }

  addLike(idKomentara: any){
    return this.http.get<any>(`${environment.baseUrl}/lajkaj/${idKomentara}`, httpOptions)
  }

  getSviZahtjevi(){
    return this.http.get<any>(`${environment.baseUrl}/zahtjev/all`, httpOptions)
  }
}
