export interface Korisnik{
  userId: number
  username: string
  firstName: string
  lastName: string
  email: string
  phone: string
  address: string
  city: string
  password: string
  role: string
  deleted: boolean
}
