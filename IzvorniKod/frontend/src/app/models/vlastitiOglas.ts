import { Korisnik } from "./korisnik";
import { Oglas } from "./oglas";

export interface VlastitiOglas{
  oglas: Oglas
  zainteresirani?: Korisnik[],
}
