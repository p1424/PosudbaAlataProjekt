import { Alat } from "./alat";
import { Komentar } from "./komentar";
import { Korisnik } from "./korisnik";

export interface PosudbaZahtjev{
  idZahtjeva: number,
  tekstZahtjeva: string,
  ponudac: Korisnik,
  nacinKontakta: string,
  trazitelj: Korisnik,
  active: boolean,
  alat?: Alat,
  fotografija: string,
  tipPosudbe: string,
  listaKomentara: Komentar[],
  povratTrazitelj: boolean,
  povratPonudac: boolean,
}
