import { Alat } from "./alat";
import { Korisnik } from "./korisnik";

export interface PosudbaOglas{
  idOglasa: number,
  tekstOglasa: string,
  nuditelj: Korisnik,
  nacinKontakta: string,
  posuditelj: Korisnik,
  active: boolean,
  alat?: Alat,
  fotografija: string,
  tipPosudbe: string,
  povratNuditelj: boolean,
  povratPosuditelj: boolean,
}
