import { Alat } from "./alat";
import { Komentar } from "./komentar";
import { Korisnik } from "./korisnik";

export interface Zahtjev{
  idZahtjeva: number,
  active: boolean,
  deleted: boolean,
  alat?: Alat,
  datumZahtjeva: string,
  fotografija: string,
  maxUdaljenost: number,
  nacinKontakta: string,
  tekstZahtjeva: string,
  trazitelj: Korisnik,
  ponudac?: Korisnik,
  listaKomentara?: Komentar[]
}
