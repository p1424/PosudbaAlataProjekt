export interface najMajstor {
    username: string,
    address: string,
    city: string,
    ocjenaCount: number,
    ocjenaAVG: number,
}