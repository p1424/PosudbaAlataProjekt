import { Korisnik } from "./korisnik";
import { Lajk } from "./lajk";

export interface Komentar{
  idKomentara: number,
  ostavljaKorisnik: Korisnik,
  datumKomentara: string,
  tekstKomentara: string,
  currentUserLiked: boolean,
  lajkovi: Lajk[]
}
