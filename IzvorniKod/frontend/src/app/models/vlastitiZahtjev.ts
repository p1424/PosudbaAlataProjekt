import { Korisnik } from "./korisnik";
import { Zahtjev } from "./zahtjev";

export interface VlastitiZahtjev{
  zahtjev: Zahtjev,
  listaZainteresiranih?: Korisnik[],
}
