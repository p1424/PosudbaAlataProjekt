export interface Alat{
  idAlata: number
  fotografija: string
  poveznicaNaProizvod: string
  snaga: number
  kategorija: string
  vrsta: string
  podvrsta: string
  mjestoPrimjene: string
  akumulatorski: boolean
}
