import { Korisnik } from "./korisnik";

export interface Prijava {
  idReporta: number,
  datumReporta: string,
  tekstReporta: string,
  zaKorisnika: Korisnik,
  odKorisnika: Korisnik
}
