export interface UserData{
  userId: number
  firstName: string
  lastName: string
  username: string
  email: string
  address: string
  city: string
  phone: string
  role: string
  accountNonExpired: boolean
  accountNonLocked: boolean
  credentialsNonExpired: boolean
}
