import { Korisnik } from "./korisnik";

export interface Feedback{
  feedbackId: number,
  datumFeedbacka: string,
  tekstFeedbacka: string,
  ocjena: number,
  zaKorisnika: Korisnik,
  odKorisnika: Korisnik,
}
