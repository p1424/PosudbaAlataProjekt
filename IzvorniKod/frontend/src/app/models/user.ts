export interface User{
  userId: number
  firstName: string
  lastName: string
  username: string
  email: string
  address: string
  city: string
  phone: number
  password: string
}
