import { Alat } from "./alat";
import { Korisnik } from "./korisnik";

export interface Oglas{
  idOglasa: number,
  datumOglasa: string,
  tekstOglasa: string,
  alat: Alat,
  nuditelj: Korisnik,
  nacinKontakta: string,
  posuditelj?: Korisnik,
  active: boolean,
  deleted: boolean,
}
