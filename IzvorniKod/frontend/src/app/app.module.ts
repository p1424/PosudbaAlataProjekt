import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './shared/header/header.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './pages/home/home.component';
import { InterceptorService } from './services/interceptorService/interceptor.service';
import { OglasComponent } from './components/oglas/oglas.component';
import { OglasDetailsComponent } from './components/oglas-details/oglas-details.component';
import { ZahtjevComponent } from './components/zahtjev/zahtjev.component';
import { ZahtjevDetailsComponent } from './components/zahtjev-details/zahtjev-details.component';
import { OglasiComponent } from './pages/oglasi/oglasi.component';
import { AddOglasComponent } from './pages/add-oglas/add-oglas.component';
import { KomentarComponent } from './components/komentar/komentar.component';
import { AddZahtjevComponent } from './pages/add-zahtjev/add-zahtjev.component';
import { ZahtjeviComponent } from './pages/zahtjevi/zahtjevi.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { UrediProfilComponent } from './pages/uredi-profil/uredi-profil.component';
import { MojiOglasiComponent } from './pages/moji-oglasi/moji-oglasi.component';
import { MojiZahtjeviComponent } from './pages/moji-zahtjevi/moji-zahtjevi.component';
import { MojOglasComponent } from './components/moj-oglas/moj-oglas.component';
import { MojOglasDetailsComponent } from './components/moj-oglas-details/moj-oglas-details.component';
import { MojZahtjevComponent } from './components/moj-zahtjev/moj-zahtjev.component';
import { MojZahtjevDetailsComponent } from './components/moj-zahtjev-details/moj-zahtjev-details.component';
import { PonudaComponent } from './components/ponuda/ponuda.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { UrediZahtjevComponent } from './pages/uredi-zahtjev/uredi-zahtjev.component';
import { UrediOglasComponent } from './pages/uredi-oglas/uredi-oglas.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PosudbeZahtjeviComponent } from './pages/posudbe-zahtjevi/posudbe-zahtjevi.component';
import { PosudbeOglasiComponent } from './pages/posudbe-oglasi/posudbe-oglasi.component';
import { MojaPosudbaOglasComponent } from './components/moja-posudba-oglas/moja-posudba-oglas.component';
import { MojaPosudbaOglasDetailsComponent } from './components/moja-posudba-oglas-details/moja-posudba-oglas-details.component';
import { MojaPosudbaZahtjevComponent } from './components/moja-posudba-zahtjev/moja-posudba-zahtjev.component';
import { MojaPosudbaZahtjevDetailsComponent } from './components/moja-posudba-zahtjev-details/moja-posudba-zahtjev-details.component';
import { KorisniciComponent } from './pages/korisnici/korisnici.component';
import { KorisnikComponent } from './components/korisnik/korisnik.component';
import { KorisnikDetailsComponent } from './components/korisnik-details/korisnik-details.component';
import { PrijavaComponent } from './components/prijava/prijava.component';
import { MojiFeedbackoviComponent } from './pages/moji-feedbackovi/moji-feedbackovi.component';
import { MojFeedbackComponent } from './components/moj-feedback/moj-feedback.component';
import { MojFeedbackDetailsComponent } from './components/moj-feedback-details/moj-feedback-details.component';
import { ErrorHandlerComponent } from './components/error-handler/error-handler.component';
import { SviZahtjeviComponent } from './pages/svi-zahtjevi/svi-zahtjevi.component';
import { ZahtjevAdminComponent } from './components/zahtjev-admin/zahtjev-admin.component';
import { ZahtjevAdminDetailsComponent } from './components/zahtjev-admin-details/zahtjev-admin-details.component';
import { SviOglasiComponent } from './pages/svi-oglasi/svi-oglasi.component';
import { OglasAdminComponent } from './components/oglas-admin/oglas-admin.component';
import { OglasAdminDetailsComponent } from './components/oglas-admin-details/oglas-admin-details.component';
import { InteresiOglasiComponent } from './pages/interesi-oglasi/interesi-oglasi.component';
import { InteresiZahtjeviComponent } from './pages/interesi-zahtjevi/interesi-zahtjevi.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    OglasComponent,
    OglasDetailsComponent,
    ZahtjevComponent,
    ZahtjevDetailsComponent,
    OglasiComponent,
    AddOglasComponent,
    ZahtjeviComponent,
    KomentarComponent,
    AddZahtjevComponent,
    ProfilePageComponent,
    UrediProfilComponent,
    MojiOglasiComponent,
    MojiZahtjeviComponent,
    MojOglasComponent,
    MojOglasDetailsComponent,
    MojZahtjevComponent,
    MojZahtjevDetailsComponent,
    PonudaComponent,
    FeedbackComponent,
    UrediZahtjevComponent,
    UrediOglasComponent,
    PosudbeZahtjeviComponent,
    PosudbeOglasiComponent,
    MojaPosudbaOglasComponent,
    MojaPosudbaOglasDetailsComponent,
    MojaPosudbaZahtjevComponent,
    MojaPosudbaZahtjevDetailsComponent,
    KorisniciComponent,
    KorisnikComponent,
    KorisnikDetailsComponent,
    PrijavaComponent,
    MojiFeedbackoviComponent,
    MojFeedbackComponent,
    MojFeedbackDetailsComponent,
    ErrorHandlerComponent,
    SviZahtjeviComponent,
    ZahtjevAdminComponent,
    ZahtjevAdminDetailsComponent,
    SviOglasiComponent,
    OglasAdminComponent,
    OglasAdminDetailsComponent,
    InteresiOglasiComponent,
    InteresiZahtjeviComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
