import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: User

  constructor(public userService: UserService, private router: Router) { }

  ngOnInit(): void {
    if(this.userService.isLoggedIn()){
      this.currentUser = JSON.parse(sessionStorage.getItem('user'))
    }
  }

  public redirectLogin(){
    this.router.navigate(['/login'])
  }

  public redirectRegister(){
    this.router.navigate(['/register'])
  }

  public changeState(){
    document.getElementById('dropdown-content').classList.toggle('show')
  }

  public logout(){
    this.userService.logout()
  }

  public showNav(){
    document.getElementById("navbar").classList.toggle('responsive')
  }

  public changeStateResponsive(){
    document.getElementById('dropdown-content-responsive').classList.toggle('show')
  }
}
