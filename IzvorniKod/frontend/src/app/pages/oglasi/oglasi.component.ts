import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Oglas } from 'src/app/models/oglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-oglasi',
  templateUrl: './oglasi.component.html',
  styleUrls: ['./oglasi.component.css']
})
export class OglasiComponent implements OnInit {

  filterForm: FormGroup;

  constructor(private formBuilder:FormBuilder, public oglasService: OglasService, private modal: NgbModal, public userService: UserService) {

    this.filterForm = this.formBuilder.group({
      akumulatorski: formBuilder.control(null),
      kategorijaAlata: formBuilder.control(null),
      mjestoPrimjeneAlata: formBuilder.control(null),
      vrsta: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      maxSnaga: formBuilder.control(0, [Validators.required]),
      maxUdaljenost: formBuilder.control(0, [Validators.required]),
      nacinKontakta: formBuilder.control(null),
    })
  }

  ngOnInit(): void {
    this.oglasService.getOglasi().subscribe(
      (res) => {
        this.oglasService.oglasi = res as Oglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }

    )
  }

  filter(){
    this.oglasService.filterOglasi(this.filterForm.value).subscribe(
      (res) => {
        this.oglasService.oglasi = res as Oglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  control(value: string){
    return this.filterForm.get(value)
  }

}
