import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Korisnik } from 'src/app/models/korisnik';
import { AdminService } from 'src/app/services/adminService/admin.service';

@Component({
  selector: 'app-korisnici',
  templateUrl: './korisnici.component.html',
  styleUrls: ['./korisnici.component.css']
})
export class KorisniciComponent implements OnInit {

  constructor(public adminService: AdminService, private modal: NgbModal) { }

  ngOnInit(): void {
    this.adminService.getUsers().subscribe(
      (res) => {
        this.adminService.korisnici = res as Korisnik[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }

    )
  }

}
