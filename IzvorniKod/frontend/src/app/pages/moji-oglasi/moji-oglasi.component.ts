import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-moji-oglasi',
  templateUrl: './moji-oglasi.component.html',
  styleUrls: ['./moji-oglasi.component.css']
})
export class MojiOglasiComponent implements OnInit {

  constructor(public oglasService: OglasService, private userService: UserService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {

    this.oglasService.getUserOglasi().subscribe(
      (res) => {
        this.oglasService.mojiOglasi = res as VlastitiOglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
