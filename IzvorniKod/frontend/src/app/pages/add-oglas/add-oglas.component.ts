import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { OglasService } from 'src/app/services/oglasService/oglas.service';

@Component({
  selector: 'app-add-oglas',
  templateUrl: './add-oglas.component.html',
  styleUrls: ['./add-oglas.component.css']
})
export class AddOglasComponent implements OnInit {

  addOglasForm: FormGroup
  errorMsg: string

  constructor(private formBuilder:FormBuilder, private router: Router, private oglasService: OglasService, private modal: NgbModal) {
    this.addOglasForm = this.formBuilder.group({
      nazivAlata: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      fotoUrl: formBuilder.control(''),
      poveznicaNaProizvod: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      snagaAlata: formBuilder.control('', [Validators.required]),
      isAkumulatorski: formBuilder.control('', [Validators.required]),
      kategorijaAlata: formBuilder.control(null, [Validators.required]),
      podvrsta: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      mjestoPrimjeneAlata: formBuilder.control(null, [Validators.required]),
      opisOglasa: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      nacinKontakta: formBuilder.control(null, [Validators.required])
    })
  }

  ngOnInit(): void {
  }

  makeOglas(){
    this.oglasService.addOglas(this.addOglasForm.value).subscribe(
    (res) =>{
      const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
      modalInstance.componentInstance.message = "Oglas uspješno napravljen!";
      modalInstance.componentInstance.afterClose = () => {
        this.router.navigate(['/profil/mojiOglasi'])
      }

    },
    (error) =>{
      this.errorMsg = error.error.message
    })
  }

  control(value: string){
    return this.addOglasForm.get(value)
  }
}
