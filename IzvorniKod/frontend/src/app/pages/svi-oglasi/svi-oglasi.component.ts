import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Oglas } from 'src/app/models/oglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';

@Component({
  selector: 'app-svi-oglasi',
  templateUrl: './svi-oglasi.component.html',
  styleUrls: ['./svi-oglasi.component.css']
})
export class SviOglasiComponent implements OnInit {

  constructor(public oglasService: OglasService, private modal: NgbModal) { }

  ngOnInit(): void {
    this.oglasService.getSviOglasi().subscribe(
      (res) => {
        this.oglasService.sviOglasi = res as Oglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message
      }
    )
  }

}
