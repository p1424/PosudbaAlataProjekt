import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrediOglasComponent } from './uredi-oglas.component';

describe('UrediOglasComponent', () => {
  let component: UrediOglasComponent;
  let fixture: ComponentFixture<UrediOglasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrediOglasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrediOglasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
