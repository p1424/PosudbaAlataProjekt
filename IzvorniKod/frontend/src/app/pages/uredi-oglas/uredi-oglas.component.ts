import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';

@Component({
  selector: 'app-uredi-oglas',
  templateUrl: './uredi-oglas.component.html',
  styleUrls: ['./uredi-oglas.component.css']
})
export class UrediOglasComponent implements OnInit {

  oglas: VlastitiOglas
  urediOglasForm: FormGroup
  errorMsg: string
  idOglas: string

  constructor(private formBuilder:FormBuilder, private router: Router, private oglasService: OglasService, private route: ActivatedRoute, private modal: NgbModal) {
    this.idOglas = this.route.snapshot.paramMap.get('idOglasa')

    this.oglasService.getOglas(this.idOglas).subscribe(
      (res) => {
        this.oglas = res as VlastitiOglas

        this.urediOglasForm.patchValue({
          nazivAlata: this.oglas.oglas.alat.vrsta,
          fotoUrl: this.oglas.oglas.alat.fotografija ? this.oglas.oglas.alat.fotografija : "",
          poveznicaNaProizvod: this.oglas.oglas.alat.poveznicaNaProizvod,
          snagaAlata: this.oglas.oglas.alat.snaga,
          isAkumulatorski: this.oglas.oglas.alat.akumulatorski.toString(),
          kategorijaAlata: this.oglas.oglas.alat.kategorija,
          podvrsta: this.oglas.oglas.alat.podvrsta,
          mjestoPrimjeneAlata: this.oglas.oglas.alat.mjestoPrimjene,
          opisOglasa: this.oglas.oglas.tekstOglasa,
          nacinKontakta: this.oglas.oglas.nacinKontakta,
        })
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/profil'])
        }
      }
    )

    this.urediOglasForm = this.formBuilder.group({
      nazivAlata: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      fotoUrl: formBuilder.control(''),
      poveznicaNaProizvod: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      snagaAlata: formBuilder.control('', [Validators.required]),
      isAkumulatorski: formBuilder.control('', [Validators.required]),
      kategorijaAlata: formBuilder.control(null, [Validators.required]),
      podvrsta: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      mjestoPrimjeneAlata: formBuilder.control(null, [Validators.required]),
      opisOglasa: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      nacinKontakta: formBuilder.control(null, [Validators.required])
    })
  }

  ngOnInit(): void {
  }

  urediOglas(){
    this.oglasService.urediOglas(this.urediOglasForm.value, this.idOglas).subscribe(
    (res) =>{
      const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
      modalInstance.componentInstance.message = "Oglas uspješno uređen!";
      modalInstance.componentInstance.afterClose = () => {
        this.router.navigate(['/profil/mojiOglasi'])
      }

    },
    (error) =>{
      this.errorMsg = error.error.message
    })
  }

  control(value: string){
    return this.urediOglasForm.get(value)
  }

}
