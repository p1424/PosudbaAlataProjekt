import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-moji-zahtjevi',
  templateUrl: './moji-zahtjevi.component.html',
  styleUrls: ['./moji-zahtjevi.component.css']
})
export class MojiZahtjeviComponent implements OnInit {

  constructor(public zahtjevService:ZahtjevService, private userService: UserService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {

    this.zahtjevService.getUserZahtjevi().subscribe(
      (res) => {
        this.zahtjevService.mojiZahtjevi = res as VlastitiZahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
