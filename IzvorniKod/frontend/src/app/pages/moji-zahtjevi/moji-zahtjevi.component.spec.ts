import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojiZahtjeviComponent } from './moji-zahtjevi.component';

describe('MojiZahtjeviComponent', () => {
  let component: MojiZahtjeviComponent;
  let fixture: ComponentFixture<MojiZahtjeviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojiZahtjeviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojiZahtjeviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
