import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-uredi-zahtjev',
  templateUrl: './uredi-zahtjev.component.html',
  styleUrls: ['./uredi-zahtjev.component.css']
})
export class UrediZahtjevComponent implements OnInit {

  zahtjev: VlastitiZahtjev
  urediZahtjevForm: FormGroup
  errorMsg: string
  idZahtjev: string

  constructor(private formBuilder: FormBuilder, private router: Router, private zahtjevService: ZahtjevService, private route: ActivatedRoute, private modal:NgbModal) {
    this.idZahtjev = this.route.snapshot.paramMap.get('idZahtjeva')
    this.zahtjevService.getZahtjev(this.idZahtjev).subscribe(
      (res) => {
        this.zahtjev = res as VlastitiZahtjev

        if(this.zahtjev.zahtjev.alat) {
          this.urediZahtjevForm.patchValue({
            vrsta: this.zahtjev.zahtjev.alat.vrsta ? this.zahtjev.zahtjev.alat.vrsta : "",
            fotografija: this.zahtjev.zahtjev.alat.fotografija ? this.zahtjev.zahtjev.alat.fotografija : "",
            poveznicaNaProizvod: this.zahtjev.zahtjev.alat.poveznicaNaProizvod ? this.zahtjev.zahtjev.alat.poveznicaNaProizvod : "",
            snaga: this.zahtjev.zahtjev.alat.snaga ? this.zahtjev.zahtjev.alat.snaga : "",
            akumulatorski: this.zahtjev.zahtjev.alat.akumulatorski ? this.zahtjev.zahtjev.alat.akumulatorski.toString() : null,
            kategorijaAlata: this.zahtjev.zahtjev.alat.kategorija ? this.zahtjev.zahtjev.alat.kategorija : null,
            podvrsta: this.zahtjev.zahtjev.alat.podvrsta ? this.zahtjev.zahtjev.alat.podvrsta : "",
            mjestoPrimjeneAlata: this.zahtjev.zahtjev.alat.mjestoPrimjene ? this.zahtjev.zahtjev.alat.mjestoPrimjene : null,
          })
        }


        this.urediZahtjevForm.patchValue({
          tekstZahtjeva: this.zahtjev.zahtjev.tekstZahtjeva,
          nacinKontakta: this.zahtjev.zahtjev.nacinKontakta,
          maxUdaljenost: this.zahtjev.zahtjev.maxUdaljenost,
        })
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/profil'])
        }
      }
    )

    this.urediZahtjevForm = this.formBuilder.group({
      vrsta: formBuilder.control(''),
      fotografija: formBuilder.control(''),
      poveznicaNaProizvod: formBuilder.control(''),
      snaga: formBuilder.control(''),
      akumulatorski: formBuilder.control(''),
      kategorijaAlata: formBuilder.control(null),
      podvrsta: formBuilder.control(''),
      mjestoPrimjeneAlata: formBuilder.control(null),
      tekstZahtjeva: formBuilder.control('', [Validators.required]),
      nacinKontakta: formBuilder.control(null, [Validators.required]),
      maxUdaljenost: formBuilder.control('', [Validators.required]),
    })
  }

  ngOnInit(): void {
  }

  urediZahtjev(){
    this.zahtjevService.urediZahtjev(this.urediZahtjevForm.value, this.idZahtjev).subscribe(
    (res) =>{
      const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
      modalInstance.componentInstance.message = "Zahtjev uspješno uređen!";
      modalInstance.componentInstance.afterClose = () => {
        this.router.navigate(['/profil/mojiZahtjevi'])
      }
    },
    (error) =>{
      this.errorMsg = error.error.message
    })
  }

  control(value: string){
    return this.urediZahtjevForm.get(value)
  }

}
