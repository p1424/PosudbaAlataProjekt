import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrediZahtjevComponent } from './uredi-zahtjev.component';

describe('UrediZahtjevComponent', () => {
  let component: UrediZahtjevComponent;
  let fixture: ComponentFixture<UrediZahtjevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrediZahtjevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrediZahtjevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
