import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/registerService/register.service';
import { Router } from '@angular/router';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent{

  registerForm: FormGroup
  errorMsg: string

  constructor(private formBuilder: FormBuilder, private registerService: RegisterService, private router:Router, private modal: NgbModal) {
    this.registerForm = this.formBuilder.group({
      firstName: formBuilder.control('', [Validators.required, Validators.maxLength(32), Validators.pattern('[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*')]),
      lastName: formBuilder.control('', [Validators.required, Validators.maxLength(32), Validators.pattern('[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*')]),
      username: formBuilder.control('', [Validators.required, Validators.minLength(3), Validators.maxLength(32), Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      email: formBuilder.control('', [Validators.required, Validators.maxLength(32), Validators.email, Validators.pattern('[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*')]),
      address: formBuilder.control('', [Validators.required]),
      city: formBuilder.control('', [Validators.required]),
      phone: formBuilder.control('', [Validators.required, Validators.pattern('([\\+]|\\d)\\d{7,12}')]),
      password: formBuilder.control('', [Validators.required, Validators.minLength(8)]),
    })
   }

  public onSubmit(){
    this.registerService.registerUser(this.registerForm.value).subscribe((res) => {
      const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
      modalInstance.componentInstance.message = "Uspješno ste registrirani.";
      modalInstance.componentInstance.afterClose = () => {
        this.router.navigate(['/login'])
      }

    },
      (error) =>{
      this.errorMsg = error.error.message
    })
  }

  public control(value: string){
    return this.registerForm.get(value)
  }
}
