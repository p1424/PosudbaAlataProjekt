import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiZahtjev } from 'src/app/models/vlastitiZahtjev';
import { UserService } from 'src/app/services/userService/user.service';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-interesi-zahtjevi',
  templateUrl: './interesi-zahtjevi.component.html',
  styleUrls: ['./interesi-zahtjevi.component.css']
})
export class InteresiZahtjeviComponent implements OnInit {

  constructor(public zahtjevService:ZahtjevService, private userService: UserService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {

    this.zahtjevService.getInteresiZahtjevi().subscribe(
      (res) => {
        this.zahtjevService.interesiZahtjevi = res as VlastitiZahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
