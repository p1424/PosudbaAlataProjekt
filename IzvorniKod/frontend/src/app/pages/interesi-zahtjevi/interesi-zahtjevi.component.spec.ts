import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteresiZahtjeviComponent } from './interesi-zahtjevi.component';

describe('InteresiZahtjeviComponent', () => {
  let component: InteresiZahtjeviComponent;
  let fixture: ComponentFixture<InteresiZahtjeviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteresiZahtjeviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteresiZahtjeviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
