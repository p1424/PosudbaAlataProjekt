import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-uredi-profil',
  templateUrl: './uredi-profil.component.html',
  styleUrls: ['./uredi-profil.component.css']
})
export class UrediProfilComponent implements OnInit {

  updateUserForm: FormGroup
  errorMsg: string

  constructor(private formBuilder: FormBuilder, public userService: UserService, private router:Router, private modal: NgbModal) {

    this.userService.getUser()

    this.updateUserForm = this.formBuilder.group({
      firstName: formBuilder.control('', [Validators.required, Validators.maxLength(32), Validators.pattern('[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*')]),
      lastName: formBuilder.control('', [Validators.required, Validators.maxLength(32), Validators.pattern('[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*')]),
      address: formBuilder.control('', [Validators.required, Validators.maxLength(64), Validators.pattern('[A-Za-zĆ-ž^.]([A-Za-zĆ-ž ^.]+| )*\\d{1,4}')]),
      city: formBuilder.control('', [Validators.required, Validators.pattern('[A-Za-zĆ-ž^.]([A-Za-zĆ-ž ^.]+| )*')]),
      phone: formBuilder.control('', [Validators.required, Validators.pattern('([\\+]|\\d)\\d{7,12}')]),
      password: formBuilder.control('', [Validators.required, Validators.minLength(8)]),
    })

    this.updateUserForm.patchValue({
      firstName: this.userService.currentUser.firstName,
      lastName: this.userService.currentUser.lastName,
      address: this.userService.currentUser.address,
      city: this.userService.currentUser.city,
      phone: this.userService.currentUser.phone,
    })
   }

  ngOnInit(): void {
  }

  public urediProfil(){

    this.userService.updateProfile(this.updateUserForm.value).subscribe(
      (res) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = "Uspješno ste promijenili profil.";
        modalInstance.componentInstance.afterClose = () => {
          this.router.navigate(['/profil'])
        }
      },
      (error) => {
        this.errorMsg = error.error.message
      }
    )
  }

  public deleteProfile(){
    this.userService.deleteProfile(this.userService.currentUser.userId).subscribe(
      (res) => {
        this.userService.logout()
        this.router.navigate([''])
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  public control(value: string){
    return this.updateUserForm.get(value)
  }
}
