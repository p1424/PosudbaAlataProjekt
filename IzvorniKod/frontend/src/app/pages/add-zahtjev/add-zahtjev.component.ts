import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-add-zahtjev',
  templateUrl: './add-zahtjev.component.html',
  styleUrls: ['./add-zahtjev.component.css']
})
export class AddZahtjevComponent implements OnInit {

  addZahtjevForm: FormGroup
  errorMsg: string

  constructor(private formBuilder: FormBuilder, private router: Router, private zahtjevService: ZahtjevService, private modal: NgbModal) {
    this.addZahtjevForm = this.formBuilder.group({
      vrsta: formBuilder.control(''),
      poveznicaNaProizvod: formBuilder.control(''),
      snaga: formBuilder.control(''),
      akumulatorski: formBuilder.control(''),
      kategorijaAlata: formBuilder.control(null),
      podvrsta: formBuilder.control(''),
      mjestoPrimjeneAlata: formBuilder.control(null),
      tekstZahtjeva: formBuilder.control('', [Validators.required]),
      nacinKontakta: formBuilder.control(null, [Validators.required]),
      savjet: formBuilder.control(''),
      maxUdaljenost: formBuilder.control('', [Validators.required]),
      fotografija: formBuilder.control('')
    })
   }

  ngOnInit(): void {
  }

  makeZahtjev(){
    this.addZahtjevForm.removeControl('savjet')
    this.zahtjevService.addZahtjev(this.addZahtjevForm.value).subscribe(
    (res) =>{
      this.addZahtjevForm.addControl('savjet', this.formBuilder.control(''))
      const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
      modalInstance.componentInstance.message = "Zahtjev uspješno napravljen!";
      modalInstance.componentInstance.afterClose = () => {
        this.router.navigate(['/profil/mojiZahtjevi'])
      }

    },
    (error) =>{
      this.addZahtjevForm.addControl('savjet', this.formBuilder.control(''))
      this.errorMsg = error.error.message
    })
  }

  control(value: string){
    return this.addZahtjevForm.get(value)
  }

}
