import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { VlastitiOglas } from 'src/app/models/vlastitiOglas';
import { OglasService } from 'src/app/services/oglasService/oglas.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-interesi-oglasi',
  templateUrl: './interesi-oglasi.component.html',
  styleUrls: ['./interesi-oglasi.component.css']
})
export class InteresiOglasiComponent implements OnInit {

  constructor(public oglasService: OglasService, private userService: UserService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.oglasService.getInteresiOglasi().subscribe(
      (res) => {
        this.oglasService.interesiOglasi = res as VlastitiOglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
