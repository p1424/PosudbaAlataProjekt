import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteresiOglasiComponent } from './interesi-oglasi.component';

describe('InteresiOglasiComponent', () => {
  let component: InteresiOglasiComponent;
  let fixture: ComponentFixture<InteresiOglasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteresiOglasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteresiOglasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
