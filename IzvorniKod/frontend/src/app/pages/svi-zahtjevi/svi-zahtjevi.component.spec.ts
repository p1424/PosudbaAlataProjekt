import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SviZahtjeviComponent } from './svi-zahtjevi.component';

describe('SviZahtjeviComponent', () => {
  let component: SviZahtjeviComponent;
  let fixture: ComponentFixture<SviZahtjeviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SviZahtjeviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SviZahtjeviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
