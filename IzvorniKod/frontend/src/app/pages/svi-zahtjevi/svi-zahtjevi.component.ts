import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Zahtjev } from 'src/app/models/zahtjev';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-svi-zahtjevi',
  templateUrl: './svi-zahtjevi.component.html',
  styleUrls: ['./svi-zahtjevi.component.css']
})
export class SviZahtjeviComponent implements OnInit {

  constructor(public zahtjevService: ZahtjevService, private modal: NgbModal) { }

  ngOnInit(): void {
    this.zahtjevService.getSviZahtjevi().subscribe(
      (res) => {
        this.zahtjevService.sviZahtjevi = res as Zahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message
      }
    )
  }

}
