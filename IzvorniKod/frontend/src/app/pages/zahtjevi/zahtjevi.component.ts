import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Zahtjev } from 'src/app/models/zahtjev';
import { ZahtjevService } from 'src/app/services/zahtjevService/zahtjev.service';

@Component({
  selector: 'app-zahtjevi',
  templateUrl: './zahtjevi.component.html',
  styleUrls: ['./zahtjevi.component.css']
})
export class ZahtjeviComponent implements OnInit {

  filterForm: FormGroup;

  constructor(private formBuilder:FormBuilder, public zahtjevService: ZahtjevService, private modal: NgbModal) {
    this.filterForm = this.formBuilder.group({
      akumulatorski: formBuilder.control(null),
      kategorijaAlata: formBuilder.control(null),
      mjestoPrimjeneAlata: formBuilder.control(null),
      vrsta: formBuilder.control('', [Validators.pattern('[A-Za-zĆ-ž\\d][^\'\"]*')]),
      snaga: formBuilder.control(0, [Validators.required]),
      nacinKontakta: formBuilder.control(null),
    })
  }

  ngOnInit(): void {
    this.zahtjevService.getZahtjevi().subscribe(
      (res) => {
        this.zahtjevService.zahtjevi = res as Zahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  filter() {
    this.zahtjevService.getZahtjevi(this.filterForm.value).subscribe(
      (res) =>{
        this.zahtjevService.zahtjevi = res as Zahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

  control(value: string){
    return this.filterForm.get(value)
  }

}
