import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MojiFeedbackoviComponent } from './moji-feedbackovi.component';

describe('MojiFeedbackoviComponent', () => {
  let component: MojiFeedbackoviComponent;
  let fixture: ComponentFixture<MojiFeedbackoviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MojiFeedbackoviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MojiFeedbackoviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
