import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { Feedback } from 'src/app/models/feedback';
import { FeedbackService } from 'src/app/services/feedbackService/feedback.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-moji-feedbackovi',
  templateUrl: './moji-feedbackovi.component.html',
  styleUrls: ['./moji-feedbackovi.component.css']
})
export class MojiFeedbackoviComponent implements OnInit {

  constructor(public feedbackService: FeedbackService, private userService: UserService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.feedbackService.getUserFeedback().subscribe(
      (res) => {
        this.feedbackService.mojiFeedbackovi = res as Feedback[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
