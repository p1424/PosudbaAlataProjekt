import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosudbeOglasiComponent } from './posudbe-oglasi.component';

describe('PosudbeOglasiComponent', () => {
  let component: PosudbeOglasiComponent;
  let fixture: ComponentFixture<PosudbeOglasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosudbeOglasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosudbeOglasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
