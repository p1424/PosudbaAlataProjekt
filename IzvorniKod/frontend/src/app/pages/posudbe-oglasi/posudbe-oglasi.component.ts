import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { PosudbaOglas } from 'src/app/models/posudbaOglas';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-posudbe-oglasi',
  templateUrl: './posudbe-oglasi.component.html',
  styleUrls: ['./posudbe-oglasi.component.css']
})
export class PosudbeOglasiComponent implements OnInit {

  constructor(private userService: UserService, public posudbaService: PosudbaService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.posudbaService.getUserPosudbaOglas().subscribe(
      (res) => {
        this.posudbaService.mojePosudbeOglasi = res as PosudbaOglas[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
