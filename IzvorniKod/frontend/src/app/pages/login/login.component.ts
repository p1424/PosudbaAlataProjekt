import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/loginService/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  errorMsg: string

  constructor(private formBuilder:FormBuilder, private loginService: LoginService, private router: Router) {
    this.loginForm = this.formBuilder.group({
      username: formBuilder.control('', [Validators.required, Validators.minLength(3), Validators.maxLength(32)]),
      password: formBuilder.control('', [Validators.required, Validators.minLength(8)])
    })
  }

  ngOnInit(): void {
  }

  public onSubmit(){
    this.loginService.loginUser(this.loginForm.value).subscribe(
    (res) =>{
      this.router.navigate(['/'])
    },
    (error) =>{
      this.errorMsg = error.error.message
    })
  }

  public control(value: string){
    return this.loginForm.get(value)
  }
}
