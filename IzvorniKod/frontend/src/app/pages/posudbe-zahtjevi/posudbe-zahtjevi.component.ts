import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorHandlerComponent } from 'src/app/components/error-handler/error-handler.component';
import { PosudbaZahtjev } from 'src/app/models/posudbaZahtjev';
import { PosudbaService } from 'src/app/services/posudbaService/posudba.service';
import { UserService } from 'src/app/services/userService/user.service';

@Component({
  selector: 'app-posudbe-zahtjevi',
  templateUrl: './posudbe-zahtjevi.component.html',
  styleUrls: ['./posudbe-zahtjevi.component.css']
})
export class PosudbeZahtjeviComponent implements OnInit {

  constructor(private userService: UserService, public posudbaService: PosudbaService, private modal: NgbModal) {
    this.userService.getUser()
  }

  ngOnInit(): void {
    this.posudbaService.getUserPosudbaZahtjev().subscribe(
      (res) => {
        this.posudbaService.mojePosudbeZahtjevi = res as PosudbaZahtjev[]
      },
      (error) => {
        const modalInstance = this.modal.open(ErrorHandlerComponent, {size: 'sm', centered: true})
        modalInstance.componentInstance.message = error.error.message;
      }
    )
  }

}
