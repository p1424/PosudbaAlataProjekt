import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosudbeZahtjeviComponent } from './posudbe-zahtjevi.component';

describe('PosudbeZahtjeviComponent', () => {
  let component: PosudbeZahtjeviComponent;
  let fixture: ComponentFixture<PosudbeZahtjeviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosudbeZahtjeviComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosudbeZahtjeviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
