import { InteresiZahtjeviComponent } from './pages/interesi-zahtjevi/interesi-zahtjevi.component';
import { InteresiOglasiComponent } from './pages/interesi-oglasi/interesi-oglasi.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OglasiComponent } from './pages/oglasi/oglasi.component';
import { ZahtjeviComponent } from './pages/zahtjevi/zahtjevi.component';
import { AddOglasComponent } from './pages/add-oglas/add-oglas.component';
import { AddZahtjevComponent } from './pages/add-zahtjev/add-zahtjev.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { MojiOglasiComponent } from './pages/moji-oglasi/moji-oglasi.component';
import { MojiZahtjeviComponent } from './pages/moji-zahtjevi/moji-zahtjevi.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { RegisterComponent } from './pages/register/register.component';
import { UrediProfilComponent } from './pages/uredi-profil/uredi-profil.component';
import { AuthenticationGuard } from './services/guards/auth/authentication.guard';
import { UnloggedUserGuard } from './services/guards/unloggedUser/unlogged-user.guard';
import { PosudbeZahtjeviComponent } from './pages/posudbe-zahtjevi/posudbe-zahtjevi.component';
import { PosudbeOglasiComponent } from './pages/posudbe-oglasi/posudbe-oglasi.component';
import { UrediZahtjevComponent } from './pages/uredi-zahtjev/uredi-zahtjev.component';
import { UrediOglasComponent } from './pages/uredi-oglas/uredi-oglas.component';
import { AdminGuard } from './services/guards/adminGuard/admin.guard';
import { KorisniciComponent } from './pages/korisnici/korisnici.component';
import { MojiFeedbackoviComponent } from './pages/moji-feedbackovi/moji-feedbackovi.component';
import { SviZahtjeviComponent } from './pages/svi-zahtjevi/svi-zahtjevi.component';
import { SviOglasiComponent } from './pages/svi-oglasi/svi-oglasi.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'register', component:RegisterComponent, canActivate: [AuthenticationGuard]},
  {path:'login', component:LoginComponent, canActivate: [AuthenticationGuard]},
  {path:'addOglas', component:AddOglasComponent, canActivate: [UnloggedUserGuard]},
  {path:'addZahtjev', component:AddZahtjevComponent, canActivate: [UnloggedUserGuard]},
  {path:'oglasi', component:OglasiComponent},
  {path:'zahtjevi', component:ZahtjeviComponent},
  // {path: 'profil', component:ProfilePageComponent, canActivate:[UnloggedUserGuard], children: [
  //   {path: 'urediProfil', component: UrediProfilComponent},
  //   {path: 'mojiOglasi', component: MojiOglasiComponent, children: [
  //     {path: 'posudba', component: PosudbeOglasiComponent},
  //     {path: 'urediOglas/:idOglasa', component: UrediOglasComponent}
  //   ]},
  //   {path: 'mojiZahtjevi', component: MojiZahtjeviComponent, children: [
  //     {path: 'posudba', component: PosudbeZahtjeviComponent},
  //     {path: 'urediZahtjev/:idzahtjeva', component: UrediZahtjevComponent}
  //   ]},
  // ]},
  {path: 'profil', component:ProfilePageComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/urediProfil', component:UrediProfilComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/mojiOglasi', component:MojiOglasiComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/mojiOglasi/urediOglas/:idOglasa', component: UrediOglasComponent, canActivate: [UnloggedUserGuard]},
  {path: 'profil/oglasi/posudba', component:PosudbeOglasiComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/oglasi/interesi', component:InteresiOglasiComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/mojiZahtjevi', component:MojiZahtjeviComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/mojiZahtjevi/urediZahtjev/:idZahtjeva', component: UrediZahtjevComponent, canActivate: [UnloggedUserGuard]},
  {path: 'profil/zahtjevi/posudba', component:PosudbeZahtjeviComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/zahtjevi/interesi', component:InteresiZahtjeviComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/feedback', component:MojiFeedbackoviComponent, canActivate:[UnloggedUserGuard]},
  {path: 'profil/korisnici', component:KorisniciComponent, canActivate:[AdminGuard]},
  {path: 'profil/zahtjevi', component:SviZahtjeviComponent, canActivate:[AdminGuard]},
  {path: 'profil/oglasi', component:SviOglasiComponent, canActivate:[AdminGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthenticationGuard, UnloggedUserGuard]
})
export class AppRoutingModule { }
