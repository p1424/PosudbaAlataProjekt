package hr.fer.posudbaAlata;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.UserRole;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.RequestDeniedException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.mockito.BDDMockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserTest {

        @Autowired
        private MockMvc mvc;

        @MockBean
        private UserService userService;

        @Test
        public void registrationSuccessful() throws Exception {
                Korisnik korisnik = new Korisnik();
                korisnik.setUsername("testuser");
                korisnik.setFirstName("Test");
                korisnik.setLastName("User");
                korisnik.setRole(UserRole.KORISNIK);
                korisnik.setEmail("testUser@gmail.com");
                korisnik.setPhone("00000000");
                korisnik.setAddress("Ulica grada Vukovara 1");
                korisnik.setCity("Zagreb");
                korisnik.setPassword("test123");

                String json = new ObjectMapper().writeValueAsString(korisnik);

                this.mvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).content(json))
                                .andDo(print())
                                .andExpect(status().isOk());

        }

        @Test
        public void registrationFailedUsernameTaken() throws Exception {
                Korisnik korisnik = new Korisnik();
                korisnik.setUsername("testuser");
                korisnik.setFirstName("Test");
                korisnik.setLastName("User");
                korisnik.setRole(UserRole.KORISNIK);
                korisnik.setEmail("testUser@gmail.com");
                korisnik.setPhone("00000000");
                korisnik.setAddress("Ulica grada Vukovara 1");
                korisnik.setCity("Zagreb");
                korisnik.setPassword("test123");

                String json = new ObjectMapper().writeValueAsString(korisnik);

                given(userService.createUser(korisnik))
                                .willThrow(new RequestDeniedException("Username username is already taken"));

                this.mvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).content(json))
                                .andExpect(status().isBadRequest())
                                .andDo(print());
                // .andExpect(jsonPath("$.message").value("Username username is already
                // taken"));

        }

        @Test
        public void registrationFailedEmailTaken() throws Exception {
                Korisnik korisnik = new Korisnik();
                korisnik.setUsername("testuser");
                korisnik.setFirstName("Test");
                korisnik.setLastName("User");
                korisnik.setRole(UserRole.KORISNIK);
                korisnik.setEmail("testUser@gmail.com");
                korisnik.setPhone("00000000");
                korisnik.setAddress("Ulica grada Vukovara 1");
                korisnik.setCity("Zagreb");
                korisnik.setPassword("test123");

                String json = new ObjectMapper().writeValueAsString(korisnik);

                given(userService.createUser(korisnik))
                                .willThrow(new RequestDeniedException("Email email@gmail.com is already in use"));

                this.mvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).content(json))
                                .andExpect(status().isBadRequest())
                                .andDo(print());
                // .andExpect(jsonPath("message").value("Email email@gmail.com is already in
                // use"));

        }

        @Test
        public void tokenTest_noToken() throws Exception {
                this.mvc.perform(get("/users"))
                                .andExpect(status().is4xxClientError());
        }

}
