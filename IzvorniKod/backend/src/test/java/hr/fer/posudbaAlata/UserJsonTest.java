package hr.fer.posudbaAlata;

import hr.fer.posudbaAlata.domain.Korisnik;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
public class UserJsonTest {

    @Autowired
    private JacksonTester<Korisnik> json;

    @Test
    void userOne_NonEmptyUserPass() throws Exception{
        Korisnik korisnik = new Korisnik("mirko123",
                "Mirko",
                "Prezime",
                "nasat23@gmail.com",
                "091234543",
                "Ulica sezama 2",
                "Zagreb",
                "volimSlape123");


        String expectedContent ="{\n" +
                "        \"userId\": null ,\n"+
                "        \"username\": \"mirko123\",\n" +
                "        \"firstName\": \"Mirko\",\n" +
                "        \"city\": \"Zagreb\",\n" +
                "        \"lastName\": \"Prezime\",\n" +
                "        \"email\": \"nasat23@gmail.com\",\n" +
                "        \"phone\":\"091234543\",\n" +
                "        \"address\": \"Ulica sezama 2\",\n" +
                "        \"password\": \"volimSlape123\",\n" +
                "        \"role\": \"KORISNIK\" ,\n" +
                "        \"deleted\": false" +
                "}";

        assertThat(json.write(korisnik)).isStrictlyEqualToJson(expectedContent);
    }

    @Test
    public void userTwo_EmptyUserPass()throws Exception{
        Korisnik korisnik = new Korisnik();

        assertThat(json.write(korisnik)).isStrictlyEqualToJson("{\n" +
                "        \"userId\": null ,\n"+
                "        \"username\": null ,\n" +
                "        \"firstName\": null ,\n" +
                "        \"city\": null ,\n" +
                "        \"lastName\": null ,\n" +
                "        \"email\": null ,\n" +
                "        \"phone\": null,\n" +
                "        \"address\": null ,\n" +
                "        \"password\": null ,\n" +
                "        \"role\": KORISNIK ,\n" +
                "        \"deleted\": false \n" +
                "}");
    }
}
