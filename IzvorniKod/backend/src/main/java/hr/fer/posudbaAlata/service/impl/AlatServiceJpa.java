package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.AlatRepository;
import hr.fer.posudbaAlata.domain.Alat;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.service.AlatService;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import io.jsonwebtoken.lang.Assert;
import hr.fer.posudbaAlata.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlatServiceJpa implements AlatService {

    private AlatRepository alatRepository;

    @Autowired
    public AlatServiceJpa(AlatRepository a){
        this.alatRepository = a;
    }

    @Override
    public Alat dodajAlat(Alat alat) {

        if(LoginController.authorize() == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");


//        private Long idAlata;                 autogenerated
//        private String fotografija;           optional
//        private int snaga;                    optional
//        private boolean isAkumulatorski;      optional
//        private KategorijaAlata kategorija;
//        private String vrsta;
//        private String podvrsta;              optional
//        private MjestoPrimjene mjestoPrimjene;

        Assert.isNull(alat.getIdAlata(), "Id alata mora biti null!");
        Assert.notNull(alat.getKategorija(), "Kategorija alata ne smije biti prazna");
        Assert.notNull(alat.getVrsta(), "Vrsta alata ne smije biti prazna");
        Assert.notNull(alat.getMjestoPrimjene(), "Mjesto primjene ne smije biti prazno");

        return alatRepository.save(alat);
    }

    @Override
    public List<Alat> dohvatiSve() {
        return alatRepository.findAll();
    }

    @Override
    public Alat dohvatiAlat(Long id) {

        Optional<Alat> alat = alatRepository.findById(id);

        if(alat.isPresent()) {
            return alat.get();
        } else {
            throw new NotFoundException("Ne postoji alat s identifikatorom " + id);
        }

    }
}
