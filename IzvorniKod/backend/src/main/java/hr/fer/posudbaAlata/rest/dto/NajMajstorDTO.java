package hr.fer.posudbaAlata.rest.dto;

public class NajMajstorDTO {

    private String username;
    private String address;
    private String city;
    private int ocjenaCount;
    private double ocjenaAVG;

    public NajMajstorDTO(String username, String address, String city, int ocjenaCount, double ocjenaAVG) {
        this.username = username;
        this.address = address;
        this.city = city;
        this.ocjenaCount = ocjenaCount;
        this.ocjenaAVG = ocjenaAVG;
    }

    public String getUsername() {
        return username;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public int getOcjenaCount() {
        return ocjenaCount;
    }

    public double getOcjenaAVG() {
        return ocjenaAVG;
    }
}
