package hr.fer.posudbaAlata.rest.dto;

public class ReportDTO {

    private String tekstReporta;

    public ReportDTO(String tekstReporta) {
        this.tekstReporta = tekstReporta;
    }

    public ReportDTO() {
    }

    public String getTekstReporta() {
        return tekstReporta;
    }

    public void setTekstReporta(String tekstReporta) {
        this.tekstReporta = tekstReporta;
    }
}
