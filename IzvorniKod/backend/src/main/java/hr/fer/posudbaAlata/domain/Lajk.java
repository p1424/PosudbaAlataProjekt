package hr.fer.posudbaAlata.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity @Table
public class Lajk {

    @Id @GeneratedValue
    private Long idLajka;

    private LocalDate datumLajka;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JsonIgnore
    private Komentar naKomentar;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik ostavljaKorisnik;

    public Long getIdLajka() {
        return idLajka;
    }

    public LocalDate getDatumLajka() {
        return datumLajka;
    }

    public void setDatumLajka(LocalDate datumLajka) {
        this.datumLajka = datumLajka;
    }

    public Komentar getNaKomentar() {
        return naKomentar;
    }

    public void setNaKomentar(Komentar naKomentar) {
        this.naKomentar = naKomentar;
    }

    public Korisnik getOstavljaKorisnik() {
        return ostavljaKorisnik;
    }

    public void setOstavljaKorisnik(Korisnik ostavljaKorisnik) {
        this.ostavljaKorisnik = ostavljaKorisnik;
    }

    public  Lajk() { }

    public Lajk(String datumLajka, Komentar naKomentar, Korisnik ostavljaKorisnik) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        this.datumLajka = LocalDate.parse(datumLajka, formatter);

        this.naKomentar = naKomentar;
        this.ostavljaKorisnik = ostavljaKorisnik;
    }
    public Lajk(LocalDate datumLajka, Komentar naKomentar, Korisnik ostavljaKorisnik) {
        this.datumLajka = datumLajka;
        this.naKomentar = naKomentar;
        this.ostavljaKorisnik = ostavljaKorisnik;
    }


    @Override
    public String toString() {
        return "Lajk{" +
                "idLajka=" + idLajka +
                ", datumLajka=" + datumLajka +
                ", naKomentar=" + naKomentar +
                ", ostavljaKorisnik=" + ostavljaKorisnik +
                '}';
    }
}
