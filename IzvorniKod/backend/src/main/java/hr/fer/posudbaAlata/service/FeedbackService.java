package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.Feedback;
import hr.fer.posudbaAlata.rest.dto.FeedbackDTO;
import hr.fer.posudbaAlata.rest.dto.NajMajstorDTO;

import java.util.List;

public interface FeedbackService {

    Feedback dodajFeedback(Long userId, FeedbackDTO dto);
    List<Feedback> dohvatiMojeFeedbackove();
    NajMajstorDTO dohvatiNajMajstora();
}
