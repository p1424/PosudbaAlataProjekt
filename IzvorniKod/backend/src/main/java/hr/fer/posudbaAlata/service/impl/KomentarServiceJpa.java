package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.KomentarRepository;
import hr.fer.posudbaAlata.domain.Komentar;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.service.KomentarService;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import hr.fer.posudbaAlata.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KomentarServiceJpa implements KomentarService {

    private final KomentarRepository komentarRepository;

    @Autowired
    public KomentarServiceJpa(KomentarRepository komentarRepository) {
        this.komentarRepository = komentarRepository;
    }

    @Override
    public boolean dodajKomentar(Komentar komentar) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        if(komentar != null){
            komentar.setDeleted(false);
            komentarRepository.save(komentar);
            return true;
        }

        return false;
    }

    @Override
    public boolean obrisiKomentar(Long idKomentara) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        if(komentarRepository.existsById(idKomentara) ){
            var k = komentarRepository.getKomentarByZahtjevId(idKomentara);
            for(var kom : k){
                if(kom.getIdKomentara() == idKomentara) {
                    kom.setDeleted(true);
                    komentarRepository.save(kom);
                }
            }
            return true;
        }

        return false;
    }

    @Override
    public Optional<Komentar> dohvatiKomentarById(Long komentarId) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Komentar> komentar = komentarRepository.findById(komentarId);

        if(komentar.isEmpty() || komentar.get().isDeleted()) {
            throw new NotFoundException("Ne postoji komentar s identifikatorom " + komentarId);
        }

        return komentar;
    }

    @Override
    public List<Komentar> dohvatiKomentarPoZahtjevu(Long idZahtjeva) {
        return komentarRepository.getKomentarByZahtjevId(idZahtjeva);
    }

    @Override
    public List<Komentar> dohvatiKomentarPoKorisniku(Long idKorisnika) {
        return komentarRepository.getKomentarByKorisnikId(idKorisnika);
    }

    @Override
    public boolean delete(Komentar komentar) {

        if(komentarRepository.existsById(komentar.getIdKomentara())){
            komentar.setDeleted(true);
            komentarRepository.save(komentar);
            return true;
        }

        return false;
    }

}
