package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.service.LajkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LajkController {

    private final LajkService lajkService;

    @Autowired
    public LajkController(LajkService lajkService) {
        this.lajkService = lajkService;
    }

    @GetMapping("/lajkaj/{komentarId}")
    public boolean lajkaj(@PathVariable("komentarId") Long komentarId){
        return lajkService.promjeniStanjeLajka(komentarId);
    }
}
