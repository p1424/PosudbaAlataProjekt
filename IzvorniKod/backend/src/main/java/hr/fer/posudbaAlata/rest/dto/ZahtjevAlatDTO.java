package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.KategorijaAlata;
import hr.fer.posudbaAlata.domain.MjestoPrimjeneAlata;
import hr.fer.posudbaAlata.domain.NacinKontakta;

public class ZahtjevAlatDTO {

    private boolean isAkumulatorski;
    private KategorijaAlata kategorijaAlata;
    private MjestoPrimjeneAlata mjestoPrimjeneAlata;
    private String fotografija;
    private String podvrsta;
    private String poveznicaNaProizvod;
    private int snaga;
    private String vrsta;
    private NacinKontakta nacinKontakta;
    private String tekstZahtjeva;
    private double maxUdaljenost;

    public String getFotografija() {
        return fotografija;
    }

    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    public ZahtjevAlatDTO(boolean isAkumulatorski, KategorijaAlata kategorijaAlata, MjestoPrimjeneAlata mjestoPrimjeneAlata, String fotografija, String podvrsta, String poveznicaNaProizvod, int snaga, String vrsta, NacinKontakta nacinKontakta, String tekstZahtjeva, double maxUdaljenost) {
        this.isAkumulatorski = isAkumulatorski;
        this.kategorijaAlata = kategorijaAlata;
        this.mjestoPrimjeneAlata = mjestoPrimjeneAlata;
        this.fotografija = fotografija;
        this.podvrsta = podvrsta;
        this.poveznicaNaProizvod = poveznicaNaProizvod;
        this.snaga = snaga;
        this.vrsta = vrsta;
        this.nacinKontakta = nacinKontakta;
        this.tekstZahtjeva = tekstZahtjeva;
        this.maxUdaljenost = maxUdaljenost;
    }

    public ZahtjevAlatDTO() { }

    public boolean isAkumulatorski() {
        return isAkumulatorski;
    }

    public void setAkumulatorski(boolean akumulatorski) {
        isAkumulatorski = akumulatorski;
    }

    public KategorijaAlata getKategorijaAlata() {
        return kategorijaAlata;
    }

    public void setKategorijaAlata(KategorijaAlata kategorijaAlata) {
        this.kategorijaAlata = kategorijaAlata;
    }

    public MjestoPrimjeneAlata getMjestoPrimjeneAlata() {
        return mjestoPrimjeneAlata;
    }

    public void setMjestoPrimjeneAlata(MjestoPrimjeneAlata mjestoPrimjeneAlata) {
        this.mjestoPrimjeneAlata = mjestoPrimjeneAlata;
    }

    public String getPodvrsta() {
        return podvrsta;
    }

    public void setPodvrsta(String podvrsta) {
        this.podvrsta = podvrsta;
    }

    public String getPoveznicaNaProizvod() {
        return poveznicaNaProizvod;
    }

    public void setPoveznicaNaProizvod(String poveznicaNaProizvod) {
        this.poveznicaNaProizvod = poveznicaNaProizvod;
    }

    public int getSnaga() {
        return snaga;
    }

    public void setSnaga(int snaga) {
        this.snaga = snaga;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }


    public NacinKontakta getNacinKontakta() {
        return nacinKontakta;
    }

    public void setNacinKontakta(NacinKontakta nacinKontakta) {
        this.nacinKontakta = nacinKontakta;
    }

    public String getTekstZahtjeva() {
        return tekstZahtjeva;
    }

    public void setTekstZahtjeva(String tekstZahtjeva) {
        this.tekstZahtjeva = tekstZahtjeva;
    }

    public double getMaxUdaljenost() {
        return maxUdaljenost;
    }

    public void setMaxUdaljenost(double maxUdaljenost) {
        this.maxUdaljenost = maxUdaljenost;
    }
}
