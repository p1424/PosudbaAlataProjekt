package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.FeedbackRepository;
import hr.fer.posudbaAlata.domain.Feedback;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.UserRole;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.rest.dto.FeedbackDTO;
import hr.fer.posudbaAlata.rest.dto.NajMajstorDTO;
import hr.fer.posudbaAlata.service.FeedbackService;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import hr.fer.posudbaAlata.utils.NotFoundException;
import hr.fer.posudbaAlata.utils.RequestDeniedException;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FeedbackServiceJpa implements FeedbackService {

    private final FeedbackRepository feedbackRepository;
    private final UserService userService;

    public FeedbackServiceJpa(FeedbackRepository feedbackRepository, UserService userService) {
        this.feedbackRepository = feedbackRepository;
        this.userService = userService;
    }

    @Override
    public Feedback dodajFeedback(Long userId, FeedbackDTO dto) {

        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        Optional<Korisnik> odUsera = userService.findUserByUsername(currentUser.getUsername());
        Optional<Korisnik> zaUsera = userService.findByid(userId);

        if(zaUsera.isEmpty()) {
            throw new NotFoundException("Ne postoji korisnik s identifikatorom " + userId);
        }

        if(odUsera.get().getUserId().equals(zaUsera.get().getUserId())) {
            throw new RequestDeniedException("Ne mozete samome sebi ostaviti feedback");
        }

        Feedback feedback = new Feedback(date, dto.getTekstFeedbacka(), dto.getOcjena(), zaUsera.get(), odUsera.get());

        return feedbackRepository.save(feedback);
    }

    @Override
    public List<Feedback> dohvatiMojeFeedbackove() {

        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        List<Feedback> list = feedbackRepository.getMyFeedbacks(trenutniKorisnik.get().getUserId());

        if(list.isEmpty()) {
            throw new NotFoundException("Nemate feedback");
        } else {
            return list;
        }
    }

    @Override
    public NajMajstorDTO dohvatiNajMajstora(){
//        svi mogu na rutu
        var flist = feedbackRepository.findAll().stream().filter(fed -> !fed.getZaKorisnika().isDeleted() && fed.getZaKorisnika().getRole() != UserRole.BLOCKED).collect(Collectors.toList());
        Map<Korisnik, Double> mapaKorisnikOcjena = new HashMap<Korisnik,Double>();
        Map<Korisnik, Integer> mapaKorisnikPuta = new HashMap<Korisnik,Integer>();
        for(var feed : flist){
            mapaKorisnikOcjena.merge(feed.getZaKorisnika(), Double.valueOf(feed.getOcjena()), Double::sum);
            mapaKorisnikPuta.merge(feed.getZaKorisnika(), 1, Integer::sum);
        }
        if(mapaKorisnikOcjena.size() >= 1) {
            var najMajstor = Collections.max(mapaKorisnikOcjena.entrySet(), Comparator.comparingDouble(Map.Entry::getValue)).getKey();
            int count = mapaKorisnikPuta.get(najMajstor);

            double ocjenaAVG = mapaKorisnikOcjena.get(najMajstor) / (double)count;
            return new NajMajstorDTO(najMajstor.getUsername(), najMajstor.getAddress(), najMajstor.getCity(), count, ocjenaAVG);
        }
        return null;




    }
}
