package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.Komentar;

import java.util.List;
import java.util.Optional;

public interface KomentarService {

    boolean dodajKomentar(Komentar komentar);
    boolean obrisiKomentar(Long idKomentara);
    Optional<Komentar> dohvatiKomentarById(Long komentarId);
    List<Komentar> dohvatiKomentarPoZahtjevu(Long idZahtjeva);
    List<Komentar> dohvatiKomentarPoKorisniku(Long idKorisnika);
    boolean delete(Komentar komentar);
}
