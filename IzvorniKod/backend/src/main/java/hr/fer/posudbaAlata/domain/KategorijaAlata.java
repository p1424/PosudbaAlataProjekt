package hr.fer.posudbaAlata.domain;

public enum KategorijaAlata {
    PROFESIONALNI, POLUPROFESIONALNI, AMATERSKI;
}
