package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Oglas;
import hr.fer.posudbaAlata.domain.Zahtjev;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InteresRepository extends JpaRepository<InteresZaPosudbu, Long> {

    @Query("SELECT i.zainteresiraniKorisnik FROM InteresZaPosudbu i WHERE i.zaOglas.idOglasa = :id AND i.deleted = false and i.zainteresiraniKorisnik.deleted = false")
    List<Korisnik> dohvatiZainteresiranePoOglasu(@Param("id") Long oglasId);

    @Query("SELECT i.zainteresiraniKorisnik FROM InteresZaPosudbu i WHERE i.zaZahtjev.idZahtjeva = :id AND i.deleted = false and i.zainteresiraniKorisnik.deleted = false")
    List<Korisnik> dohvatiZainteresiranePoZahtjevu(@Param("id") Long zahtjevId);

    @Query("SELECT i FROM InteresZaPosudbu i WHERE i.zaZahtjev.idZahtjeva = :zahtjevId AND i.zainteresiraniKorisnik.userId = :userId AND i.deleted = false and i.zainteresiraniKorisnik.deleted = false")
    InteresZaPosudbu dohvatiInteresPoZahtjevuIKorisniku(@Param("zahtjevId") Long zahtjevId, @Param("userId") Long userId);

    @Query("SELECT i FROM InteresZaPosudbu i WHERE i.zaOglas.idOglasa = :oglasId AND i.zainteresiraniKorisnik.userId = :userId AND i.deleted = false and i.zainteresiraniKorisnik.deleted = false")
    InteresZaPosudbu dohvatiInteresPoOglasuIKorisniku(@Param("oglasId") Long oglasId, @Param("userId") Long userId);

    @Query("SELECT i.zaZahtjev FROM InteresZaPosudbu i WHERE i.zainteresiraniKorisnik.userId = :id AND i.deleted = false")
    List<Zahtjev> dohvatiZahtjeveIzInteresa(@Param("id") Long userId);

    @Query("SELECT i.zaOglas FROM InteresZaPosudbu i WHERE i.zainteresiraniKorisnik.userId = :id AND i.deleted = false")
    List<Oglas> dohvatiOglaseIzInteresa(@Param("id") Long userId);

    @Query("SELECT i FROM InteresZaPosudbu i WHERE i.zaZahtjev.idZahtjeva = :id AND i.deleted = false")
    List<InteresZaPosudbu> findAllZahtjev(@Param("id") Long zahtjevId);

    @Query("SELECT i FROM InteresZaPosudbu i WHERE i.zaOglas.idOglasa = :id AND i.deleted = false")
    List<InteresZaPosudbu> findAllOglas(@Param("id") Long oglasId);

}
