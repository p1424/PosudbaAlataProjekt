package hr.fer.posudbaAlata.domain;

public enum NacinKontakta {
    EMAIL, TELEFON
}
