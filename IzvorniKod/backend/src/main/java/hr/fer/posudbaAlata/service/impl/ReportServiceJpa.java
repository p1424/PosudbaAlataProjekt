package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.ReportRepository;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Report;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.rest.dto.ReportDTO;
import hr.fer.posudbaAlata.service.ReportService;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import hr.fer.posudbaAlata.utils.NotFoundException;
import hr.fer.posudbaAlata.utils.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceJpa implements ReportService {

    private final ReportRepository reportRepository;
    private final UserService userService;

    @Autowired
    public ReportServiceJpa(ReportRepository reportRepository, UserService userService) {
        this.reportRepository = reportRepository;
        this.userService = userService;
    }

    @Override
    public Report prijaviKorisnika(Long userId, ReportDTO dto) {

        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        Optional<Korisnik> odUsera = userService.findUserByUsername(currentUser.getUsername());
        Optional<Korisnik> zaUsera = userService.findByid(userId);

        if(zaUsera.isEmpty()) {
            throw new NotFoundException("Ne postoji korisnik s identifikatorom " + userId);
        }

        if(odUsera.get().getUserId().equals(zaUsera.get().getUserId())) {
            throw new RequestDeniedException("Ne mozete prijaviti samog sebe");
        }

        Report report = new Report(date, dto.getTekstReporta(), zaUsera.get(), odUsera.get());

        return  reportRepository.save(report);

    }

    @Override
    public List<Report> dohvatiListuReportaZaKorisnika(Long userId) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
        if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
            throw new IllegalAccess("Za pristup potrebno je biti admin!");
        }

        return reportRepository.findReportZaKorisnika(userId);
    }
}
