package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Komentar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KomentarRepository extends JpaRepository<Komentar, Long> {

    @Query("SELECT k FROM Komentar k WHERE k.naZahtjev.idZahtjeva = :id and k.deleted = false")
    List<Komentar> getKomentarByZahtjevId(@Param("id") Long zahtjevId);

    @Query("SELECT k FROM Komentar k WHERE k.ostavljaKorisnik.userId = :id and k.deleted = false")
    List<Komentar> getKomentarByKorisnikId(@Param("id")Long idKorisnika);
}
