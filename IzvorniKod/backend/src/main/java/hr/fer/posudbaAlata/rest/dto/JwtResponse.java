package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.Korisnik;

public class JwtResponse {
    private String token;
    private Korisnik korisnik;

    public JwtResponse(String token, Korisnik korisnik) {
        this.token = token;
        this.korisnik = korisnik;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
