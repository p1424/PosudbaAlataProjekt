package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.Alat;

import java.util.List;

public interface AlatService {

    Alat dodajAlat(Alat alat);
    Alat dohvatiAlat(Long alatId);
    List<Alat> dohvatiSve();
}
