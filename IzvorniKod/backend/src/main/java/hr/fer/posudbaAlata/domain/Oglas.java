package hr.fer.posudbaAlata.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Oglas {

    @Id @GeneratedValue
    private Long idOglasa;

    /**
     * Koristi 'dd.MM.yyyy' format
     */
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @JsonFormat(pattern="dd.MM.yyyy")
    private LocalDate datumOglasa;
    private String tekstOglasa;

    @OneToOne
    private Alat alat;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik nuditelj;

    private NacinKontakta nacinKontakta;
    private boolean isActive;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik posuditelj;

    private boolean povratNuditelj;

    private boolean povratPosuditelj;

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Oglas() {
    }

    public Korisnik getPosuditelj() {
        return posuditelj;
    }

    public void setPosuditelj(Korisnik posuditelj) {
        this.posuditelj = posuditelj;
    }

    public Long getIdOglasa() {
        return idOglasa;
    }

    public LocalDate getDatumOglasa() {
        return datumOglasa;
    }

    public void setDatumOglasa(LocalDate datumOglasa) {
        this.datumOglasa = datumOglasa;
    }

    public String getTekstOglasa() {
        return tekstOglasa;
    }

    public void setTekstOglasa(String tekstOglasa) {
        this.tekstOglasa = tekstOglasa;
    }


    public Alat getAlat() {
        return alat;
    }

    public void setAlat(Alat alat) {
        this.alat = alat;
    }

    public Korisnik getNuditelj() {
        return nuditelj;
    }

    public void setNuditelj(Korisnik nuditelj) {
        this.nuditelj = nuditelj;
    }

    public NacinKontakta getNacinKontakta() {
        return nacinKontakta;
    }

    public void setNacinKontakta(NacinKontakta nacinKontakta) {
        this.nacinKontakta = nacinKontakta;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isPovratNuditelj() {
        return povratNuditelj;
    }

    public void setPovratNuditelj(boolean povratNuditelj) {
        this.povratNuditelj = povratNuditelj;
    }

    public boolean isPovratPosuditelj() {
        return povratPosuditelj;
    }

    public void setPovratPosuditelj(boolean povratPosuditelj) {
        this.povratPosuditelj = povratPosuditelj;
    }

    public Oglas(LocalDate datumOglasa, String tekstOglasa, Alat alat, Korisnik nuditelj, NacinKontakta nacinKontakta, boolean isActive, Korisnik posuditelj, boolean povratNuditelj, boolean povratPosuditelj, boolean deleted) {
        this.datumOglasa = datumOglasa;
        this.tekstOglasa = tekstOglasa;
        this.alat = alat;
        this.nuditelj = nuditelj;
        this.nacinKontakta = nacinKontakta;
        this.isActive = isActive;
        this.posuditelj = posuditelj;
        this.povratNuditelj = povratNuditelj;
        this.povratPosuditelj = povratPosuditelj;
        this.deleted = deleted;
    }
}
