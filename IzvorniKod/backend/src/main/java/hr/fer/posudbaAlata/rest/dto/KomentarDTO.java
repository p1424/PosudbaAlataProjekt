package hr.fer.posudbaAlata.rest.dto;

public class KomentarDTO {

    private String tekstKomentara;

    public String getTekstKomentara() {
        return tekstKomentara;
    }

    public void setTekstKomentara(String tekstKomentara) {
        this.tekstKomentara = tekstKomentara;
    }

    public KomentarDTO(String tekstKomentara) {
        this.tekstKomentara = tekstKomentara;
    }

    public KomentarDTO() { }

}
