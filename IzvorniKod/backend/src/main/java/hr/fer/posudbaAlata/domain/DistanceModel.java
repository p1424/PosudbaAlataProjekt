package hr.fer.posudbaAlata.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class DistanceModel {

    @Id
    @GeneratedValue
    private long model_id;
    private long place_id;
    private double lat;
    private double lon;
    private String display_name;
    private String type;

    public DistanceModel(long place_id, double lat, double lon, String display_name, String type) {
        this.place_id = place_id;
        this.lat = lat;
        this.lon = lon;
        this.display_name = display_name;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPlace_id() {
        return place_id;
    }

    public void setPlace_id(long place_id) {
        this.place_id = place_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }


    public DistanceModel() {
    }

    @Override
    public String toString() {
        return "DistanceModel{" +
                "place_id=" + place_id +
                ", lat=" + lat +
                ", lon=" + lon +
                ", display_name='" + display_name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
