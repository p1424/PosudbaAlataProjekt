package hr.fer.posudbaAlata.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class InteresZaPosudbu {

    @Id @GeneratedValue
    private Long idInteresa;

    private LocalDate datumInteresa;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik zainteresiraniKorisnik;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Oglas zaOglas;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Zahtjev zaZahtjev;

    private Status status;

    private boolean deleted;

    public Long getIdInteresa() {
        return idInteresa;
    }

    public LocalDate getDatumInteresa() {
        return datumInteresa;
    }

    public void setDatumInteresa(LocalDate datumInteresa) {
        this.datumInteresa = datumInteresa;
    }

    public Korisnik getZainteresiraniKorisnik() {
        return zainteresiraniKorisnik;
    }

    public void setZainteresiraniKorisnik(Korisnik zainteresiraniKorisnik) {
        this.zainteresiraniKorisnik = zainteresiraniKorisnik;
    }

    public Oglas getZaOglas() {
        return zaOglas;
    }

    public void setZaOglas(Oglas zaOglas) {
        this.zaOglas = zaOglas;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Zahtjev getZaZahtjev() {
        return zaZahtjev;
    }

    public void setZaZahtjev(Zahtjev zaZahtjev) {
        this.zaZahtjev = zaZahtjev;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public InteresZaPosudbu() {

    }

    public InteresZaPosudbu(LocalDate datumInteresa, Korisnik zainteresiraniKorisnik, Oglas zaOglas, Zahtjev zaZahtjev, Status status, boolean deleted) {
        this.datumInteresa = datumInteresa;
        this.zainteresiraniKorisnik = zainteresiraniKorisnik;
        this.zaOglas = zaOglas;
        this.zaZahtjev = zaZahtjev;
        this.status = status;
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "InteresZaPosudbu{" +
                "idInteresa=" + idInteresa +
                ", datumInteresa=" + datumInteresa +
                ", zainteresiraniKorisnik=" + zainteresiraniKorisnik +
                ", zaOglas=" + zaOglas +
                ", status=" + status +
                '}';
    }
}
