package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Zahtjev;
import hr.fer.posudbaAlata.rest.dto.KomentarDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevAlatDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevAndInteresDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevFilterDTO;

import java.util.List;

public interface ZahtjevService {

    Zahtjev createZahtjev(ZahtjevAlatDTO dto);
    List<Zahtjev> dohvatiSve();
    boolean komentiraj(Long idZahtjeva, KomentarDTO komentarDTO);
    List<ZahtjevAndInteresDTO> dohvatiZahtjevePoKorisniku();
    List<Zahtjev> dohvatiNeaktivneZahtjevePoKorisniku();
    List<Zahtjev> dohvatiAktivneZahtjeve();
    List<ZahtjevAndInteresDTO> dohvatiZahtjevePoInteresu();
    boolean obrisiZahtjev(Long zahtjevId);
    Zahtjev modifyZahtjevById(Long id, ZahtjevAlatDTO dto);
    ZahtjevAndInteresDTO getZahtjevByZahtjevId(Long zahtjevId);
    InteresZaPosudbu zabiljeziInteresZaZahtjev(Long zahtjevId);
    boolean napraviPosudbuZahtjev(Long oglasId, Long ponudacId);
    Zahtjev povratZahtjev(Long zahtjevId);
    List<Zahtjev> dohvatiAktivneZahtjeveFilter(ZahtjevFilterDTO zahtjevFilterDTO);
    boolean otkaziInteresZahtjev(Long zahtjevId);
    boolean obrisiKomentar(Long zahtjevId, Long komentarId);
    List<Zahtjev> dohvatiZahtjevePoIduKorisnika(Long idKorisnik);
    void refresh(Zahtjev zahtjev);
    Zahtjev dohvatiZahtjevSIdKomentara(Long idKomentara);
}