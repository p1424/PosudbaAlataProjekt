package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Oglas;
import hr.fer.posudbaAlata.domain.Zahtjev;

import java.util.List;

public interface InteresService {

    List<Korisnik> dohvatiZainteresiraneKorisnikePoOglasu(Long oglasId);
    List<Korisnik> dohvatiZainteresiraneKorisnikePoZahtjevu(Long zahtjevId);

    InteresZaPosudbu saveInteres(InteresZaPosudbu interes);

    InteresZaPosudbu dohvatiPoZahtjevuIKorisniku(Long zahtjevId, Long userId);
    InteresZaPosudbu dohvatiPoOglasuIKorisniku(Long oglasId, Long userId);

    List<Zahtjev> dohvatiZahtjeveIzInteresa(Long userId);
    List<Oglas> dohvatiOglaseIzInteresa(Long userId);
    void delete(Long id);
    List<InteresZaPosudbu> getAll();

    List<InteresZaPosudbu> getAllZahtjev(Long zahtjevId);
    List<InteresZaPosudbu> getAllOglas(Long oglasId);
    
}
