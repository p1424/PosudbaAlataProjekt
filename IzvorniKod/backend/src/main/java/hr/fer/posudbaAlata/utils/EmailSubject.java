package hr.fer.posudbaAlata.utils;

public enum EmailSubject {
    NEW_MESSAGE, ACCOUNT_CREATED, ACCOUNT_DELETED, ACCOUNT_BLOCKED,OGLAS_CREATED, OGLAS_DELETED;
}
