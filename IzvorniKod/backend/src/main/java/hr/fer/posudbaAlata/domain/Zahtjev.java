package hr.fer.posudbaAlata.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table
public class Zahtjev {

    @Id
    @GeneratedValue
    private Long idZahtjeva;

    private LocalDate datumZahtjeva;
    
    private String fotografija;

    private String tekstZahtjeva;

    private double maxUdaljenost;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Komentar> listaKomentara;

    @OneToOne(cascade = CascadeType.ALL)
    private Alat alat;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik trazitelj;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik ponudac;

    private NacinKontakta nacinKontakta;

    private boolean isActive;

    private boolean povratTrazitelj;

    private boolean povratPonudac;

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFotografija() {
        return fotografija;
    }

    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    public Long getIdZahtjeva() {
        return idZahtjeva;
    }

    public LocalDate getDatumZahtjeva() {
        return datumZahtjeva;
    }

    public void setDatumZahtjeva(LocalDate datumZahtjeva) {
        this.datumZahtjeva = datumZahtjeva;
    }

    public String getTekstZahtjeva() {
        return tekstZahtjeva;
    }

    public void setTekstZahtjeva(String tekstZahtjeva) {
        this.tekstZahtjeva = tekstZahtjeva;
    }

    public double getMaxUdaljenost() {
        return maxUdaljenost;
    }

    public void setMaxUdaljenost(double maxUdaljenost) {
        this.maxUdaljenost = maxUdaljenost;
    }

    public Alat getAlat() {
        return alat;
    }

    public void setAlat(Alat alat) {
        this.alat = alat;
    }

    public NacinKontakta getNacinKontakta() {
        return nacinKontakta;
    }

    public void setNacinKontakta(NacinKontakta nacinKontakta) {
        this.nacinKontakta = nacinKontakta;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Zahtjev() { }

    public List<Komentar> getListaKomentara() {
        return this.listaKomentara;
    }

    public void setKomentar(Komentar komentar) {
        if(this.listaKomentara == null){
            this.listaKomentara = new LinkedList<>();
        }

        this.listaKomentara.add(komentar);
    }

    public void deleteKomentar(Komentar komentar){
        if(this.listaKomentara.contains(komentar)){
            this.listaKomentara.remove(komentar);
        }
    }

    public void setListOfComments(List<Komentar> kom) {
        this.listaKomentara = kom;
    }

    public Korisnik getTrazitelj() {
        return trazitelj;
    }

    public void setTrazitelj(Korisnik trazitelj) {
        this.trazitelj = trazitelj;
    }

    public boolean isPovratTrazitelj() {
        return povratTrazitelj;
    }

    public void setPovratTrazitelj(boolean povratTrazitelj) {
        this.povratTrazitelj = povratTrazitelj;
    }

    public boolean isPovratPonudac() {
        return povratPonudac;
    }

    public void setPovratPonudac(boolean povratPonudac) {
        this.povratPonudac = povratPonudac;
    }

    public Korisnik getPonudac() {
        return ponudac;
    }

    public void setPonudac(Korisnik ponudac) {
        this.ponudac = ponudac;
    }

    public Zahtjev(LocalDate datumZahtjeva, String fotografija, String tekstZahtjeva, double maxUdaljenost, List<Komentar> listaKomentara, Alat alat, Korisnik trazitelj, Korisnik ponudac, NacinKontakta nacinKontakta, boolean isActive, boolean povratTrazitelj, boolean povratPonudac, boolean deleted) {
        this.datumZahtjeva = datumZahtjeva;
        this.fotografija = fotografija;
        this.tekstZahtjeva = tekstZahtjeva;
        this.maxUdaljenost = maxUdaljenost;
        this.listaKomentara = listaKomentara;
        this.alat = alat;
        this.trazitelj = trazitelj;
        this.ponudac = ponudac;
        this.nacinKontakta = nacinKontakta;
        this.isActive = isActive;
        this.povratTrazitelj = povratTrazitelj;
        this.povratPonudac = povratPonudac;
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Zahtjev{" +
                "idZahtjeva=" + idZahtjeva +
                ", datumZahtjeva=" + datumZahtjeva +
                ", tekstZahtjeva='" + tekstZahtjeva + '\'' +
                ", maxUdaljenost=" + maxUdaljenost +
                ", alat=" + alat +
                ", tražitelj=" + trazitelj +
                ", nacinKontakta=" + nacinKontakta +
                ", isActive=" + isActive +
                '}';
    }
}
