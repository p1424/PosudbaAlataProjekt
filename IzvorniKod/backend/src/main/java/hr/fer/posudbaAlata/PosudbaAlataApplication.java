
package hr.fer.posudbaAlata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
public class PosudbaAlataApplication {

	@Bean
	public PasswordEncoder passEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
    public WebMvcConfigurer getCorsConfiguration() {
    	return new WebMvcConfigurer() {
    		
    		private final static String ANGULAR_APP_FRONTEND_URL = "ANGULAR_APP_FRONTEND_URL";

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins(System.getenv(ANGULAR_APP_FRONTEND_URL));
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS");
			}
		};
    }
	
	
	public static void main(String[] args) {
		SpringApplication.run(PosudbaAlataApplication.class, args);
	}

}
