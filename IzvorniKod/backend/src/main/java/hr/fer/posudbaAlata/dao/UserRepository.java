package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Korisnik, Long>{

	int countByEmail(String email);
	int countByUsername(String username);
	Optional<Korisnik> findUserByUsername(String username);
	Optional<Korisnik> findById(Long id);

	@Query(value = "select * from korisnik where role = 2", nativeQuery = true)
	List<Korisnik> getAllBlocked();
	@Query(value = "select * from korisnik where role != 2", nativeQuery = true)
	List<Korisnik> getAllNonBlocked();



}
