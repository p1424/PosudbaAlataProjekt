package hr.fer.posudbaAlata.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.posudbaAlata.domain.DistanceModel;

import java.io.IOException;
import java.util.*;

public class DistanceAPI {

    //https://nominatim.org/release-docs/develop/api/Search/
    //free API koji za string mjesta vraca koordinate

    /**
     * Method for parsing address into DistanceModel with lat and log
     *
     * @param arg1 ->  address, arg2 -> city
     * @return List of known distanceModels for given address and city
     * @throws IOException
     */
    public static DistanceModel parseAddressToDistanceModel(String arg1, String arg2, boolean exitFalse) throws IOException {
        String address = arg1;
        String city = arg2;
        List<DistanceModel> lista = new ArrayList<>();

        address = zamjeniNasaSlova(address.toLowerCase());
        city = zamjeniNasaSlova(city.toLowerCase());

        address = address.replace(" ", "%20");
        city = city.replace(" ", "%20");

//        &limit=1 returns only one address

        String command = "curl -X GET https://nominatim.openstreetmap.org/search/?format=json&city={city}&limit=1&street={address}";

        command = command.replace("{address}", address).replace("{city}", city);

        Process process = Runtime.getRuntime().exec(command);

        var input = process.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> jsonMap = mapper.readValue(input, new TypeReference<List<Map<String, Object>>>(){});

        for(var part : jsonMap){
            DistanceModel dm = parseToObject(part);
            lista.add(dm);
        }

        if(lista.isEmpty() && exitFalse == false) {
            //probaj bez kucnog broja, ako ne onda je sigurno kriva adresa ili grad
            return parseAddressToDistanceModel(arg1.split(" ")[0], city, true);
        }

        input.close();
        return lista.stream().findFirst().orElseThrow(()-> new NotFoundException("Adresa i/ili grad ne postoje!"));
    }

    /**
     * Metoda koja zamjenjuje šđčćž s sdccz
     *
     * @param address
     * @return string bez 'nasih' slova
     */
    private static String zamjeniNasaSlova(String address) {

        if(address.contains("č")){
           address = address.replaceAll("č","c");
        }
        else if(address.contains("ć")){
            address = address.replaceAll("ć","c");
        }
        else if(address.contains("š")){
            address = address.replaceAll("š","s");
        }
        else if(address.contains("đ")){
            address = address.replaceAll("đ","d");
        }
        else if(address.contains("ž")){
            address = address.replaceAll("ž","z");
        }
        return address;
    }

    private static DistanceModel parseToObject(Map<String, Object> part) {

        DistanceModel dm = new DistanceModel();

        var parts = part.toString().replace("{","").replace("}","").split(",");

        boolean displayName=false;
        String name = new String();
        for(var p : parts){

            if(p.contains("place_id=")){
                dm.setPlace_id(Long.parseLong(p.split("=")[1]));
            }
            else if(p.contains("lat=")){
                dm.setLat(Double.parseDouble(p.split("=")[1]));
            }
            else if(p.contains("lon=")){
                dm.setLon(Double.parseDouble(p.split("=")[1]));
            }
            else if(p.contains("display_name=")){
                name = p.split("=")[1];
                displayName=true;
            }else if(p.contains("type=")){
                dm.setType(p.split("=")[1]);
            }
            else if(!p.contains("class=") && displayName){
                name += p;
            }
            else if(p.contains("class=")){
                displayName=false;
                dm.setDisplay_name(name);
            }

        }

        return dm;
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Kilometers
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2) {

        final int R = 6371; // Radius of the earth
        double el1 = 0.0;
        double el2 = 0.0;

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance) / 1000;
    }

}
