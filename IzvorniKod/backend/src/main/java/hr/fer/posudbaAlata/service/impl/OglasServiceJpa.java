package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.OglasRepository;
import hr.fer.posudbaAlata.domain.*;
import hr.fer.posudbaAlata.rest.dto.OglasAndAlatDTO;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.rest.dto.OglasAndInteresDTO;
import hr.fer.posudbaAlata.rest.dto.OglasFilterDTO;
import hr.fer.posudbaAlata.service.AlatService;
import hr.fer.posudbaAlata.service.InteresService;
import hr.fer.posudbaAlata.service.OglasService;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.*;
import io.jsonwebtoken.lang.Assert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OglasServiceJpa implements OglasService {

//    private final EmailAPI emailAPI;
    private final OglasRepository oglasRepo;
    private final UserService userService;
    private final AlatService alatService;
    private final InteresService interesService;

    @Autowired
    public OglasServiceJpa(EmailAPI emailAPI, OglasRepository o, UserService userService, AlatService alatService, InteresService is)
    {
//        this.emailAPI = emailAPI;
        this.oglasRepo = o;
        this.userService= userService;
        this.alatService = alatService;
        this.interesService = is;
    }


    @Override
    public Oglas createOglas(OglasAndAlatDTO oglasAndAlatDTO) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");


        Optional<Korisnik> nuditelj = userService.findUserByUsername(currentUser.getUsername());
        Oglas oglas = new Oglas();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);
        oglas.setDatumOglasa(date);


        if (nuditelj.isEmpty())
            throw new IllegalStateException("Error na serveru");


            Assert.notNull(oglasAndAlatDTO.getNazivAlata(), "Naziv alat ne smije biti prazan");
            Assert.notNull(oglasAndAlatDTO.getNacinKontakta(), "Nacin kontakta ne smije biti prazan");
            Assert.notNull(oglasAndAlatDTO.isAkumulatorski(), "Akumulatorski atribut ne smije biti prazan");
            Assert.notNull(oglasAndAlatDTO.getMjestoPrimjeneAlata(), "Mjesto primjene ne smije biti prazno");

//            stvori alat
            Alat alat = new Alat(oglasAndAlatDTO.getFotoUrl(),
                    oglasAndAlatDTO.getSnagaAlata(),
                    oglasAndAlatDTO.isAkumulatorski(),
                    oglasAndAlatDTO.getKategorijaAlata(),
                    oglasAndAlatDTO.getPoveznicaNaProizvod(),
                    oglasAndAlatDTO.getNazivAlata(),
                    oglasAndAlatDTO.getPodvrsta(),
                    oglasAndAlatDTO.getMjestoPrimjeneAlata());

            if(alatService.dodajAlat(alat) == null){
                throw new IllegalAccess("Alat nije dodan!");
            }

            oglas.setAlat(alat);
            oglas.setTekstOglasa(oglasAndAlatDTO.getOpisOglasa());
            oglas.setNacinKontakta(oglasAndAlatDTO.getNacinKontakta());
            oglas.setNuditelj(nuditelj.get());
            oglas.setActive(true);
            oglas.setPosuditelj(null);
            oglas.setPovratNuditelj(true);
            oglas.setPovratPosuditelj(true);

//            emailAPI.sendMail(nuditelj.get().getEmail(), EmailSubject.OGLAS_CREATED, nuditelj.get().getUsername(), "Uspješno ste predali oglas na stranicu.");

            return oglasRepo.save(oglas);
    }

    @Override
    public OglasAndInteresDTO dohvatiOglasPoId(Long idOglas) {
        Optional<Oglas> oglas = oglasRepo.findById(idOglas);

        if(oglas.isEmpty() || oglas.get().isDeleted()) {
            throw new NotFoundException("Ne postoji oglas s identifikatorom " + idOglas);
        }

        List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoOglasu(oglas.get().getIdOglasa());

        return new OglasAndInteresDTO(oglas.get(), listaZainteresiranih);
    }

    @Override
    public List<Oglas> dohvatiSveOglase() {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
        if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
            throw new IllegalAccess("Za dohvat svih oglasa potrebno je biti admin!");
        }

        return oglasRepo.findAll().stream().filter(o -> !o.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public List<Oglas> dohvatiAktivneOglase() {
        List<Oglas> list = oglasRepo.findActiveOglas();

        if(list.isEmpty()) {
            throw new NotFoundException("Trenutno nema aktivnih oglasa");
        } else return list;
    }

    @Override
    public List<Oglas> dohvatiNeAktivneOglase() {
        List<Oglas> list = oglasRepo.findInActiveOglas();

        if(list.isEmpty()) {
            throw new NotFoundException("Trenutno nema neaktivnih oglasa");
        } else return list;
    }

    @Override
    public List<OglasAndInteresDTO> dohvatiOglasePoInteresu() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik =  userService.findUserByUsername(currentUser.getUsername());

        if(trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        List<Oglas> listaOglasa = interesService.dohvatiOglaseIzInteresa(trenutniKorisnik.get().getUserId());

        List<OglasAndInteresDTO> dtoList = new LinkedList<>();

        for(Oglas o : listaOglasa) {
            if(!o.isDeleted()) {
                OglasAndInteresDTO dto = new OglasAndInteresDTO(o, null);
                dtoList.add(dto);
            }
        }

        if(dtoList.isEmpty()) {
            throw new NotFoundException("Trenutno niste zainteresirani niti za jedan oglas");
        }

        return dtoList;
    }

    @Override
    public boolean obrisiOglas(Long idOglas) {
        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Oglas> o = oglasRepo.findById(idOglas);

        if(o.isPresent() && !o.get().isDeleted()) {

            Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

            if (trenutniKorisnik.isEmpty())
                throw new IllegalStateException("Error na serveru");

            if(!o.get().getNuditelj().getUserId().equals(trenutniKorisnik.get().getUserId()) && !(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))) {
                throw new RequestDeniedException("Ne mozete brisati tude oglase");
            }

            o.get().setDeleted(true);
            oglasRepo.save(o.get());
            return true;

        } else {
            throw new NotFoundException("Oglas s identifikatorom " + idOglas + " ne postoji");
        }
    }

    @Override
    public List<Oglas> dohvatiOglasKojiSadrziAlatId(Long idAlata) {
        List<Oglas> list = oglasRepo.findOglasByAlat(idAlata);

        if(list.isEmpty()) {
            throw new NotFoundException("Ne postoje oglasi s takvim alatom");
        } else return list;
    }

    @Override
    public List<Oglas> dohvatiOglaseKojiSadrzeVrsteAlata(String vrstaAlata) {
        List<Oglas> list = oglasRepo.findOglasByVrstaAlata(vrstaAlata);

        if(list.isEmpty()) {
            throw new NotFoundException("Ne postoje oglasi s takvom vrstom alata");
        } else return list;
    }

    @Override
    public boolean napraviPosudbuOglas(Long oglasId, Long posuditeljId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Oglas> oglas = oglasRepo.findById(oglasId);

        if(oglas.isEmpty() || oglas.get().isDeleted()) {
            throw new NotFoundException("Ne postoji oglas s identifikatorom " + oglasId);
        }

        Optional<Korisnik> nuditelj = userService.findUserByUsername(currentUser.getUsername());

        if(!oglas.get().getNuditelj().equals(nuditelj.get())) {
            throw new IllegalAccess("Nemate prava pristupa zahtjevima drugih korisnika");
        }

        Optional<Korisnik> posuditelj = userService.findByid(posuditeljId);

        List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoOglasu(oglasId);

        if(posuditelj.isEmpty() ||!listaZainteresiranih.contains(posuditelj.get())) {
            throw new NotFoundException("Korisnik ne postoji ili nije zainteresiran za oglas");
        }

        List<InteresZaPosudbu> interesi = interesService.getAllOglas(oglasId);

        for(InteresZaPosudbu i : interesi) {
            i.setDeleted(true);
            interesService.saveInteres(i);
        }

        oglas.get().setPosuditelj(posuditelj.get());
        oglas.get().setActive(false);
        oglas.get().setPovratPosuditelj(false);
        oglas.get().setPovratNuditelj(false);
        oglasRepo.save(oglas.get());

        return true;
    }
    @Override
    public Oglas oglasModify(Long id, OglasAndAlatDTO oglasAndAlatDTO) {
        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");


        Optional<Korisnik> nuditelj = userService.findUserByUsername(currentUser.getUsername());

        Oglas oglas = oglasRepo.findById(id).orElseThrow(() -> {
            throw new IllegalAccess("Id oglasa ne postoji");
        });

        if(oglas.isDeleted())
            throw new IllegalAccess("Id oglasa ne postoji");

        if(!oglas.getNuditelj().getUserId().equals(nuditelj.get().getUserId())) {
            throw new RequestDeniedException("Ne mozete modificirati tude oglase");
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);
        oglas.setDatumOglasa(date);


        if (nuditelj.isEmpty())
            throw new IllegalStateException("Error na serveru");


        Assert.notNull(oglasAndAlatDTO.getNazivAlata(), "Naziv alat ne smije biti prazan");
        Assert.notNull(oglasAndAlatDTO.getNacinKontakta(), "Nacin kontakta ne smije biti prazan");
        Assert.notNull(oglasAndAlatDTO.isAkumulatorski(), "Akumulatorski atribut ne smije biti prazan");
        Assert.notNull(oglasAndAlatDTO.getMjestoPrimjeneAlata(), "Mjesto primjene ne smije biti prazno");

//            stvori alat
        Alat alat = new Alat(oglasAndAlatDTO.getFotoUrl(),
                oglasAndAlatDTO.getSnagaAlata(),
                oglasAndAlatDTO.isAkumulatorski(),
                oglasAndAlatDTO.getKategorijaAlata(),
                oglasAndAlatDTO.getPoveznicaNaProizvod(),
                oglasAndAlatDTO.getNazivAlata(),
                oglasAndAlatDTO.getPodvrsta(),
                oglasAndAlatDTO.getMjestoPrimjeneAlata());

        if(alatService.dodajAlat(alat) == null){
            throw new IllegalAccess("Alat nije dodan!");
        }


        oglas.setAlat(alat);
        oglas.setTekstOglasa(oglasAndAlatDTO.getOpisOglasa());
        oglas.setNacinKontakta(oglasAndAlatDTO.getNacinKontakta());


        return oglasRepo.save(oglas);
    }

    @Override
    public List<Oglas> dohvatiOglasePoIduKorisnika(Long idKorisnik){
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        return oglasRepo.findOglasByKorisnik(idKorisnik);
    }

    @Override
    public List<OglasAndInteresDTO> dohvatiOglasePoKorisniku() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (trenutniKorisnik.isEmpty())
            throw new NotFoundException("Error na serveru");

        List<Oglas> listaOglasa = oglasRepo.findOglasByKorisnik(trenutniKorisnik.get().getUserId());

        if(listaOglasa.isEmpty()) {
            throw new NotFoundException("Korisnik trenutno nema oglase");
        }

        List<OglasAndInteresDTO> DTOlist = new LinkedList<>();

        for(Oglas o : listaOglasa) {

            if(o.isDeleted())
                continue;

            List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoOglasu(o.getIdOglasa());

            OglasAndInteresDTO dto = new OglasAndInteresDTO(o, listaZainteresiranih);

            DTOlist.add(dto);
        }

        return DTOlist;
    }

    @Override
    public List<Oglas> dohvbatiNeaktivneOglasePoKorisniku() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        List<Oglas> list = oglasRepo.findInactiveOglasByKorisnik(trenutniKorisnik.get().getUserId());

        if(list.isEmpty()) {
            throw new NotFoundException("Trenutno nemate neaktivne oglase");
        } else return list;

    }

    @Override
    public InteresZaPosudbu zabiljeziInteresZaOglas(Long oglasId) {
        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> zainteresiraniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (zainteresiraniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        Optional<Oglas> oglas = oglasRepo.findById(oglasId);


        if(oglas.isEmpty() || oglas.get().isDeleted()) {
            throw new NotFoundException("Ne postoji oglas s identifikatorom " + oglasId);
        }

        if(oglas.get().getNuditelj().equals(zainteresiraniKorisnik.get())) {
            throw new RequestDeniedException("Ne mozete pokazati interes za Vaš oglas.");
        }

        if(!oglas.get().isActive()) {
            throw new RequestDeniedException("Oglas trenutno nije aktivan");
        }

        List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoOglasu(oglasId);

        if(listaZainteresiranih.contains(zainteresiraniKorisnik.get())) {
            throw new RequestDeniedException("Vec ste pokazali interes za ovaj oglas");
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        InteresZaPosudbu interes = new InteresZaPosudbu(date,
                zainteresiraniKorisnik.get(),
                oglas.get(),
                null,
                Status.U_OBRADI,
                false);

        return interesService.saveInteres(interes);
    }

    @Override
    public Oglas povratOglas(Long oglasId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Oglas> oglas = oglasRepo.findById(oglasId);

        if(oglas.isEmpty() || oglas.get().isDeleted()) {
            throw new NotFoundException("Ne postoji oglas s identifikatorom " + oglasId);
        }

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        //Nuditelj napravi povrat prije posuditelja
        if(oglas.get().getNuditelj().equals(trenutniKorisnik.get())) {

            oglas.get().setPovratNuditelj(true);

            //posuditelj napravi povrat prije nuditelja
        } else if(oglas.get().getPosuditelj().equals(trenutniKorisnik.get())) {

            oglas.get().setPovratPosuditelj(true);

        } else {
            throw new RequestDeniedException("Korisnik nema nikakve veze s oglasom");
        }

        if(oglas.get().isPovratNuditelj() && oglas.get().isPovratPosuditelj()) {
            oglas.get().setActive(true);
            oglas.get().setPosuditelj(null);
        }

        return oglasRepo.save(oglas.get());

    }

    @Override
    public List<Oglas> dohvatiAktivneOglaseFilter(OglasFilterDTO oglasFilterDTO) {


        User currentUser = LoginController.authorize();
        List<Oglas> vrati = null;

        Boolean akumulatorski = null;
        String vrsta = null;

        if(oglasFilterDTO.getAkumulatorski() != null && !oglasFilterDTO.getAkumulatorski().isEmpty() && !oglasFilterDTO.getAkumulatorski().isBlank() && !oglasFilterDTO.getAkumulatorski().equals("")){
            if(oglasFilterDTO.getAkumulatorski().equals("true")){
                akumulatorski=true;
            }
            else akumulatorski=false;
        }

        if(oglasFilterDTO.getVrsta() != null) vrsta = oglasFilterDTO.getVrsta().toUpperCase();

        if(akumulatorski == null)
            vrati = oglasRepo.findFilteredOglas(oglasFilterDTO.getKategorijaAlata(), oglasFilterDTO.getMjestoPrimjeneAlata(), vrsta, oglasFilterDTO.getMaxSnaga(), oglasFilterDTO.getNacinKontakta());
        else
            vrati = oglasRepo.findFilteredOglasWak(akumulatorski,oglasFilterDTO.getKategorijaAlata(), oglasFilterDTO.getMjestoPrimjeneAlata(), vrsta, oglasFilterDTO.getMaxSnaga(), oglasFilterDTO.getNacinKontakta());

        return vrati.stream().filter(oglas -> {

            if(currentUser != null){
                Korisnik trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername()).orElseThrow();

                Double lat1 = trenutniKorisnik.getDistanceModel().getLat();
                Double lon1 = trenutniKorisnik.getDistanceModel().getLon();

                Double lat2 = oglas.getNuditelj().getDistanceModel().getLat();
                Double lon2 = oglas.getNuditelj().getDistanceModel().getLon();

                double realDistance =DistanceAPI.distance(lat1,lat2,lon1,lon2);

                //TODO delete sout
                System.out.println("Udaljenost trenutnog korisnika i oglasa: "+realDistance +" km");

                return realDistance <= oglasFilterDTO.getMaxUdaljenost();
            }

            return true;
        }).collect(Collectors.toList());
    }

    @Override
    public boolean otkaziInteresOglas(Long oglasId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> zainteresiraniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (zainteresiraniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        InteresZaPosudbu interes = interesService.dohvatiPoOglasuIKorisniku(oglasId, zainteresiraniKorisnik.get().getUserId());

        if(interes == null || interes.isDeleted()) {
            throw new NotFoundException("Oglas ne postoji ili niste zainteresirani za njega");
        }

        if(!interes.getZainteresiraniKorisnik().getUserId().equals(zainteresiraniKorisnik.get().getUserId())) {
            throw new IllegalAccess("Nemate pravo otkazati interes drugih korisnika");
        }

        interes.setDeleted(true);
        interesService.saveInteres(interes);

        return true;
    }



}
