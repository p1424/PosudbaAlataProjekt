package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.UserRepository;
import hr.fer.posudbaAlata.domain.*;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.rest.dto.JwtRequest;
import hr.fer.posudbaAlata.rest.dto.JwtResponse;
import hr.fer.posudbaAlata.service.*;
import hr.fer.posudbaAlata.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceJpa implements UserService, UserDetailsService {

	private final UserRepository userRepo;
	private final OglasService oglasService;
	private final ZahtjevService zahtjevService;
	private final KomentarService komentarService;
	private final InteresService interesService;

//	@Autowired
//	private final EmailAPI emailAPI;

	private final PasswordEncoder passwordEncoder;

	@Autowired
	public UserServiceJpa(UserRepository userRepo,@Lazy InteresService interesService,@Lazy OglasService oglasService,@Lazy ZahtjevService zahtjevService,@Lazy KomentarService komentarService, PasswordEncoder passwordEncoder) {
		this.userRepo = userRepo;
		this.oglasService = oglasService;
		this.zahtjevService = zahtjevService;
		this.komentarService = komentarService;
		this.passwordEncoder = passwordEncoder;
		this.interesService = interesService;
	}

	@Override
	public Korisnik createUser(Korisnik korisnik) {

		Assert.notNull(korisnik, "User object must be given");
		Assert.isNull(korisnik.getUserId(), "User id must be null, not " + korisnik.getUserId());

		korisnik.setRole(UserRole.KORISNIK);

		String email = korisnik.getEmail();
		Assert.hasText(email, "Email must be given");

		if (userRepo.countByEmail(email) > 0) {
			throw new RequestDeniedException("Email " + email + " is already in use");
		}

		String username = korisnik.getUsername();
		Assert.hasText(username, "Username must be given");

		if (userRepo.countByUsername(username) > 0) {
			throw new RequestDeniedException("Username " + username + " is already taken");
		}
		String password = passwordEncoder.encode(korisnik.getPassword());
		korisnik.setPassword(password);

		//emailAPI.sendMail(korisnik.getEmail(), EmailSubject.ACCOUNT_CREATED, korisnik.getUsername(), "Vaš račun je uspješno kreiran!");

		korisnik.setDeleted(false);

		//distanceModel add
		try {
			korisnik.setDistanceModel(DistanceAPI.parseAddressToDistanceModel(korisnik.getAddress(), korisnik.getCity(), false));
		} catch (IOException e) {
			throw new NotFoundException("Adresa i/ili mjesto ne postoje");
		}

		return userRepo.save(korisnik);
	}

	@Override
	public JwtResponse loginUser(JwtRequest jwtReq, String token){

		Optional<Korisnik> existingOne = userRepo.findUserByUsername(jwtReq.getUsername());
		if(existingOne.isPresent() && !existingOne.get().isDeleted()){
//			user is present

			if(existingOne.get().getRole() == UserRole.BLOCKED)
				throw new IllegalAccess("Korisnik je blokiran i ne može se ulogirati!");

			String givenPassword = jwtReq.getPassword();
			if(passwordEncoder.matches(givenPassword, existingOne.get().getPassword())){
//				validation succeeded

				return new JwtResponse(token,existingOne.get());
			}
		}
		throw new UserOrPasswordException("User " + jwtReq.getUsername() + " does not exists or incorrect password");

	}

	@Override
	public Korisnik deleteUser(Long userId) {

		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

		Optional<Korisnik> existingOne = userRepo.findById(userId);

		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN")) && existingOne.get().getUserId() != userId){
			throw new IllegalAccess("Nedozvoljeno brisanje korisnika!");
		}


		if(existingOne.isPresent()){
//			can be deleted


//			emailAPI.sendMail(existingOne.get().getEmail(), EmailSubject.ACCOUNT_DELETED, existingOne.get().getUsername(), "Vaš račun obrisan!");

//			makni sve njegove oglase, zahtjeve i  komentari, lajkovi neka ostanu
			removeAllInfo(userId);

			existingOne.get().setDeleted(true);
			userRepo.save(existingOne.get());
//			is deleted

			return existingOne.get();
		}
		throw new UserOrPasswordException("User with id " + userId + " cannot be deleted, because it does not exist!");
	}

	@Override
	public Korisnik getUser(Long id) {

		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

		Optional<Korisnik> existingOne = userRepo.findById(id);

		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN")) && existingOne.get().getUserId() != id){
			throw new IllegalAccess("Nedozvoljeni pristup korisniku!");
		}

		if(existingOne.isPresent()){

			if(!existingOne.get().isDeleted())
			 return existingOne.get();
		}
		throw new UserOrPasswordException("User with id " + id + " does not exist!");
	}

	@Override
	public Korisnik modifyUser(Korisnik korisnik) {

		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

		Korisnik previous = userRepo.findUserByUsername(currentUser.getUsername()).orElseThrow();


		String password = passwordEncoder.encode(korisnik.getPassword());
		previous.setPassword(password);
		previous.setCity(korisnik.getCity());
		previous.setAddress(korisnik.getAddress());
		//new distance model
		try {
			previous.setDistanceModel(DistanceAPI.parseAddressToDistanceModel(korisnik.getAddress(), korisnik.getCity(), false));
		} catch (IOException e) {
			throw new NotFoundException("Adresa i/ili mjesto ne postoje");
		}
		previous.setPhone(korisnik.getPhone());
		previous.setFirstName(korisnik.getFirstName());
		previous.setLastName(korisnik.getLastName());

		return userRepo.save(previous);
	}

	@Override
	public List<Korisnik> getAllUsers(){

		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
			throw new IllegalAccess("Za pristup potrebno je biti admin!");
		}

		return userRepo.findAll().stream().filter(user -> !user.isDeleted()).collect(Collectors.toList());
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Optional<Korisnik> optOne = userRepo.findUserByUsername(username);
		if(optOne.isEmpty())
				throw new UsernameNotFoundException("User is not found or his token is invalid!");
		return new User(optOne.get().getUsername(), optOne.get().getPassword(),optOne.get().getAuthorities());
	}

	@Override
	public Optional<Korisnik> findUserByUsername(String username) {
		Optional<Korisnik> korisnik = userRepo.findUserByUsername(username);

		if(korisnik.isPresent()) {
			if(!korisnik.get().isDeleted())
			return korisnik;
		}
		throw new NotFoundException("Ne postoji korisnik s koji koristi username " + username);

	}

	@Override
	public Korisnik blokirajKorisnika(Long userId) {
		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token ili korisnik nije ulogiran!");
		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
			throw new IllegalAccess("Za pristup potrebno je biti admin!");
		}

		Optional<Korisnik> existingOne = userRepo.findById(userId);
		if(existingOne.isPresent()){
//			can be blocked


//			makni sve njegove oglase, zahtjeve i  komentari, lajkovi neka ostanu
			removeAllInfo(userId);

			existingOne.get().setRole(UserRole.BLOCKED);
//			is blocked
			userRepo.save(existingOne.get());
//			emailAPI.sendMail(existingOne.get().getEmail(), EmailSubject.ACCOUNT_BLOCKED, existingOne.get().getUsername(), "Vaš račun blokiran je od strane admina!");
			return existingOne.get();
		}
		throw new UserOrPasswordException("User with id " + userId + " cannot be deleted, because it does not exist!");
	}

	@Override
	public Korisnik odblokirajKorisnika(Long userId) {
		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token ili korisnik nije ulogiran!");
		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
			throw new IllegalAccess("Za pristup potrebno je biti admin!");
		}

		Optional<Korisnik> existingOne = userRepo.findById(userId);
		if(existingOne.isPresent() && existingOne.get().getRole() == UserRole.BLOCKED){
//			can be unblocked
			existingOne.get().setRole(UserRole.KORISNIK);
			userRepo.save(existingOne.get());
//			is unblocked

			return existingOne.get();
		}
		throw new UserOrPasswordException("Korisnik id " + userId + " ne moze biti blokiran, jer ne postoji ili nije bio blokiran!");
	}

	@Override
	public List<Korisnik> getBlockedUsers() {
		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
			throw new IllegalAccess("Za pristup potrebno je biti admin!");
		}

		return userRepo.getAllBlocked();
	}

	@Override
	public List<Korisnik> getUnblockedUsers() {
		User currentUser = LoginController.authorize();
		if( currentUser == null)
			throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
		if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
			throw new IllegalAccess("Za pristup potrebno je biti admin!");
		}

		return userRepo.getAllNonBlocked();
	}

	@Override
	public Optional<Korisnik> findByid(Long id) {
		Optional<Korisnik> korisnik = userRepo.findById(id);

		if(korisnik.isPresent()) {
			if(!korisnik.get().isDeleted())
			return korisnik;
		}

		throw new NotFoundException("Ne postoji korisnik s identifikatorom " + id);

	}

	/**
	 * removes all zahtjev oglas komentar from Korisnik
	 * @param userId
	 */
	private void removeAllInfo(Long userId) {

		List<Komentar> komentari = komentarService.dohvatiKomentarPoKorisniku(userId);
		if(komentari != null || !komentari.isEmpty()){
			for(var komentar : komentari){
				var zahtjev = zahtjevService.dohvatiZahtjevSIdKomentara(komentar.getIdKomentara());
				zahtjevService.obrisiKomentar(zahtjev.getIdZahtjeva(), komentar.getIdKomentara());
			}
		}


		List<Oglas> oglasi = oglasService.dohvatiOglasePoIduKorisnika(userId);
		if(!oglasi.isEmpty()){
			for(var oglas : oglasi){
				oglasService.obrisiOglas(oglas.getIdOglasa());
			}
		}

		var zahtjevi = zahtjevService.dohvatiZahtjevePoIduKorisnika(userId);
		if(!zahtjevi.isEmpty()){
			for(var zahtjev : zahtjevi){
				zahtjevService.obrisiZahtjev(zahtjev.getIdZahtjeva());
			}
		}

		var interesi = interesService.getAll();
		Set<Long>  set= new LinkedHashSet<>();
		if(!set.isEmpty()){
			for(var in : interesi){
				if(in.getZainteresiraniKorisnik().getUserId() == userId){
					set.add(in.getIdInteresa());
				}
			}
			for(var i : set){
				interesService.delete(i);
			}
		}

	}

}
