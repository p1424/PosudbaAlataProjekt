package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.KategorijaAlata;
import hr.fer.posudbaAlata.domain.MjestoPrimjeneAlata;
import hr.fer.posudbaAlata.domain.NacinKontakta;

public class ZahtjevFilterDTO {

    private String akumulatorski;
    private int kategorijaAlata;
    private int mjestoPrimjeneAlata;
    private String vrsta;
    private int snaga;
    private int nacinKontakta;


    public ZahtjevFilterDTO(String akumulatorski, String kategorijaAlata, String mjestoPrimjeneAlata, String vrsta, int maxSnaga, String nacinKontakta) {

        this.akumulatorski = akumulatorski;

        if(kategorijaAlata == null)
            this.kategorijaAlata= -1;
        else if(!kategorijaAlata.equals(""))
            this.kategorijaAlata = KategorijaAlata.valueOf(kategorijaAlata.toUpperCase()).ordinal();
        else
            this.kategorijaAlata= -1;

        if(mjestoPrimjeneAlata == null)
            this.mjestoPrimjeneAlata= -1;
        else if(!mjestoPrimjeneAlata.equals(""))
            this.mjestoPrimjeneAlata = MjestoPrimjeneAlata.valueOf(mjestoPrimjeneAlata.toUpperCase()).ordinal();
        else
            this.mjestoPrimjeneAlata= -1;

        if(vrsta == null)
            this.vrsta = null;
        else if(!vrsta.equals(""))
            this.vrsta = vrsta;
        else
            this.vrsta = null;


        if(maxSnaga > 0)
            this.snaga = maxSnaga;
        else
            this.snaga = 0;

        if(nacinKontakta == null)
            this.nacinKontakta = -1;
        else if(!nacinKontakta.equals(""))
            this.nacinKontakta = NacinKontakta.valueOf(nacinKontakta.toUpperCase()).ordinal();
        else
            this.nacinKontakta = -1;


    }

    public String getAkumulatorski() {
        return akumulatorski;
    }

    public void setAkumulatorski(String akumulatorski) {
        this.akumulatorski = akumulatorski;
    }

    public int getKategorijaAlata() {
        return kategorijaAlata;
    }

    public void setKategorijaAlata(int kategorijaAlata) {
        this.kategorijaAlata = kategorijaAlata;
    }

    public int getMjestoPrimjeneAlata() {
        return mjestoPrimjeneAlata;
    }

    public void setMjestoPrimjeneAlata(int mjestoPrimjeneAlata) {
        this.mjestoPrimjeneAlata = mjestoPrimjeneAlata;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public int getSnaga() {
        return snaga;
    }

    public void setSnaga(int snaga) {
        this.snaga = snaga;
    }

    public int getNacinKontakta() {
        return nacinKontakta;
    }

    public void setNacinKontakta(int nacinKontakta) {
        this.nacinKontakta = nacinKontakta;
    }
}
