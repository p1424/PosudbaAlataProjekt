package hr.fer.posudbaAlata.utils;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserOrPasswordException extends RuntimeException{

    public UserOrPasswordException(String message) {
        super(message);
    }
}
