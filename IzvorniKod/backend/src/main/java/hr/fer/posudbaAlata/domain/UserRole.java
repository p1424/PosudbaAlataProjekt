package hr.fer.posudbaAlata.domain;

public enum UserRole {
    KORISNIK, ADMIN, BLOCKED
}
