package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Lajk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LajkRepository extends JpaRepository<Lajk, Long> {
}
