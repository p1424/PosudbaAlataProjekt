package hr.fer.posudbaAlata.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Report {

    @Id
    @GeneratedValue
    private Long reportId;

    private LocalDate datumReporta;

    private String tekstReporta;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik reportaniKorisnik;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik odKorisnika;

    public Long getReportId() {
        return reportId;
    }

    public LocalDate getDatumReporta() {
        return datumReporta;
    }

    public void setDatumReporta(LocalDate datumReporta) {
        this.datumReporta = datumReporta;
    }

    public String getTekstReporta() {
        return tekstReporta;
    }

    public void setTekstReporta(String tekstReporta) {
        this.tekstReporta = tekstReporta;
    }

    public Korisnik getReportaniKorisnik() {
        return reportaniKorisnik;
    }

    public void setReportaniKorisnik(Korisnik reportaniKorisnik) {
        this.reportaniKorisnik = reportaniKorisnik;
    }

    public Korisnik getOdKorisnika() {
        return odKorisnika;
    }

    public void setOdKorisnika(Korisnik odKorisnika) {
        this.odKorisnika = odKorisnika;
    }

    public Report() { }

    public Report(LocalDate datumReporta, String tekstReporta, Korisnik reportaniKorisnik, Korisnik odKorisnika) {
        this.datumReporta = datumReporta;
        this.tekstReporta = tekstReporta;
        this.reportaniKorisnik = reportaniKorisnik;
        this.odKorisnika = odKorisnika;
    }

    @Override
    public String toString() {
        return "Report{" +
                "reportId=" + reportId +
                ", datumReporta=" + datumReporta +
                ", tekstReporta='" + tekstReporta + '\'' +
                ", reportaniKorisnik=" + reportaniKorisnik +
                ", odKorisnika=" + odKorisnika +
                '}';
    }
}
