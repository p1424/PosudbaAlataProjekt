package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService us, AuthenticationManager am) {
        this.userService = us;
    }

    @GetMapping(path = "/users")
    public List<Korisnik> getUsers(){
        return userService.getAllUsers();
    }

    @GetMapping(path = "/users/get/{id}")
    public Korisnik getUser(@PathVariable("id")Long id){return userService.getUser(id);}

    @DeleteMapping(path = "/users/delete/{id}")
    public Korisnik deleteUser(@PathVariable("id") Long id){return userService.deleteUser(id);}

    @PutMapping(path = "/users/modify")
    public Korisnik modifyUser(@RequestBody Korisnik korisnik){return userService.modifyUser(korisnik);}

    @GetMapping(path = "/user{id}/block")
    public Korisnik blokirajKorisnika(@PathVariable("id") Long id){
        return userService.blokirajKorisnika(id);
    }

    @GetMapping(path = "/user{id}/unblock")
    public Korisnik odblokirajKorisnika(@PathVariable("id") Long id){
        return userService.odblokirajKorisnika(id);
    }

    @GetMapping(path = "/users/blocked")
    public List<Korisnik> getAllBlocked(){
        return userService.getBlockedUsers();
    }

    @GetMapping(path = "/users/nonblocked")
    public List<Korisnik> getNonBlocked(){
        return userService.getUnblockedUsers();
    }


}
