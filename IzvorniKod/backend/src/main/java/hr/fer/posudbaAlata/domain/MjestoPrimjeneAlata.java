package hr.fer.posudbaAlata.domain;

public enum MjestoPrimjeneAlata {
    VANJSKI, UNUTARNJI, NEDEFINIRANO
}
