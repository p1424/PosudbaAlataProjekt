package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.LajkRepository;
import hr.fer.posudbaAlata.domain.Komentar;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Lajk;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.service.KomentarService;
import hr.fer.posudbaAlata.service.LajkService;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class LajkServiceJpa implements LajkService {

    private KomentarService komentarService;
    private UserService userService;
    private LajkRepository lajkRepository;

    @Autowired
    public LajkServiceJpa(KomentarService komentarService, UserService userService, LajkRepository lajkRepository) {
        this.komentarService = komentarService;
        this.userService = userService;
        this.lajkRepository = lajkRepository;
    }

    @Override
    public boolean promjeniStanjeLajka(Long komentarId) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Komentar> komentar = komentarService.dohvatiKomentarById(komentarId);
        Optional<Korisnik> korisnik = userService.findUserByUsername(currentUser.getUsername());

        if(komentar.isEmpty() || korisnik.isEmpty()) throw new IllegalStateException("Komentar i Korisnik ne smiju biti prazni!");

        //probati pronaci ako postoji lajk current korinika na komentarom(id) ako postoji obrisi ga, ako ne stvori ga
        var lajkovi = komentar.get().getLajkovi();
        for(var lajk : lajkovi){
            if(lajk.getOstavljaKorisnik().getUserId() == korisnik.get().getUserId()){
                //postoji, obrisi ga! Dislike
                lajkRepository.delete(lajk);
                komentar.get().makniLajk(lajk);
                komentarService.dodajKomentar(komentar.get());
                return false;
            }
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);
        Lajk lajkNew = new Lajk(date, komentar.get(), korisnik.get());

        lajkRepository.save(lajkNew);
        komentar.get().setLajkov(lajkNew);
        komentarService.dodajKomentar(komentar.get());


        return true;

    }
}
