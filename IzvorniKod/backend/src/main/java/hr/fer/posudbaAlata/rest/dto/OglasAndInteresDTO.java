package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Oglas;

import java.util.List;

public class OglasAndInteresDTO {

    private Oglas oglas;
    private List<Korisnik> zainteresirani;

    public Oglas getOglas() {
        return oglas;
    }

    public void setOglas(Oglas oglas) {
        this.oglas = oglas;
    }

    public List<Korisnik> getZainteresirani() {
        return zainteresirani;
    }

    public void setZainteresirani(List<Korisnik> zainteresirani) {
        this.zainteresirani = zainteresirani;
    }

    public OglasAndInteresDTO(Oglas oglas, List<Korisnik> zainteresirani) {
        this.oglas = oglas;
        this.zainteresirani = zainteresirani;
    }

    public OglasAndInteresDTO() { }
}
