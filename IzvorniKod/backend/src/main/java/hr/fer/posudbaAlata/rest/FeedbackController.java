package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.domain.Feedback;
import hr.fer.posudbaAlata.rest.dto.FeedbackDTO;
import hr.fer.posudbaAlata.rest.dto.NajMajstorDTO;
import hr.fer.posudbaAlata.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FeedbackController {

    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PostMapping("/feedback/{id}")
    public Feedback addFeedback(@PathVariable("id") Long userId, @RequestBody FeedbackDTO dto) {
        return feedbackService.dodajFeedback(userId, dto);
    }

    @GetMapping("/feedback")
    public List<Feedback> getMyFeedback() {
        return feedbackService.dohvatiMojeFeedbackove();
    }

    @GetMapping("/najMajstor")
    public NajMajstorDTO getNajMajstor(){return feedbackService.dohvatiNajMajstora();}

}
