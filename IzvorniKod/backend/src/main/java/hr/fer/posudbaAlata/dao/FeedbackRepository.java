package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

    @Query("SELECT f FROM Feedback f WHERE f.zaKorisnika.userId = :id")
    List<Feedback> getMyFeedbacks(@Param("id") Long myId);
}
