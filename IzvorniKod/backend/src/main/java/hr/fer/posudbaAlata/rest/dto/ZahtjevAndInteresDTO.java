package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Zahtjev;

import java.util.List;

public class ZahtjevAndInteresDTO {

    private Zahtjev zahtjev;
    private List<Korisnik> listaZainteresiranih;

    public ZahtjevAndInteresDTO(Zahtjev zahtjev, List<Korisnik> listaZainteresiranih) {
        this.zahtjev = zahtjev;
        this.listaZainteresiranih = listaZainteresiranih;
    }

    public ZahtjevAndInteresDTO() {
    }

    public Zahtjev getZahtjev() {
        return zahtjev;
    }

    public void setZahtjev(Zahtjev zahtjev) {
        this.zahtjev = zahtjev;
    }

    public List<Korisnik> getListaZainteresiranih() {
        return listaZainteresiranih;
    }

    public void setListaZainteresiranih(List<Korisnik> listaZainteresiranih) {
        this.listaZainteresiranih = listaZainteresiranih;
    }
}
