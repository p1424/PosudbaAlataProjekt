package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.Report;
import hr.fer.posudbaAlata.rest.dto.ReportDTO;

import java.util.List;

public interface ReportService {

    Report prijaviKorisnika(Long userId, ReportDTO dto);
    List<Report> dohvatiListuReportaZaKorisnika(Long userId);
}
