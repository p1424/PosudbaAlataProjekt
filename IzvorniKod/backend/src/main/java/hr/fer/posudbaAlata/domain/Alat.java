package hr.fer.posudbaAlata.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.*;

@Entity
@Table
public class Alat {

    @Id @GeneratedValue
    private Long idAlata;
    private String fotografija;     //optional
    private String poveznicaNaProizvod;  //optional
    private int snaga;              //optional
    private boolean akumulatorski;//optional
    private KategorijaAlata kategorija;
    private String vrsta;
    private String podvrsta;        //optional
    private MjestoPrimjeneAlata mjestoPrimjene;

    public Alat() {
    }

    public String getPoveznicaNaProizvod() {
        return poveznicaNaProizvod;
    }

    public void setPoveznicaNaProizvod(String poveznicaNaProizvod) {
        this.poveznicaNaProizvod = poveznicaNaProizvod;
    }

    public Long getIdAlata() {
        return idAlata;
    }

    public void setIdAlata(Long idAlata) {
        this.idAlata = idAlata;
    }

    public String getFotografija() {
        return fotografija;
    }

    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    public int getSnaga() {
        return snaga;
    }

    public void setSnaga(int snaga) {
        this.snaga = snaga;
    }

    public boolean isAkumulatorski() {
        return akumulatorski;
    }

    public void setAkumulatorski(boolean akumulatorski) {
        this.akumulatorski = akumulatorski;
    }

    public KategorijaAlata getKategorija() {
        return kategorija;
    }

    public void setKategorija(KategorijaAlata kategorija) {
        this.kategorija = kategorija;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public String getPodvrsta() {
        return podvrsta;
    }

    public void setPodvrsta(String podvrsta) {
        this.podvrsta = podvrsta;
    }

    public MjestoPrimjeneAlata getMjestoPrimjene() {
        return mjestoPrimjene;
    }

    public void setMjestoPrimjene(MjestoPrimjeneAlata mjestoPrimjene) {
        this.mjestoPrimjene = mjestoPrimjene;
    }

    public Alat(String fotografija, int snaga, boolean akumulatorski, KategorijaAlata kategorija, String poveznica, String vrsta, String podvrsta, MjestoPrimjeneAlata mjestoPrimjene) {
        this.poveznicaNaProizvod = poveznica;
        this.fotografija = fotografija;
        this.snaga = snaga;
        this.akumulatorski = akumulatorski;
        this.kategorija = kategorija;
        this.vrsta = vrsta;
        this.podvrsta = podvrsta;
        this.mjestoPrimjene = mjestoPrimjene;
    }

}
