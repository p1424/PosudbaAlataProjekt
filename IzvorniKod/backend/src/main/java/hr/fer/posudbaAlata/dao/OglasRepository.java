package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Oglas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OglasRepository extends JpaRepository<Oglas, Long> {

    @Query("SELECT o FROM Oglas o WHERE o.isActive = true and o.deleted = false")
    List<Oglas> findActiveOglas();

    @Query("SELECT o FROM Oglas o WHERE o.isActive = false and o.deleted = false")
    List<Oglas> findInActiveOglas();

    @Query("SELECT o FROM Oglas o WHERE o.alat.id = :id and o.deleted = false")
    List<Oglas> findOglasByAlat(@Param("id") Long alatId);

    @Query("SELECT o FROM Oglas o JOIN Alat a ON o.alat.id = a.idAlata WHERE a.vrsta = :v and o.deleted = false")
    List<Oglas> findOglasByVrstaAlata(@Param("v") String vrsta);

    @Query("SELECT o FROM Oglas o WHERE o.nuditelj.userId = :id and o.deleted = false")
    List<Oglas> findOglasByKorisnik(@Param("id") Long korisnikId);

    @Query("SELECT o FROM Oglas o WHERE (o.nuditelj.userId = :id OR o.posuditelj.userId = :id) AND o.isActive = false and o.deleted = false")
    List<Oglas> findInactiveOglasByKorisnik(@Param("id") Long korisnikId);

    @Query(nativeQuery = true, value = "SELECT oglas.*\n" +
            "                FROM oglas NATURAL JOIN alat\n" +
            "                WHERE oglas.alat_id_alata = alat.id_alata" +
            "                AND oglas.is_active = true\n" +
            "                AND oglas.deleted = false\n" +
            "                AND (:kat = -1 OR alat.kategorija = :kat)\n" +
            "                AND (:mjpr = -1 OR alat.mjesto_primjene = :mjpr)\n" +
            "                AND (:vrsta is null OR UPPER(alat.vrsta) = cast(:vrsta AS text))\n" +
            "                AND (:snaga = 0 OR alat.snaga <= :snaga)\n" +
            "                AND (:nakont = -1 OR oglas.nacin_kontakta = :nakont)")
    List<Oglas> findFilteredOglas(
                                  @Param("kat") int kat, //-1
                                  @Param("mjpr") int mjpr, //-1
                                  @Param("vrsta") String vrsta, //null ako nije prisutan
                                  @Param("snaga") int snaga,
                                  @Param("nakont") int naKont); //-1

    @Query(nativeQuery = true, value = "SELECT oglas.*\n" +
            "                FROM oglas NATURAL JOIN alat\n" +
            "                WHERE oglas.alat_id_alata = alat.id_alata\n" +
            "                AND oglas.deleted = false\n" +
            "                AND oglas.is_active = true\n" +
            "                AND alat.akumulatorski = :ak \n" +
            "                AND (:kat = -1 OR alat.kategorija = :kat)\n" +
            "                AND (:mjpr = -1 OR alat.mjesto_primjene = :mjpr)\n" +
            "                AND (:vrsta is null OR UPPER(alat.vrsta) = cast(:vrsta AS text))\n" +
            "                AND (:snaga = 0 OR alat.snaga <= :snaga)\n" +
            "                AND (:nakont = -1 OR oglas.nacin_kontakta = :nakont)")
    List<Oglas> findFilteredOglasWak(@Param("ak") boolean ak,
                                  @Param("kat") int kat, //-1
                                  @Param("mjpr") int mjpr, //-1
                                  @Param("vrsta") String vrsta, //null ako nije prisutan
                                  @Param("snaga") int snaga,
                                  @Param("nakont") int naKont); //-1
}
