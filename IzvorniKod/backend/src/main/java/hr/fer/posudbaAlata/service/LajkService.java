package hr.fer.posudbaAlata.service;

public interface LajkService {

    /**
     *  Metoda lajkanja
     *
     * @param idKomentara id komentara nad kojim se lajka/dislajka
     * @return
     *        - true -> lajkano
     *        - false -> dislajkano
     */
    boolean promjeniStanjeLajka(Long idKomentara);
}
