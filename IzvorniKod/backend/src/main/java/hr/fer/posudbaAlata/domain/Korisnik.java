package hr.fer.posudbaAlata.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@Entity
@Table
public class Korisnik implements Comparable<Korisnik>, UserDetails {

	@Id
	@SequenceGenerator(
			name="userOnly_sequence",
			sequenceName="userOnly_sequence",
			allocationSize = 1
	)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "userOnly_sequence"
	)
	@Column(name = "userId")
	private Long userId;

	@Column(unique = true, name = "username")
	@NotNull
	@NotBlank(message = "Ime nemože biti prazno!")
	@Length(min= 3, max = 32, message = "Korisničko ime je predugačako ili prekratko!")
	@Pattern(regexp = "^[A-Za-zĆ-ž\\d][^\'\"]*$")
	private String username;

	@NotBlank(message = "Ime ne može biti prazno")
	@Pattern(regexp = "^[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*$", message = "Ime je neispravano ")
	@Length(max = 32, message = "Ime je predugačako!")
	private String firstName;

	@NotBlank(message = "Prezime ne može biti prazno")
	@Pattern(regexp = "^[A-Za-zĆ-ž]([A-Za-zĆ-ž -]+| )*$", message = "Prezime je neispravno ")
	@Length(max = 32, message = "Prezime je predugačako!")
	private String lastName;

	@Column(name = "role")
	private UserRole role;

	@Column(unique = true, name = "email")
	@NotNull
	@NotBlank(message = "Email ne može biti prazan")
	@Length(max = 32, message = "Email je predugačak!")
	@Email
	private String email;

	@Column(name = "phone")
	@NotBlank(message = "Broj telefona ne može biti prazan")
	@Pattern(regexp = "^([\\+]|\\d)\\d{7,12}$", message = "Broj telefona je neispravan ")
	private String phone;

	@Column(name = "address")
	@NotBlank(message = "Adresa ne može biti prazna")
	@Length(max = 64, message = "Adresa je predugačka!")
	@Pattern(regexp = "^[A-Za-zĆ-ž]([A-Za-zĆ-ž ]+| )*\\d{1,4}$", message = "Adresa nije ispravno zadana! Format:\"<naziv ulice> <kućni broj>\"")
	private String address;

	@Column(name = "city")
	@NotBlank(message = "Grad ne može biti prazan")
	@Pattern(regexp = "^[A-Za-zĆ-ž]([A-Za-zĆ-ž ]+| )*$", message = "Grad nije ispravno zadan!")
	private String city;

	@Column(name = "password")
	@Length(min = 8)
	@NotBlank(message = "Lozinka ne može biti prazna")
	private String password;

	private boolean deleted;

	@OneToOne(cascade=CascadeType.ALL) @JsonIgnore
	private DistanceModel distanceModel;

	public DistanceModel getDistanceModel() {
		return distanceModel;
	}

	public void setDistanceModel(DistanceModel distanceModel) {
		this.distanceModel = distanceModel;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Transient
	private Boolean locked;
	@Transient
	private Boolean enabled;

	public Long getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}


	@Override @Transient @JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}


	@Override @Transient @JsonIgnore
	public boolean isAccountNonLocked() {
		return !locked;
	}


	@Override @Transient @JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}


	@Override @Transient @JsonIgnore
	public boolean isEnabled() {
		return enabled;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String name) {
		this.firstName = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	@Override @Transient @JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		final SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.name());
		return Collections.singletonList(simpleGrantedAuthority);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Korisnik{" +
				"userId=" + userId +
				", username='" + username + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", role=" + role +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				", address='" + address + '\'' +
				", city='" + city + '\'' +
				", password='" + password + '\'' +
				'}';
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public Korisnik(){
		this.role = UserRole.KORISNIK;
		locked = false;
		enabled = false;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String s) {
		this.email = s;
	}

	@Override
	public int compareTo(Korisnik o) {
		return this.getUserId().compareTo(o.getUserId());
	}

	public Korisnik(String username, String firstName, String lastName, String email, String phone, String address, String city, String password) {
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.city = city;
		this.password = password;

		this.role = UserRole.KORISNIK;
		locked = false;
		enabled = true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Korisnik korisnik = (Korisnik) o;
		return Objects.equals(userId, korisnik.userId) && Objects.equals(username, korisnik.username) && Objects.equals(firstName, korisnik.firstName) && Objects.equals(lastName, korisnik.lastName) && role == korisnik.role && Objects.equals(email, korisnik.email) && Objects.equals(phone, korisnik.phone) && Objects.equals(address, korisnik.address) && Objects.equals(city, korisnik.city) && Objects.equals(password, korisnik.password) && Objects.equals(locked, korisnik.locked) && Objects.equals(enabled, korisnik.enabled);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId, username, firstName, lastName, role, email, phone, address, city, password, locked, enabled);
	}
}
