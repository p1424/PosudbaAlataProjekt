package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.rest.dto.OglasAndAlatDTO;
import hr.fer.posudbaAlata.domain.Oglas;
import hr.fer.posudbaAlata.rest.dto.OglasAndInteresDTO;
import hr.fer.posudbaAlata.rest.dto.OglasFilterDTO;

import java.util.List;

public interface OglasService {

    Oglas createOglas(OglasAndAlatDTO oglasReq);
    OglasAndInteresDTO dohvatiOglasPoId(Long idOglas);
    List<Oglas> dohvatiSveOglase();
    List<Oglas> dohvatiOglasePoIduKorisnika(Long idKorisnik);
    List<Oglas> dohvatiOglasKojiSadrziAlatId(Long idAlata);
    List<Oglas> dohvatiOglaseKojiSadrzeVrsteAlata(String vrstaAlata);
    List<Oglas> dohvatiAktivneOglase();
    List<Oglas> dohvatiNeAktivneOglase();
    List<OglasAndInteresDTO> dohvatiOglasePoInteresu();
    boolean obrisiOglas(Long idOglas);
    boolean napraviPosudbuOglas(Long oglasId, Long traziteljId);
    Oglas oglasModify(Long id, OglasAndAlatDTO dto);
    List<OglasAndInteresDTO> dohvatiOglasePoKorisniku();
    List<Oglas> dohvbatiNeaktivneOglasePoKorisniku();
    InteresZaPosudbu zabiljeziInteresZaOglas(Long oglasId);
    Oglas povratOglas(Long oglasId);
    List<Oglas> dohvatiAktivneOglaseFilter(OglasFilterDTO oglasFilterDTO);

    boolean otkaziInteresOglas(Long oglasId);
}
