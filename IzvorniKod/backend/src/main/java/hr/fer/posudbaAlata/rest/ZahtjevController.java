package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Zahtjev;
import hr.fer.posudbaAlata.rest.dto.KomentarDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevAlatDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevAndInteresDTO;
import hr.fer.posudbaAlata.rest.dto.ZahtjevFilterDTO;
import hr.fer.posudbaAlata.service.ZahtjevService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ZahtjevController {

    private final ZahtjevService zahtjevService;

    @Autowired
    public ZahtjevController(ZahtjevService zs){
        this.zahtjevService = zs;
    }

    @PostMapping("/zahtjev")
    public Zahtjev dodajZahtjev(@RequestBody ZahtjevAlatDTO zdto){
        return zahtjevService.createZahtjev(zdto);
    }

    @GetMapping("/zahtjev/all")
    public List<Zahtjev> dohvatiSveZahtjeve() {
        return zahtjevService.dohvatiSve();
    }

    @PostMapping("/zahtjev{zahtjevId}/komentiraj")
    public boolean komentiraj(@PathVariable("zahtjevId") Long zahtjevId, @RequestBody KomentarDTO komentarDTO){
        return zahtjevService.komentiraj(zahtjevId, komentarDTO);
    }

    @DeleteMapping("/zahtjev{zahtjevId}/komentar{komentarid}/delete")
    public boolean obrisiKomentar(@PathVariable("zahtjevId") Long zahtjevId, @PathVariable("komentarid") Long komentarId){
        return zahtjevService.obrisiKomentar(zahtjevId, komentarId);
    }

    @GetMapping("/zahtjev/korisnik")
    public List<ZahtjevAndInteresDTO> dohvatiZahtjevePoKorisniku() {
        return zahtjevService.dohvatiZahtjevePoKorisniku();
    }

    @GetMapping("/zahtjev/korisnik/inactive")
    public List<Zahtjev> dohvatiNeaktivneZahtjevePoKorisniku() {
        return zahtjevService.dohvatiNeaktivneZahtjevePoKorisniku();
    }

    @GetMapping("zahtjev/korisnik/interes")
    public List<ZahtjevAndInteresDTO> dohvatiZahtjevePoInteresu() {
        return zahtjevService.dohvatiZahtjevePoInteresu();
    }

    @DeleteMapping("/zahtjev/delete/{id}")
    public void obrisiZahtjev(@PathVariable("id") Long id) {
        zahtjevService.obrisiZahtjev(id);
    }

    @GetMapping("/zahtjev/active")
    public List<Zahtjev> dohvatiAktivneZahtjeve()  {
        return zahtjevService.dohvatiAktivneZahtjeve();
    }

    //filter
    @PostMapping("/zahtjev/active")
    public List<Zahtjev> dohvatiAktivneZahtjeveFilter(@RequestBody ZahtjevFilterDTO zahtjevFilterDTO)  {
        return zahtjevService.dohvatiAktivneZahtjeveFilter(zahtjevFilterDTO);
    }

    @PutMapping("/zahtjev/modify/{idZahtjeva}")
    public Zahtjev zahtjevModify(@PathVariable("idZahtjeva") Long id, @RequestBody ZahtjevAlatDTO dto){
        return zahtjevService.modifyZahtjevById(id, dto);
    }

    @GetMapping("/zahtjev/{id}")
    public ZahtjevAndInteresDTO getZahtjevById(@PathVariable("id") Long id) {
        return zahtjevService.getZahtjevByZahtjevId(id);
    }

    @PostMapping("/posudba/zahtjev/{id}")
    public InteresZaPosudbu zabiljeziInteresZaZahtjev(@PathVariable("id") Long id) {
        return zahtjevService.zabiljeziInteresZaZahtjev(id);
    }

    @PostMapping("/posudba/zahtjev/{zahtjevId}/{ponudacId}")
    public boolean napraviPosudbuZahtjev(@PathVariable("zahtjevId") Long zId, @PathVariable("ponudacId") Long pId) {
        return zahtjevService.napraviPosudbuZahtjev(zId, pId);
    }

    @PostMapping("/zahtjev/povrat/{zahtjevId}")
    public Zahtjev napraviPovratZahtjev(@PathVariable("zahtjevId") Long zahtjevId) {
        return zahtjevService.povratZahtjev(zahtjevId);
    }

    @GetMapping("zahtjev/interes/cancel/{id}")
    public boolean otkaziInteresZahtjev(@PathVariable("id") Long zahtjevId) {
        return zahtjevService.otkaziInteresZahtjev(zahtjevId);
    }

}
