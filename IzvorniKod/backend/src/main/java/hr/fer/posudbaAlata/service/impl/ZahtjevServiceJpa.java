package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.ZahtjevRepository;
import hr.fer.posudbaAlata.domain.*;
import hr.fer.posudbaAlata.rest.LoginController;
import hr.fer.posudbaAlata.rest.dto.*;
import hr.fer.posudbaAlata.service.*;
import hr.fer.posudbaAlata.utils.IllegalAccess;
import hr.fer.posudbaAlata.utils.NotFoundException;
import hr.fer.posudbaAlata.utils.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ZahtjevServiceJpa implements ZahtjevService {

    private final UserService userService;
    private final AlatService alatService;
    private final ZahtjevRepository zahtjevRepository;
    private final KomentarService komentarService;
    private final InteresService interesService;


    @Autowired
    public ZahtjevServiceJpa(UserService userService, AlatService alatService, ZahtjevRepository zahtjevRepository, KomentarService komentarService, InteresService is) {
        this.userService = userService;
        this.alatService = alatService;
        this.zahtjevRepository = zahtjevRepository;
        this.komentarService = komentarService;
        this.interesService = is;
    }

    @Override
    public Zahtjev createZahtjev(ZahtjevAlatDTO dto) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trazitelj = userService.findUserByUsername(currentUser.getUsername());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        if (trazitelj.isEmpty()) {
            throw new IllegalStateException("Error na serveru");
        }

        Zahtjev zahtjev = new Zahtjev();

        if(!(dto.getVrsta() == null || dto.getVrsta().isEmpty() || dto.getVrsta().isBlank())) {
            Alat alat = new Alat();

            alat.setAkumulatorski(dto.isAkumulatorski());
            alat.setFotografija(dto.getFotografija());
            alat.setKategorija(dto.getKategorijaAlata());
            alat.setPodvrsta(dto.getPodvrsta());
            alat.setMjestoPrimjene(dto.getMjestoPrimjeneAlata());
            alat.setPoveznicaNaProizvod(dto.getPoveznicaNaProizvod());
            alat.setSnaga(dto.getSnaga());
            alat.setVrsta(dto.getVrsta());
            if (alatService.dodajAlat(alat) == null) {
                throw new IllegalStateException("Alat nije dodan!");
            }
            zahtjev.setAlat(alat);
        }
        else
            zahtjev.setAlat(null);


        zahtjev.setDatumZahtjeva(date);
        zahtjev.setMaxUdaljenost(dto.getMaxUdaljenost());
        zahtjev.setTekstZahtjeva(dto.getTekstZahtjeva());
        zahtjev.setFotografija(dto.getFotografija());
        zahtjev.setTrazitelj(trazitelj.get());
        zahtjev.setNacinKontakta(dto.getNacinKontakta());
        zahtjev.setActive(true);
        zahtjev.setPovratTrazitelj(true);
        zahtjev.setPovratPonudac(true);
        zahtjev.setDeleted(false);

        return zahtjevRepository.save(zahtjev);
    }

    @Override
    public List<Zahtjev> dohvatiSve(){

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");
        if(!(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))){
            throw new IllegalAccess("Za dohvat svih zahtjeva potrebno je biti admin!");
        }

        return zahtjevRepository.findAll().stream().filter(z -> !z.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public boolean komentiraj(Long idZahtjeva, KomentarDTO komentarDTO) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> autorKomentara = userService.findUserByUsername(currentUser.getUsername());
        Optional<Zahtjev> zahtjev = zahtjevRepository.findById(idZahtjeva);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        //stvori komentar i dodaj ga na zahtjev

        Komentar komentar = new Komentar();

        if(zahtjev.isEmpty() || autorKomentara.isEmpty() || zahtjev.get().isDeleted())
            throw new NotFoundException("Zahtjev ili Autor moraju biti postojeci!");

        komentar.setDatumKomentara(date);
        komentar.setTekstKomentara(komentarDTO.getTekstKomentara());
        komentar.setNaZahtjev(zahtjev.get());
        komentar.setOstavljaKorisnik(autorKomentara.get());
        komentar.setCurrentUserLiked(false);

        if(komentarService.dodajKomentar(komentar)){

            zahtjev.get().setKomentar(komentar);
            zahtjevRepository.save(zahtjev.get());
            return true;
        }
        return false;
    }

    @Override
    public boolean obrisiKomentar(Long idZahtjeva, Long idKomentara) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        var zahtjev = zahtjevRepository.findById(idZahtjeva);
        var komentar = komentarService.dohvatiKomentarById(idKomentara);

        if(zahtjev.isEmpty() || komentar.isEmpty() || zahtjev.get().isDeleted() || komentar.get().isDeleted())
            throw new NotFoundException("Zahtjev i/ili komentar ne postoji!");

        boolean noCommentInZahtjevError = true;
        for(Komentar kom : zahtjev.get().getListaKomentara()) {
            if(kom.getIdKomentara().equals(komentar.get().getIdKomentara())) {
                noCommentInZahtjevError = false;
                break;
            }
        }

        if(noCommentInZahtjevError) {
            throw new NotFoundException("Ne postoji komentar s identifikatorom " + idKomentara + " u ovome zahtjevu");
        }

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        if(!komentar.get().getOstavljaKorisnik().getUserId().equals(trenutniKorisnik.get().getUserId())
                && !(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN")) ) {
            throw new RequestDeniedException("Ne mozete brisati tude komentare");
        }

        if(komentarService.obrisiKomentar(idKomentara)){
            zahtjev.get().deleteKomentar(komentar.get());
            komentarService.delete(komentar.get());
            zahtjevRepository.save(zahtjev.get());
            return true;
        }
        return false;
    }

    @Override
    public List<Zahtjev> dohvatiZahtjevePoIduKorisnika(Long idKorisnik){
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        return zahtjevRepository.getZahtjevByUserId(idKorisnik);
    }

    @Override
    public void refresh(Zahtjev zahtjev) {
        zahtjevRepository.save(zahtjev);
    }

    @Override
    public Zahtjev dohvatiZahtjevSIdKomentara(Long idKomentara) {

       return zahtjevRepository.findAll().stream().filter(zahtjev -> !zahtjev.getListaKomentara().isEmpty()).filter(zahtjev -> {
            if(zahtjev.getListaKomentara().stream().filter(kom -> kom.getIdKomentara().equals(idKomentara)).collect(Collectors.toList()).size() >= 1)
                return true;
            return false;
        }).collect(Collectors.toList()).stream().findFirst().orElseThrow();

    }

    @Override
    public List<ZahtjevAndInteresDTO> dohvatiZahtjevePoKorisniku() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        List<Zahtjev> listaZahtjeva = zahtjevRepository.getZahtjevByUserId(trenutniKorisnik.get().getUserId());
//        listaZahtjeva.addAll(interesService.dohvatiZahtjeveIzInteresa(trenutniKorisnik.get().getUserId()));

        if(listaZahtjeva.isEmpty()) {
            throw new NotFoundException("Korisnik nema zahtjeve");
        }

        List<ZahtjevAndInteresDTO> list = new LinkedList<>();

        for(Zahtjev z : listaZahtjeva) {

            if(z.isDeleted())
                continue;

            List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoZahtjevu(z.getIdZahtjeva());

            ZahtjevAndInteresDTO dto = new ZahtjevAndInteresDTO(z, listaZainteresiranih);

            list.add(dto);
        }

        return list;
    }

    @Override
    public List<Zahtjev> dohvatiNeaktivneZahtjevePoKorisniku() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        List<Zahtjev> list = zahtjevRepository.getInActiveZahtjevByUserId(trenutniKorisnik.get().getUserId());

        if(list.isEmpty()) {
            throw new NotFoundException("Trenutno nemate neaktivne zahtjeve");
        } else return list;
    }

    @Override
    public List<Zahtjev> dohvatiAktivneZahtjeve() {
        List<Zahtjev> listaZahtjeva = zahtjevRepository.getActive();


        if(listaZahtjeva.isEmpty()) {
            throw new NotFoundException("Trenutno nema aktivnih zahtjeva");
        }
        if(LoginController.authorize() != null && !listaZahtjeva.isEmpty())
            for (Zahtjev zahtjev : listaZahtjeva) {
                checkCurrentUserLike(zahtjev);
            }

        return listaZahtjeva;
    }

    @Override
    public List<ZahtjevAndInteresDTO> dohvatiZahtjevePoInteresu() {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if(trenutniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        List<Zahtjev> listaZahtjeva = interesService.dohvatiZahtjeveIzInteresa(trenutniKorisnik.get().getUserId());

        List<ZahtjevAndInteresDTO> dtoList = new LinkedList<>();

        for(Zahtjev z : listaZahtjeva) {
            if(!z.isDeleted()) {
                ZahtjevAndInteresDTO dto = new ZahtjevAndInteresDTO(z, null);
                dtoList.add(dto);
            }
        }

        if(dtoList.isEmpty()) {
            throw new NotFoundException("Trenutno niste zainteresirani niti za jedan zahtjev");
        }

        return dtoList;
    }

    @Override
    public boolean obrisiZahtjev(Long zahtjevId) {
        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Zahtjev> z = zahtjevRepository.findById(zahtjevId);

        if(z.isPresent() && !z.get().isDeleted()) {

            Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

            if (trenutniKorisnik.isEmpty())
                throw new IllegalStateException("Error na serveru");

            if(!z.get().getTrazitelj().getUserId().equals(trenutniKorisnik.get().getUserId()) && !(currentUser.getAuthorities().stream().findFirst().get().toString().equals("ADMIN"))) {
                throw new RequestDeniedException("Ne mozete brisati tude zahtjeve");
            }

            z.get().setDeleted(true);
            zahtjevRepository.save(z.get());
            return true;

        } else {
            throw new NotFoundException("Zahtjev s identifikatorom " + zahtjevId + " ne postoji");
        }
    }

    @Override
    public Zahtjev modifyZahtjevById(Long id, ZahtjevAlatDTO dto) {

        User currentUser = LoginController.authorize();
        if( currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> trazitelj = userService.findUserByUsername(currentUser.getUsername());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        if (trazitelj.isEmpty()) {
            throw new IllegalStateException("Error na serveru");
        }

        Zahtjev zahtjev = zahtjevRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException("Id zahtjeva ne postoji");
        });

        if(zahtjev.isDeleted())
            throw new IllegalAccess("Zahtjev s id: " + zahtjev.getIdZahtjeva() + " ne postoji!");

        if(!(dto.getVrsta() == null || dto.getVrsta().isEmpty() || dto.getVrsta().isBlank())) {
            Alat alat = new Alat();

        if(!zahtjev.getTrazitelj().getUserId().equals(trazitelj.get().getUserId())) {
            throw new RequestDeniedException("Ne mozete modificirati tude zahtjeve");
        }

            alat.setAkumulatorski(dto.isAkumulatorski());
            alat.setFotografija(dto.getFotografija());
            alat.setKategorija(dto.getKategorijaAlata());
            alat.setPodvrsta(dto.getPodvrsta());
            alat.setMjestoPrimjene(dto.getMjestoPrimjeneAlata());
            alat.setPoveznicaNaProizvod(dto.getPoveznicaNaProizvod());
            alat.setSnaga(dto.getSnaga());
            alat.setVrsta(dto.getVrsta());
            if (alatService.dodajAlat(alat) == null) {
                throw new IllegalStateException("Alat nije dodan!");
            }
            zahtjev.setAlat(alat);
        }
        else
            zahtjev.setAlat(null);



        zahtjev.setDatumZahtjeva(date);
        zahtjev.setMaxUdaljenost(dto.getMaxUdaljenost());
        zahtjev.setTekstZahtjeva(dto.getTekstZahtjeva());
        zahtjev.setFotografija(dto.getFotografija());
        zahtjev.setTrazitelj(trazitelj.get());
        zahtjev.setNacinKontakta(dto.getNacinKontakta());
        zahtjev.setActive(true);



        return zahtjevRepository.save(zahtjev);
    }

    @Override
    public ZahtjevAndInteresDTO getZahtjevByZahtjevId(Long zahtjevId) {
       Optional<Zahtjev> zahtjev = zahtjevRepository.findById(zahtjevId);

       if(zahtjev.isEmpty() || zahtjev.get().isDeleted()) {
           throw new NotFoundException("Ne postoji zahtjev s identifikatorom " + zahtjevId);
       }

       List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoZahtjevu(zahtjevId);

       return new ZahtjevAndInteresDTO(zahtjev.get(), listaZainteresiranih);
    }

    @Override
    public InteresZaPosudbu zabiljeziInteresZaZahtjev(Long zahtjevId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> zainteresiraniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (zainteresiraniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        Optional<Zahtjev> zahtjev = zahtjevRepository.findById(zahtjevId);

        if(zahtjev.isEmpty() || zahtjev.get().isDeleted()) {
            throw new NotFoundException("Ne postoji zahtjev s identifikatorom " + zahtjevId);
        }

        if(zahtjev.get().getTrazitelj().equals(zainteresiraniKorisnik.get())) {
            throw new RequestDeniedException("Ne mozete pokazati interes za Vaš zahtjev.");
        }

        if(!zahtjev.get().isActive()) {
            throw new RequestDeniedException("Zahtjev trenutno nije aktivan");
        }

        List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoZahtjevu(zahtjevId);

        if(listaZainteresiranih.contains(zainteresiraniKorisnik.get())) {
            throw new RequestDeniedException("Vec ste pokazali interes za ovaj zahtjev");
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String sdate = LocalDate.now().format(formatter);
        LocalDate date = LocalDate.parse(sdate, formatter);

        InteresZaPosudbu interes = new InteresZaPosudbu(date,
                zainteresiraniKorisnik.get(),
                null,
                zahtjev.get(),
                Status.U_OBRADI,
                false);

        return interesService.saveInteres(interes);
    }

    @Override
    public boolean napraviPosudbuZahtjev(Long zahtjevId, Long ponudacId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Zahtjev> zahtjev = zahtjevRepository.findById(zahtjevId);

        if(zahtjev.isEmpty() || zahtjev.get().isDeleted()) {
            throw new NotFoundException("Ne postoji zahtjev s identifikatorom " + zahtjevId);
        }

        Optional<Korisnik> trazitelj = userService.findUserByUsername(currentUser.getUsername());

        if(!zahtjev.get().getTrazitelj().equals(trazitelj.get())) {
            throw new IllegalAccess("Nemate prava pristupa zahtjevima drugih korisnika");
        }

        Optional<Korisnik> ponudac = userService.findByid(ponudacId);

        List<Korisnik> listaZainteresiranih = interesService.dohvatiZainteresiraneKorisnikePoZahtjevu(zahtjevId);

        if(ponudac.isEmpty() || !listaZainteresiranih.contains(ponudac.get())) {
            throw new NotFoundException("Korisnik ne postoji ili nije zainteresiran za zahtjev");
        }

        List<InteresZaPosudbu> interesi = interesService.getAllZahtjev(zahtjevId);

        for(InteresZaPosudbu i : interesi) {
            i.setDeleted(true);
            interesService.saveInteres(i);
        }

        zahtjev.get().setPonudac(ponudac.get());
        zahtjev.get().setActive(false);
        zahtjev.get().setPovratPonudac(false);
        zahtjev.get().setPovratTrazitelj(false);
        zahtjevRepository.save(zahtjev.get());

        return true;
    }

    @Override
    public Zahtjev povratZahtjev(Long zahtjevId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Zahtjev> zahtjev = zahtjevRepository.findById(zahtjevId);

        if(zahtjev.isEmpty() || zahtjev.get().isDeleted()) {
            throw new NotFoundException("Ne postoji zahtjev s identifikatorom " + zahtjevId);
        }

        Optional<Korisnik> trenutniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        //trazitelj napravi povrat prije ponudaca
        if(zahtjev.get().getTrazitelj().equals(trenutniKorisnik.get())) {

            zahtjev.get().setPovratTrazitelj(true);

        //ponudac napravi povrat prije trazitelja
        } else if(zahtjev.get().getPonudac().equals(trenutniKorisnik.get())) {

            zahtjev.get().setPovratPonudac(true);

        } else {
            throw new RequestDeniedException("Korisnik nema nikakve veze s zahtjevom");
        }

        if(zahtjev.get().isPovratPonudac() && zahtjev.get().isPovratTrazitelj()) {
            zahtjev.get().setActive(true);
            zahtjev.get().setPonudac(null);
        }

        return zahtjevRepository.save(zahtjev.get());

    }

    @Override
    public List<Zahtjev> dohvatiAktivneZahtjeveFilter(ZahtjevFilterDTO zahtjevFilterDTO) {

        Boolean akumulatorski = null;
        String vrsta = null;

        if(zahtjevFilterDTO.getAkumulatorski() != null && !zahtjevFilterDTO.getAkumulatorski().isEmpty() && !zahtjevFilterDTO.getAkumulatorski().isBlank() && !zahtjevFilterDTO.getAkumulatorski().equals("")){
            if(zahtjevFilterDTO.getAkumulatorski().equals("true")){
                akumulatorski=true;
            }
            else akumulatorski=false;
        }

        if(zahtjevFilterDTO.getVrsta() != null) vrsta = zahtjevFilterDTO.getVrsta().toUpperCase();

        if(akumulatorski == null)
            return zahtjevRepository.findFilteredZahtjev(zahtjevFilterDTO.getKategorijaAlata(), zahtjevFilterDTO.getMjestoPrimjeneAlata(), vrsta, zahtjevFilterDTO.getSnaga(), zahtjevFilterDTO.getNacinKontakta());
        else
            return zahtjevRepository.findFilteredZahtjevWak(akumulatorski,zahtjevFilterDTO.getKategorijaAlata(), zahtjevFilterDTO.getMjestoPrimjeneAlata(), vrsta, zahtjevFilterDTO.getSnaga(), zahtjevFilterDTO.getNacinKontakta());
    }

    @Override
    public boolean otkaziInteresZahtjev(Long zahtjevId) {
        User currentUser = LoginController.authorize();
        if(currentUser == null)
            throw new IllegalAccess("Nevaljali token iil korisnik nije ulogiran!");

        Optional<Korisnik> zainteresiraniKorisnik = userService.findUserByUsername(currentUser.getUsername());

        if (zainteresiraniKorisnik.isEmpty())
            throw new IllegalStateException("Error na serveru");

        InteresZaPosudbu interes = interesService.dohvatiPoZahtjevuIKorisniku(zahtjevId, zainteresiraniKorisnik.get().getUserId());

        if(interes == null || interes.isDeleted()) {
            throw new NotFoundException("Zahtjev ne postoji ili niste zainteresirani za njega");
        }

        if(!interes.getZainteresiraniKorisnik().getUserId().equals(zainteresiraniKorisnik.get().getUserId())) {
            throw new IllegalAccess("Nemate pravo otkazati interes drugih korisnika");
        }

        interes.setDeleted(true);
        interesService.saveInteres(interes);

        return true;
    }


    /**
     *metoda koja gleda je li trenutni korisnik lajkao neki komentar nad zatjevom
     * i ako je postavlja varijablu currentUserLiked u true
     *
     * @param zahtjev
     * @return
     */
    private void checkCurrentUserLike(Zahtjev zahtjev){

        Korisnik korisnik = userService.findUserByUsername(LoginController.authorize().getUsername()).orElseThrow();

        if(zahtjev != null && !zahtjev.getListaKomentara().isEmpty()) {

            System.out.println(zahtjev);
            System.out.println(zahtjev.getListaKomentara());

            for(int i=0; i<zahtjev.getListaKomentara().size();i++){

                for(int j=0;j<zahtjev.getListaKomentara().get(i).getLajkovi().size();j++){

                    if(zahtjev.getListaKomentara().get(i).getLajkovi().get(j).getOstavljaKorisnik().getUserId() == (korisnik.getUserId())){

                        zahtjev.getListaKomentara().get(i).setCurrentUserLiked(true);
                        komentarService.dodajKomentar(zahtjev.getListaKomentara().get(i));
                    }

                }

            }

            zahtjevRepository.save(zahtjev);
        }
    }
}
