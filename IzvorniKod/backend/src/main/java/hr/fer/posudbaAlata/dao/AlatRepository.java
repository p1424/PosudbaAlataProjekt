package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Alat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface AlatRepository extends JpaRepository<Alat, Long> {
    Optional<Alat> findById(Long id);
}
