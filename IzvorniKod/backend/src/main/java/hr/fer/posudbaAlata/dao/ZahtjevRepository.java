package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Zahtjev;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZahtjevRepository extends JpaRepository<Zahtjev, Long> {

    @Query("SELECT z FROM Zahtjev z WHERE trazitelj.userId = :id and z.deleted = false")
    List<Zahtjev> getZahtjevByUserId(@Param("id") Long userId);

    @Query("SELECT z FROM Zahtjev z WHERE (z.trazitelj.userId = :id OR z.ponudac.userId = :id) AND z.isActive = false and z.deleted=false")
    List<Zahtjev> getInActiveZahtjevByUserId(@Param("id") Long userId);

    @Query("SELECT z FROM Zahtjev z WHERE z.isActive = true and z.deleted = false")
    List<Zahtjev> getActive();

    @Query(nativeQuery = true, value = "SELECT zahtjev.* \n" +
            "FROM zahtjev LEFT JOIN alat ON(zahtjev.alat_id_alata = alat.id_alata)\n" +
            "WHERE zahtjev.deleted = false\n" +
            "AND zahtjev.is_active = true\n" +
            "                AND (:kat = -1 OR alat.kategorija = :kat)\n" +
            "                AND (:mjpr = -1 OR alat.mjesto_primjene = :mjpr)\n" +
            "                AND (:vrsta is null OR UPPER(alat.vrsta) = cast(:vrsta AS text))\n" +
            "                AND (:snaga = 0 OR (alat_id_alata IS NOT NULL AND alat.snaga <= :snaga))\n" +
            "                AND (:nakont = -1 OR zahtjev.nacin_kontakta = :nakont)"
            )
    List<Zahtjev> findFilteredZahtjev(
            @Param("kat") int kat, //-1
            @Param("mjpr") int mjpr, //-1
            @Param("vrsta") String vrsta, //null ako nije prisutan
            @Param("snaga") int snaga,
            @Param("nakont") int naKont);


    @Query(nativeQuery = true, value = "SELECT zahtjev.* \n" +
            "FROM zahtjev LEFT JOIN alat ON(zahtjev.alat_id_alata = alat.id_alata)\n" +
            "WHERE zahtjev.deleted = false\n" +
            "AND zahtjev.is_active = true\n" +
            "                AND alat.akumulatorski = :ak \n" +
            "                AND (:kat = -1 OR alat.kategorija = :kat)\n" +
            "                AND (:mjpr = -1 OR alat.mjesto_primjene = :mjpr)\n" +
            "                AND (:vrsta is null OR UPPER(alat.vrsta) = cast(:vrsta AS text))\n" +
            "                AND (:snaga = 0 OR (alat_id_alata IS NOT NULL AND alat.snaga <= :snaga))\n" +
            "                AND (:nakont = -1 OR zahtjev.nacin_kontakta = :nakont)")
            List<Zahtjev> findFilteredZahtjevWak(@Param("ak") boolean ak,
                @Param("kat") int kat, //-1
                @Param("mjpr") int mjpr, //-1
                @Param("vrsta") String vrsta, //null ako nije prisutan
                 @Param("snaga") int snaga,
                @Param("nakont") int naKont); //-1
}
