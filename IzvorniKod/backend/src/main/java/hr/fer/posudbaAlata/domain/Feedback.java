package hr.fer.posudbaAlata.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Feedback {

    @Id @GeneratedValue
    private Long feedbackId;

    private LocalDate datumFeedbacka;

    private String tekstFeedbacka;

    private int ocjena;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik zaKorisnika;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik odKorisnika;

    public LocalDate getDatumFeedbacka() {
        return datumFeedbacka;
    }

    public void setDatumFeedbacka(LocalDate datumFeedbacka) {
        this.datumFeedbacka = datumFeedbacka;
    }

    public String getTekstFeedbacka() {
        return tekstFeedbacka;
    }

    public void setTekstFeedbacka(String tekstFeedbacka) {
        this.tekstFeedbacka = tekstFeedbacka;
    }

    public int getOcjena() {
        return ocjena;
    }

    public void setOcjena(int ocjena) {
        this.ocjena = ocjena;
    }

    public Korisnik getZaKorisnika() {
        return zaKorisnika;
    }

    public void setZaKorisnika(Korisnik zaKorisnika) {
        this.zaKorisnika = zaKorisnika;
    }

    public Korisnik getOdKorisnika() {
        return odKorisnika;
    }

    public void setOdKorisnika(Korisnik odKorisnika) {
        this.odKorisnika = odKorisnika;
    }

    public Feedback () { }

    public Feedback(LocalDate datumFeedbacka, String tekstFeedbacka, int ocjena, Korisnik zaKorisnika, Korisnik odKorisnika) {
        this.datumFeedbacka = datumFeedbacka;
        this.tekstFeedbacka = tekstFeedbacka;
        this.ocjena = ocjena;
        this.zaKorisnika = zaKorisnika;
        this.odKorisnika = odKorisnika;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "feedbackId=" + feedbackId +
                ", datumFeedbacka=" + datumFeedbacka +
                ", tekstFeedbacka='" + tekstFeedbacka + '\'' +
                ", ocjena=" + ocjena +
                ", zaKorisnika=" + zaKorisnika +
                ", odKorisnika=" + odKorisnika +
                '}';
    }
}
