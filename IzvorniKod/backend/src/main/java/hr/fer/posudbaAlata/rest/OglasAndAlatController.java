package hr.fer.posudbaAlata.rest;


import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Oglas;
import hr.fer.posudbaAlata.rest.dto.OglasAndAlatDTO;
import hr.fer.posudbaAlata.rest.dto.OglasAndInteresDTO;
import hr.fer.posudbaAlata.rest.dto.OglasFilterDTO;
import hr.fer.posudbaAlata.service.AlatService;
import hr.fer.posudbaAlata.service.OglasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class OglasAndAlatController {

    private OglasService oglasService;
    private AlatService alatService;

    @Autowired
    public OglasAndAlatController(OglasService o, AlatService a){
        this.oglasService = o;
        this.alatService = a;
    }

    @PostMapping("/oglas")
    public Oglas dodajOglas(@RequestBody OglasAndAlatDTO oglasAndAlatDTO){
        return oglasService.createOglas(oglasAndAlatDTO);
    }

    @GetMapping("/oglas/{id}")
    public OglasAndInteresDTO dohvatiOglas(@PathVariable("id") Long id) {
        return oglasService.dohvatiOglasPoId(id);
    }

    @GetMapping("/oglas/all")
    public List<Oglas> dohvatiSveOglase() {
        return oglasService.dohvatiSveOglase();
    }

    @DeleteMapping("/oglas/delete/{id}")
    public boolean obrisiOglas(@PathVariable("id") Long id) {
        return oglasService.obrisiOglas(id);
    }

    @GetMapping("/oglas/active")
    public List<Oglas> dohvatiAktivneOglase() {
        return oglasService.dohvatiAktivneOglase();
    }

    //filter
    @PostMapping("/oglas/active")
    public List<Oglas> dohvatiAktivneOglaseFilter(@RequestBody OglasFilterDTO oglasFilterDTO) {
        return oglasService.dohvatiAktivneOglaseFilter(oglasFilterDTO);
    }

    @GetMapping("/oglas/inactive")
    public List<Oglas> dohvatiNeAktivneOglase() {
        return oglasService.dohvatiNeAktivneOglase();
    }

    @GetMapping("/oglas/getByAlat/{id}")
    public List<Oglas> dohvatiOglasPoAlatu(@PathVariable("id") Long alatId) {
        return oglasService.dohvatiOglasKojiSadrziAlatId(alatId);
    }

    @GetMapping("/oglas/korisnik")
    public List<OglasAndInteresDTO> dohvatiOglasePoKorisniku() {
        return oglasService.dohvatiOglasePoKorisniku();
    }

    @GetMapping("/oglas/korisnik/inactive")
    public List<Oglas> dohvatiNeaktivneOglasePoKorisniku() {
        return oglasService.dohvbatiNeaktivneOglasePoKorisniku();
    }

    @GetMapping("/oglas/korisnik/interes")
    public  List<OglasAndInteresDTO> dohvatiOglasePoInteresu() {
        return oglasService.dohvatiOglasePoInteresu();
    }

    @PutMapping("/oglas/modify/{oglasId}")
    public Oglas oglasModify(@PathVariable("oglasId") Long id, @RequestBody OglasAndAlatDTO dto){
        return oglasService.oglasModify(id, dto);
    }

    @PostMapping("/posudba/oglas/{id}")
    public InteresZaPosudbu zabiljeziInteresZaZahtjev(@PathVariable("id") Long id) {
        return oglasService.zabiljeziInteresZaOglas(id);
    }

    @PostMapping("/posudba/oglas/{oglasId}/{posuditeljId}")
    public void napraviPosudbuOglas(@PathVariable("oglasId") Long oId, @PathVariable("posuditeljId") Long pId) {
        oglasService.napraviPosudbuOglas(oId, pId);
    }

    @PostMapping("/oglas/povrat/{id}")
    public Oglas napraviPovratOglas(@PathVariable("id") Long oglasId) {
        return oglasService.povratOglas(oglasId);
    }

    @GetMapping("oglas/interes/cancel/{id}")
    public boolean otkaziInteresOglas(@PathVariable("id") Long oglasId) {
        return oglasService.otkaziInteresOglas(oglasId);
    }

}
