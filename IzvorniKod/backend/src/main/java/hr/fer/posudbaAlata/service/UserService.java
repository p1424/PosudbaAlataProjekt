package hr.fer.posudbaAlata.service;

import hr.fer.posudbaAlata.rest.dto.JwtRequest;
import hr.fer.posudbaAlata.rest.dto.JwtResponse;
import hr.fer.posudbaAlata.domain.Korisnik;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserService {
	
	Korisnik createUser(Korisnik korisnik);
	JwtResponse loginUser(JwtRequest jwtreq, String token);
	Korisnik deleteUser(Long userId);
	Korisnik getUser(Long id);
	Korisnik modifyUser(Korisnik korisnik);
	List<Korisnik> getAllUsers();
	UserDetails loadUserByUsername(String username);
    Optional<Korisnik> findUserByUsername(String username);
    Korisnik blokirajKorisnika(Long userId);
	List<Korisnik> getBlockedUsers();
	List<Korisnik> getUnblockedUsers();
	Optional<Korisnik> findByid(Long id);
    Korisnik odblokirajKorisnika(Long id);
}
