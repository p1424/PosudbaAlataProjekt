package hr.fer.posudbaAlata.service.impl;

import hr.fer.posudbaAlata.dao.InteresRepository;
import hr.fer.posudbaAlata.domain.InteresZaPosudbu;
import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.domain.Oglas;
import hr.fer.posudbaAlata.domain.Zahtjev;
import hr.fer.posudbaAlata.service.InteresService;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InteresServiceJpa implements InteresService {

    private final InteresRepository interesRepo;
    private final UserService userService;

    @Autowired
    public InteresServiceJpa(InteresRepository interesRepo, UserService us) {
        this.interesRepo = interesRepo;
        this.userService = us;
    }

    @Override
    public List<Korisnik> dohvatiZainteresiraneKorisnikePoOglasu(Long oglasId) {
        return interesRepo.dohvatiZainteresiranePoOglasu(oglasId);
    }

    @Override
    public List<Korisnik> dohvatiZainteresiraneKorisnikePoZahtjevu(Long zahtjevId) {
        return interesRepo.dohvatiZainteresiranePoZahtjevu(zahtjevId);
    }

    @Override
    public List<InteresZaPosudbu> getAll(){
        return interesRepo.findAll();
    }

    @Override
    public List<InteresZaPosudbu> getAllZahtjev(Long zahtjevId) {
        return interesRepo.findAllZahtjev(zahtjevId);
    }

    @Override
    public List<InteresZaPosudbu> getAllOglas(Long oglasId) {
        return interesRepo.findAllOglas(oglasId);
    }

    @Override
    public void delete(Long intId){
        interesRepo.deleteById(intId);
    }

    @Override
    public InteresZaPosudbu saveInteres(InteresZaPosudbu interes) {
        return interesRepo.save(interes);
    }

    @Override
    public InteresZaPosudbu dohvatiPoZahtjevuIKorisniku(Long zahtjevId, Long userId) {
        InteresZaPosudbu interes = interesRepo.dohvatiInteresPoZahtjevuIKorisniku(zahtjevId, userId);

        if(interes == null) {
            throw new NotFoundException("Takav interes ne postoji");
        } else {
            return interes;
        }
    }

    @Override
    public InteresZaPosudbu dohvatiPoOglasuIKorisniku(Long oglasId, Long userId) {
        InteresZaPosudbu interes = interesRepo.dohvatiInteresPoOglasuIKorisniku(oglasId, userId);

        if(interes == null) {
            throw new NotFoundException("Takav interes ne postoji");
        } else {
            return interes;
        }
    }

    @Override
    public List<Zahtjev> dohvatiZahtjeveIzInteresa(Long userId) {
        return interesRepo.dohvatiZahtjeveIzInteresa(userId);
    }

    @Override
    public List<Oglas> dohvatiOglaseIzInteresa(Long userId) {
        return interesRepo.dohvatiOglaseIzInteresa(userId);
    }

}
