package hr.fer.posudbaAlata.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Entity @Table
public class Komentar {

    @Id @GeneratedValue
    private Long idKomentara;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @JsonFormat(pattern="dd.MM.yyyy")
    private LocalDate datumKomentara;

    private String tekstKomentara;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JsonIgnore
    private Zahtjev naZahtjev;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Korisnik ostavljaKorisnik;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Lajk> lajkovi;

    @Transient
    private boolean currentUserLiked;

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Lajk> getLajkovi() {
        return lajkovi;
    }

    public void setLajkov(Lajk lajk) {
        if(this.lajkovi == null){
            this.lajkovi = new LinkedList<Lajk>();
        }


        this.lajkovi.add(lajk);
    }

    public Long getIdKomentara() {
        return idKomentara;
    }

    public LocalDate getDatumKomentara() {
        return datumKomentara;
    }

    public void setDatumKomentara(LocalDate datumKomentara) {
        this.datumKomentara = datumKomentara;
    }

    public String getTekstKomentara() {
        return tekstKomentara;
    }

    public void setTekstKomentara(String tekstKomentara) {
        this.tekstKomentara = tekstKomentara;
    }

    public Zahtjev getNaZahtjev() {
        return naZahtjev;
    }

    public void setNaZahtjev(Zahtjev naZahtjev) {
        this.naZahtjev = naZahtjev;
    }

    public Korisnik getOstavljaKorisnik() {
        return ostavljaKorisnik;
    }

    public void setOstavljaKorisnik(Korisnik ostavljaKorisnik) {
        this.ostavljaKorisnik = ostavljaKorisnik;
    }

    public Komentar() { }

    public Komentar(String datumKomentara, String tekstKomentara, Zahtjev naZahtjev, Korisnik ostavljaKorisnik) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        this.datumKomentara = LocalDate.parse(datumKomentara, formatter);
        this.tekstKomentara = tekstKomentara;
        this.naZahtjev = naZahtjev;
        this.ostavljaKorisnik = ostavljaKorisnik;
    }

    @Override
    public String toString() {
        return "Komentar{" +
                "idKomentara=" + idKomentara +
                ", datumKomentara=" + datumKomentara +
                ", tekstKomentara='" + tekstKomentara + '\'' +
                ", naZahtjev=" + naZahtjev +
                ", ostavljaKorisnik=" + ostavljaKorisnik +
                '}';
    }

    public void makniLajk(Lajk lajk) {
        lajkovi.remove(lajk);
    }

    public boolean isCurrentUserLiked() {
        return currentUserLiked;
    }

    public void setCurrentUserLiked(boolean currentUserLiked) {
        this.currentUserLiked = currentUserLiked;
    }
}
