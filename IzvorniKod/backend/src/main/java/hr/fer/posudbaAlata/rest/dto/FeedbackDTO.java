package hr.fer.posudbaAlata.rest.dto;

public class FeedbackDTO {

    private String tekstFeedbacka;
    private int ocjena;

    public FeedbackDTO(String tekstFeedbacka, int ocjena) {
        this.tekstFeedbacka = tekstFeedbacka;
        this.ocjena = ocjena;
    }

    public FeedbackDTO() {
    }

    public String getTekstFeedbacka() {
        return tekstFeedbacka;
    }

    public void setTekstFeedbacka(String tekstFeedbacka) {
        this.tekstFeedbacka = tekstFeedbacka;
    }

    public int getOcjena() {
        return ocjena;
    }

    public void setOcjena(int ocjena) {
        this.ocjena = ocjena;
    }
}
