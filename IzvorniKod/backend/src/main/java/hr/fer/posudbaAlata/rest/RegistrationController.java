package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.domain.Korisnik;
import hr.fer.posudbaAlata.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
public class RegistrationController {
	
	@Autowired
	private UserService userService;

	@PostMapping
	public Korisnik createUser(@RequestBody Korisnik korisnik) {
		return userService.createUser(korisnik);
	}

}
