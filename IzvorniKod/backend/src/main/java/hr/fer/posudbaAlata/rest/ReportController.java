package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.domain.Report;
import hr.fer.posudbaAlata.rest.dto.ReportDTO;
import hr.fer.posudbaAlata.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/report/{id}")
    public Report prijaviKorisnika(@PathVariable("id") Long userId, @RequestBody ReportDTO reportDTO) {
        return reportService.prijaviKorisnika(userId, reportDTO);
    }

    @GetMapping("/reports/{userId}")
    public List<Report> dohvatiListuReportaZaKorisika(@PathVariable("userId") Long userId){
        return reportService.dohvatiListuReportaZaKorisnika(userId);
    }
}
