package hr.fer.posudbaAlata.rest;

import hr.fer.posudbaAlata.rest.dto.JwtRequest;
import hr.fer.posudbaAlata.rest.dto.JwtResponse;
import hr.fer.posudbaAlata.service.UserService;
import hr.fer.posudbaAlata.utils.JWTUtil;
import hr.fer.posudbaAlata.utils.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    public LoginController(UserService us, JWTUtil jwt, AuthenticationManager am){
        this.userService = us;
        this.jwtUtil = jwt;
        this.authenticationManager = am;
    }

    public static User authorize(){
        try{
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            return (User) auth.getPrincipal();
        } catch (Exception e) {
            return null;
        }
    }


    @PostMapping
    public JwtResponse validateUser(@RequestBody JwtRequest jwtreq) throws NotFoundException {

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            jwtreq.getUsername(),
                           jwtreq.getPassword()
                    )
            );

            System.out.println("in validateUser: " + authentication.getPrincipal());
        }catch (BadCredentialsException e){
            throw new NotFoundException("Krivo korisničko ime ili lozinka.");
        }

        final UserDetails userDetails = userService.loadUserByUsername(jwtreq.getUsername());
        final String token = jwtUtil.generateToken(userDetails);

        return userService.loginUser(jwtreq, token);
    }

}