package hr.fer.posudbaAlata.rest.dto;

import hr.fer.posudbaAlata.domain.KategorijaAlata;
import hr.fer.posudbaAlata.domain.MjestoPrimjeneAlata;
import hr.fer.posudbaAlata.domain.NacinKontakta;

public class OglasAndAlatDTO {

    private String nazivAlata;
    private String fotoUrl;
    private String poveznicaNaProizvod;
    private int snagaAlata;
    private boolean isAkumulatorski;
    private KategorijaAlata kategorijaAlata;
    private String podvrsta;
    private MjestoPrimjeneAlata mjestoPrimjeneAlata;
    private String opisOglasa;
    private NacinKontakta nacinKontakta;

    public OglasAndAlatDTO(String nazivAlata, String fotoUrl, String poveznicaNaProizvod, int snagaAlata, boolean isAkumulatorski, KategorijaAlata kategorijaAlata, String podvrsta, MjestoPrimjeneAlata mjestoPrimjeneAlata, String opisOglasa, NacinKontakta nacinKontakta) {
        this.nazivAlata = nazivAlata;
        this.fotoUrl = fotoUrl;
        this.poveznicaNaProizvod = poveznicaNaProizvod;
        this.snagaAlata = snagaAlata;
        this.isAkumulatorski = isAkumulatorski;
        this.kategorijaAlata = kategorijaAlata;
        this.podvrsta = podvrsta;
        this.mjestoPrimjeneAlata = mjestoPrimjeneAlata;
        this.opisOglasa = opisOglasa;
        this.nacinKontakta = nacinKontakta;
    }

    public OglasAndAlatDTO() {
    }

    public String getNazivAlata() {
        return nazivAlata;
    }

    public void setNazivAlata(String nazivAlata) {
        this.nazivAlata = nazivAlata;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getPoveznicaNaProizvod() {
        return poveznicaNaProizvod;
    }

    public void setPoveznicaNaProizvod(String poveznicaNaProizvod) {
        this.poveznicaNaProizvod = poveznicaNaProizvod;
    }

    public int getSnagaAlata() {
        return snagaAlata;
    }

    public void setSnagaAlata(int snagaAlata) {
        this.snagaAlata = snagaAlata;
    }

    public boolean isAkumulatorski() {
        return isAkumulatorski;
    }

    public void setAkumulatorski(boolean akumulatorski) {
        isAkumulatorski = akumulatorski;
    }

    public KategorijaAlata getKategorijaAlata() {
        return kategorijaAlata;
    }

    public void setKategorijaAlata(KategorijaAlata kategorijaAlata) {
        this.kategorijaAlata = kategorijaAlata;
    }

    public String getPodvrsta() {
        return podvrsta;
    }

    public void setPodvrsta(String podvrsta) {
        this.podvrsta = podvrsta;
    }

    public MjestoPrimjeneAlata getMjestoPrimjeneAlata() {
        return mjestoPrimjeneAlata;
    }

    public void setMjestoPrimjeneAlata(MjestoPrimjeneAlata mjestoPrimjeneAlata) {
        this.mjestoPrimjeneAlata = mjestoPrimjeneAlata;
    }

    public String getOpisOglasa() {
        return opisOglasa;
    }

    public void setOpisOglasa(String opisOglasa) {
        this.opisOglasa = opisOglasa;
    }

    public NacinKontakta getNacinKontakta() {
        return nacinKontakta;
    }

    public void setNacinKontakta(NacinKontakta nacinKontakta) {
        this.nacinKontakta = nacinKontakta;
    }
}
