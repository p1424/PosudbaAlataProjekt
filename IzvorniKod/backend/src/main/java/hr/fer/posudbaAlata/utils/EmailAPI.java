package hr.fer.posudbaAlata.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class EmailAPI{

    @Value("${email.secret}")
    private String secret;

    public void sendMail(String to, EmailSubject subject, String username, String text){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();


        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);
        javaMailSender.setUsername("posudbaAlata@gmail.com");
        javaMailSender.setPassword(secret);
        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");


        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("posudbaAlata@gmail.com");
        msg.setTo(to);
        msg.setSubject(subject.toString());
        msg.setText(getHtml(username,text));
        javaMailSender.send(msg);
    }

    private String getHtml(String username, String message) {
        return "Pozdrav "+username+ "!"+
                """
                
                """+
                message +
                """
                
                
                
                
                
                
                
                Molimo da na ovu poruku ne odgovarate. Poruka je generirana automatski. Za više informacija posjetite stranicu https://posudba-alata.herokuapp.com/ .
                """;

    }
}
