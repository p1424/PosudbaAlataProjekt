package hr.fer.posudbaAlata.dao;

import hr.fer.posudbaAlata.domain.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    @Query("SELECT r FROM Report r WHERE r.reportaniKorisnik.userId = :id")
    List<Report> findReportZaKorisnika(@Param("id") Long korisnikId);
}
